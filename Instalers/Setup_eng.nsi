;----------------------------------
; ����� ������������
;----------------------------------
  RequestExecutionLevel admin
  SetCompressor /SOLID lzma
  OutFile "WindowsPM.exe"

  Name "Windows Position Manager"
  !define VERSION "1.0.0"
  Caption "$(^Name) ${VERSION} Setup Wizard"

  XPStyle on
  !include "MUI2.nsh"

  !define MUI_ICON "Images\Install.ico"
  !define MUI_UNICON "Images\Uninstall.ico"

  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_RIGHT
  !define MUI_HEADERIMAGE_BITMAP "Images\Header.bmp"
  !define MUI_WELCOMEFINISHPAGE_BITMAP "Images\Left.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "Images\Left.bmp"
  !define MUI_ABORTWARNING

  ; �������� ������������
  !define MUI_WELCOMEPAGE_TITLE "Welcome to the $(^Name) ${VERSION} Setup Wizard!"
  !define MUI_WELCOMEPAGE_TEXT "$\r$\n$\nThis wizard will guide you through the installation process of $(^Name).$\r$\n$\nClick Next to continue or Cancel to exit Setup."
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "Suite_eng\License.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !define MUI_FINISHPAGE_RUN "$INSTDIR\WPM.exe"
  !define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\Readme.txt"
  !define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
  !define MUI_FINISHPAGE_LINK "� 2015, Dmitry Malikhtarovich"
  !define MUI_FINISHPAGE_LINK_LOCATION "https://www.facebook.com/dimrandom"
  !define MUI_FINISHPAGE_REBOOTLATER_DEFAULT
  !insertmacro MUI_PAGE_FINISH

  ; �������� ��������������
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !define MUI_FINISHPAGE_REBOOTLATER_DEFAULT
  !insertmacro MUI_UNPAGE_FINISH

  ; ���� ���������� - English
  !insertmacro MUI_LANGUAGE "English"


;----------------------------------
; �������
;----------------------------------

;------------------------------------- 

Function .onInit
/*
  ��� ������� (callback) ���������� �� ������ ������������ � ��������� ���������
  ���������� %INSTDIR%, ������� ���� � ������������� ����� ��������� �� �������
*/
  ReadRegStr $INSTDIR HKCU "SOFTWARE\StRandom\$(^Name)" "InstallPath"
  IfFileExists $INSTDIR Exit
  StrCpy $INSTDIR "$PROGRAMFILES\$(^Name)"
  Exit:
FunctionEnd

;----------------------------------

Function .onInstSuccess
/*
  ��� ������� ��������� ���� � ����� Run � ���������, ���������� �� WPM, �
  ���� �� �� ���������� � ������� 1 �������, �� ��������� ����� � �������� ���������
*/
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "$(^Name)" '"$INSTDIR\WPM.exe"'

  HideWindow
  Sleep 250
  FindWindow $0 "BKA_WPMPRG_MMF"
  IntCmp $0 0 +2
    Return
  Sleep 750
  FindWindow $0 "BKA_WPMPRG_MMF"
  IntCmp $0 0 +2
    Return
  SetShellVarContext current
  ExecShell "open" '"$SMPROGRAMS\$(^Name)"'
FunctionEnd

;----------------------------------

Function IsOsVista
/*
  ������� ��������� ����� ������ Windows; ���� �� Vista, �� � $R0 �����
  ���������� '1', ����� � $R0 ����� '0'
*/
  ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows NT\CurrentVersion" "CurrentVersion"
  StrCmp $R0 "6.0" OsVista
    StrCpy $R0 '0'
    Return
  OsVista:
    StrCpy $R0 '1'
FunctionEnd

;----------------------------------

Function CloseRunningWPM
/*
  ��� ������� ��������� ������� ���������� ����� WPM (�� ������ ����); ���� WPM
  ��������, �������� ��� �������, � ���� �� ����������, ��������� ������� �����������
*/
  FindWindow $0 "BKA_WPMPRG_MAINWND"
  IntCmp $0 0 NotRunned
    MessageBox MB_ICONQUESTION|MB_OKCANCEL|MB_DEFBUTTON1 "$(^Name) is running now, but it needs to be closed$\r$\nbefore the installation process can continue.$\r$\n$\r$\nClick OK to close $(^Name) automatically or$\r$\nCancel to stop the installation." IDOK NeedToClose
    Abort
  NeedToClose:
    StrCpy $0 "$TEMP\_WPM_inst"
    SetOutPath $0
    File "Suite_eng\WPM.exe"
    File "Suite_eng\KbdHook.dll"
    ExecWait '"$0\WPM.exe" /close'
    SetOutPath $TEMP
    Sleep 200
    RmDir /r $0
    FindWindow $0 "BKA_WPMPRG_MAINWND"
    IntCmp $0 0 NotRunned
      Sleep 300
      FindWindow $0 "BKA_WPMPRG_MAINWND"
      IntCmp $0 0 NotRunned
        Sleep 500
        FindWindow $0 "BKA_WPMPRG_MAINWND"
        IntCmp $0 0 NotRunned
          MessageBox MB_ICONSTOP|MB_OK "$(^Name) could not be closed automatically and is still running,$\r$\ntherefore the installation process cannot continue. Please$\r$\nclose $(^Name) manually and start the setup program again."
          Abort
  NotRunned:
FunctionEnd

;----------------------------------

Function un.CloseRunningWPM
/*
  ��� ������� ��������� ������� ���������� ����� WPM (�� ������ ����); ���� WPM
  ��������, �������� ��� �������, � ���� �� ����������, ��������� ������� �������������
*/
  FindWindow $0 "BKA_WPMPRG_MAINWND"
  IntCmp $0 0 NotRunned
    ReadRegStr $0 HKCU "SOFTWARE\StRandom\$(^Name)" "InstallPath"
    StrCmp $0 "" +1 PathGot
      StrCpy $0 $INSTDIR
    PathGot:
      ExecWait '"$0\WPM.exe" /close'
      Sleep 200
      FindWindow $0 "BKA_WPMPRG_MAINWND"
      IntCmp $0 0 NotRunned
        Sleep 300
        FindWindow $0 "BKA_WPMPRG_MAINWND"
        IntCmp $0 0 NotRunned
          Sleep 500
          FindWindow $0 "BKA_WPMPRG_MAINWND"
          IntCmp $0 0 NotRunned
            MessageBox MB_ICONSTOP|MB_OK "$(^Name) could not be closed automatically and is still running,$\r$\ntherefore the deinstallation process cannot continue. Please$\r$\nclose $(^Name) manually and try to uninstall the program again."
            Abort
  NotRunned:
FunctionEnd

;----------------------------------

Function CopyRegStrParam
/*
  ��� ������� ��������� ������� ��������� ����� ������� HKCU $R1 $0, ���� ��� ���, ��
  � ������ ������� ��������� HKCU $R0 $0 �������� ��� ���������� � HKCU $R1 $0
*/
  Push $5
  ReadRegStr $5 HKCU $R1 $0
  StrCmp $5 "" +1 Exit
  ReadRegStr $5 HKCU $R0 $0
  StrCmp $5 "" Exit
  WriteRegStr HKCU $R1 $0 $5
  Exit:
  Pop $5
FunctionEnd

Function CopyAndRenameRegStrParam
/*
  ��� ������� ��������� ������� ��������� ����� ������� HKCU $R1 $1, ���� ��� ���, ��
  � ������ ������� ��������� HKCU $R0 $0 �������� ��� ���������� � HKCU $R1 $1
*/
  Push $5
  ReadRegStr $5 HKCU $R1 $1
  StrCmp $5 "" +1 Exit
  ReadRegStr $5 HKCU $R0 $0
  StrCmp $5 "" Exit
  WriteRegStr HKCU $R1 $1 $5
  Exit:
  Pop $5
FunctionEnd

;----------------------------------

Function CopyActionParams
/*
  ��� ������� �������� ��������� ����� $R0\Actions\$R2 � ���� $R1\Actions\$R2
*/
  ClearErrors
  StrCpy $0 "$R0\Actions\$R2"
  StrCpy $1 "$R1\Actions\$R2"
  ReadRegStr $5 HKCU $0 "Case"
  IfErrors Exit
  WriteRegStr HKCU $1 "Case" $5
  ReadRegStr $5 HKCU $0 "Delim"
  IfErrors Exit
  WriteRegStr HKCU $1 "Delim" $5
  ReadRegStr $5 HKCU $0 "Hotkey"
  IfErrors Exit
  WriteRegStr HKCU $1 "Hotkey" $5
  ReadRegStr $5 HKCU $0 "Keyword"
  IfErrors Exit
  WriteRegStr HKCU $1 "Keyword" $5
  ReadRegStr $5 HKCU $0 "Layout"
  IfErrors Exit
  WriteRegStr HKCU $1 "Layout" $5
  Exit:
FunctionEnd

;----------------------------------

Function DeleteOldFiles
/*
  ��� ������� ������� ��� ����� ���������� ��������� WPM, � ����� ���������������
  ����� .dll � .exe � ������, ���� �� ������� ������ (���� ��� ������ ������� ����������)
*/
  ; �������� ������ ���������� ������
  DeleteRegKey HKCU "SOFTWARE\StRandom\WPM"

  ; �������� ������� ���������� ������
  Delete "$SMPROGRAMS\WPM\Help.lnk"
  Delete "$SMPROGRAMS\WPM\Uninstall.lnk"

  ; �������� ������ ������
  Delete /REBOOTOK "$INSTDIR\KbdHook.old"
  Delete /REBOOTOK "$INSTDIR\WPM.old"

  ; ������� �������� ������, ������� ����� �� ���� ������������
  Delete "$INSTDIR\KbdHook.dll"
  Delete "$INSTDIR\WPM.exe"

  ; �������� ����� ����������� �� ������� (�� ������ ������������)
  DeleteRegValue HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "$(^Name)"

  ; �������� ��������� �������� KbdHook.dll
  IfFileExists $INSTDIR\KbdHook.dll +1 KbdHookDllOk
  Rename "$INSTDIR\KbdHook.dll" "$INSTDIR\KbdHook.old"
  Delete /REBOOTOK "$INSTDIR\KbdHook.old"
  IfFileExists $INSTDIR\KbdHook.dll DeletionError
  KbdHookDllOk:

  ; �������� ��������� �������� WPM.exe
  IfFileExists $INSTDIR\SoundPilot.exe +1 WPMExeOk
  Rename "$INSTDIR\WPM.exe" "$INSTDIR\WPM.old"
  Delete /REBOOTOK "$INSTDIR\WPM.old"
  IfFileExists $INSTDIR\WPM.exe DeletionError
  WPMExeOk:

  Return

  DeletionError:
    MessageBox MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2 "Some files could not be written to the destination folder.$\r$\nPlease restart your computer and run the setup program again.$\r$\nClick Yes if you want to restart now or No to quit." IDNO +2
    Reboot
    Abort
FunctionEnd

;----------------------------------

Function GetAffiliateCode
/*
  ��� ������� ���� ��������-���, �������� � .exe ������������ � �������� ��� � ������
*/
  FileOpen $R0 $EXEPATH r		; � $R0 - ����� �����
  IntCmp $R0 0 Exit
  FileSeek $R0 0 END $R1		; � $R1 - ������ �����
  IntOp $0 $R1 & 0x7FFFFFF0
  IntOp $0 $0 - 4			; � $0 - ������� SignB
  IntOp $1 $0 - 8192			; � $1 - ������ ������� ������
  Loop:
    FileSeek $R0 $0
    FileRead $R0 $2 4
    IntOp $0 $0 - 16
    StrCmp $2 "A9=z" SignBFound
    IntCmp $0 $1 CloseAndExit CloseAndExit Loop
  SignBFound:
    IntOp $0 $0 + 14			; ����� � $0 - ������� ����� ������� �����
    FileSeek $R0 $0
    FileReadByte $R0 $1
    FileReadByte $R0 $2
    IntOp $2 $2 * 256
    IntOp $0 $0 - $2
    IntOp $0 $0 - $1
    IntOp $0 $0 + 6			; ������ � $0 - ������� SignA
    FileSeek $R0 $0
    FileRead $R0 $0 4
    StrCmp $0 "c{e2" SignAFound CloseAndExit
  SignAFound:
    FileRead $R0 $0
    WriteRegStr HKCU "SOFTWARE\StRandom\$(^Name)" "CustomBuild" $0
  CloseAndExit:
    FileClose $R0
  Exit:
FunctionEnd 

;----------------------------------

Function un.OpenUninstallForm
/*
  ��� ������� ���������� � ������ ����������� ��������� �������� ��������������;
  ������ ������� - �������� feedback-����� �� ����� ���������
*/
  ReadRegStr $0 HKCU "SOFTWARE\StRandom\$(^Name)" "US2"
  ExecShell "open" "https://www.facebook.com/dimrandom"
FunctionEnd


;----------------------------------
; ������ ������������
;----------------------------------
Section "Install"
  Call CloseRunningWPM

  StrCpy $R1 "SOFTWARE\StRandom\$(^Name)"
  WriteRegStr HKCU $R1 "InstallPath" "$INSTDIR"
  WriteRegDWORD HKCU $R1 "ShowTutorialOnRun" 1

  /*Call CopyOldRegParameters*/

  Call DeleteOldFiles
  SetOutPath $INSTDIR
  File "Suite_eng\*.*"

  SetOutPath $INSTDIR

  DeleteRegValue HKCU "SOFTWARE\StRandom\$(^Name)" "CustomBuild"
  Call GetAffiliateCode
  
  StrCpy $1 "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
  WriteRegStr HKLM $1 "DisplayName" "$(^Name)"
  WriteRegStr HKLM $1 "DisplayIcon" '"$INSTDIR\WPM.exe"'
  WriteRegStr HKLM $1 "DisplayVersion" "${VERSION}"
  WriteRegStr HKLM $1 "UninstallString" '"$INSTDIR\Uninstall.exe"'
  WriteRegStr HKLM $1 "Publisher" "Dmitry Malikhtarovich"
  WriteRegStr HKLM $1 "URLInfoAbout" "https://www.facebook.com/dimrandom"
  WriteRegStr HKLM $1 "HelpLink" "https://www.facebook.com/dimrandom"
  WriteRegStr HKLM $1 "URLUpdateInfo" "https://www.facebook.com/dimrandom"
  WriteRegDWORD HKLM $1 "NoModify" 1
  WriteRegDWORD HKLM $1 "NoRepair" 1

  WriteUninstaller "$INSTDIR\Uninstall.exe"

  Call IsOsVista
  SetShellVarContext current
  CreateDirectory "$SMPROGRAMS\$(^Name)"
  CreateShortCut "$SMPROGRAMS\$(^Name)\$(^Name) Tour.lnk" "$INSTDIR\Tour.chm" "" "$INSTDIR\Tour.chm" 0
  CreateShortCut "$SMPROGRAMS\$(^Name)\$(^Name) Help.lnk" "$INSTDIR\Help.chm" "" "$INSTDIR\Help.chm" 0
  CreateShortCut "$SMPROGRAMS\$(^Name)\$(^Name).lnk" "$INSTDIR\WPM.exe" "" "$INSTDIR\WPM.exe" 0

  StrCmp $R0 '0' NonVistaOs
    CreateShortCut "$SMPROGRAMS\$(^Name)\$(^Name) (as administrator).lnk" "$INSTDIR\WPM.exe" "/elevated" "$INSTDIR\WPM.exe" 0
    CreateShortCut "$SMPROGRAMS\$(^Name)\Uninstall $(^Name).lnk" "$INSTDIR\Uninstall.exe" "/uninstall" "$INSTDIR\Uninstall.exe" 0
    Goto Finish
  NonVistaOS:
    CreateShortCut "$SMPROGRAMS\$(^Name)\Uninstall $(^Name).lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
  Finish:

  SetRebootFlag false
SectionEnd


;----------------------------------
; ������ ��������������
;----------------------------------
Section "Uninstall"
  Call un.CloseRunningWPM

  DeleteRegValue HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "$(^Name)"
  DeleteRegValue HKCU "Software\StRandom" "$(^Name)" 

  SetShellVarContext current
  RMDir /r "$SMPROGRAMS\$(^Name)"

  Delete /REBOOTOK "$INSTDIR\Help.chm"
  Delete /REBOOTOK "$INSTDIR\History.txt"
  Delete /REBOOTOK "$INSTDIR\KbdHook.dll"
  Delete /REBOOTOK "$INSTDIR\License.txt"
  Delete /REBOOTOK "$INSTDIR\Readme.txt"
  Delete /REBOOTOK "$INSTDIR\Tour.chm"
  Delete /REBOOTOK "$INSTDIR\WPM.exe"
  Delete /REBOOTOK "$INSTDIR\Uninstall.exe"
  RMDir /REBOOTOK "$INSTDIR"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"

  SetRebootFlag false
  Call un.OpenUninstallForm
SectionEnd