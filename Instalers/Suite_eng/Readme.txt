WPM�
Copyright � 2015 Dmitry Malikhtarovich
All Rights Reserved

===============
README CONTENTS
===============

	1. OVERVIEW
	2. SYSTEM REQUIREMENTS
	3. INSTALLATION
	4. UNINSTALL
	5. THE STATUS OF THE PROGRAM
	6. HOW TO CONTACT US


1. OVERVIEW
===========

The program adds the ability to easily control the position of the windows. It creates your own tile interface. 


2. SYSTEM REQUIREMENTS
======================

 - PC-compatible computer with the 486 processor
   or higher;
 - Microsoft Windows XP/Vista/7/8 (x86 and x64);
 - 4 Mb of the free hard disk space.


3. INSTALLATION
===============

To install the program, run WindowsPM.exe and then
follow requests of the installation program.


4. UNINSTALL
============

If you wish to uninstall WPM, follow these
instructions:
  - click on the Uninstall icon in WPM
    group in Start Menu;
or:
  - click the Start menu from the taskbar and
    select Settings/Control Panel;
  - double click on Add/Remove Programs;
  - select WPM from the list and click the
    Add/Remove button;
  - confirm that you want to uninstall WPM.


5. THE STATUS OF THE PROGRAM
============================

WPM is distributed as Shareware.


6. HOW TO CONTACT ME
====================

Dmitry Malikhtarovich
www.facebook.com/dimrandom
