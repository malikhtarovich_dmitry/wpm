/*! @file HookProc.h */

/**
 * @addtogroup HookProc
 * @brief Header file for main definitions
 * @{
 */

/**
 * Message's name for main program
 */
#define HOOKPROC_MSGNAME "M_WPMHkPrcMsg" 

#define HOOKPROC_RES_ER			0x005f	/**< Error (function returned FALSE) */ 
#define HOOKPROC_RES_OK			0x00a7	/**< Success (function returned TRYE) */

/**
 *Command codes for HookProc
 */
#define HOOKPROC_CMD_SET		0x3b00	/**< Command SetHooks(...) */
#define HOOKPROC_CMD_RESET		0x3b01	/**< Command ResetHooks() */
#define HOOKPROC_CMD_SUSPEND	0x3b02	/**< Command SuspendHooks(...) */
#define HOOKPROC_CMD_WAITENDKEY	0x3b03	/**< Command SetWaitForEndKey(...) */

/*! @} */