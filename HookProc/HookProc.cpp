/*! @file HookProc.cpp */

#include <Windows.h>
#include "..\KbdHook\KbdHook.h"
#include "HookProc.h"


HWND	hWMPWnd;	/**< Main window handle of WPM */
HWND	hHookProcWnd;	/**< Main window handle of HookProc.exe */

UINT	dwHookProcMsg;	/**< Message for interaction with WPM */

const char	szMainWndClass[] = "WPM_HkPrc_MainWnd"; /**< Main window's class name */
const char	szMainWndTitle[] = "HookProc x64";        /**< Main window's title */

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == dwHookProcMsg)
	{
		BOOL fOk = TRUE;
		switch (wParam)
		{
			case HOOKPROC_CMD_SET:
				fOk = SetHooks(hWMPWnd, (BOOL) lParam);
				break;
			case HOOKPROC_CMD_RESET:
				fOk = ResetHooks();
				break;
			case HOOKPROC_CMD_SUSPEND:
				SuspendHooks((BOOL) lParam);
				break;
			case HOOKPROC_CMD_WAITENDKEY:
				SetWaitForEndKey((BOOL) lParam);
				break;
			default:
				fOk = FALSE;
		}
		if (fOk)
			return MAKELONG(wParam, HOOKPROC_RES_OK);
		else
			return MAKELONG(wParam, HOOKPROC_RES_ER);
	}

	if (message == WM_DESTROY)
	{
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

/**
 * Get main window handle of WPM
 * @return TRUE/FALSE
 */
BOOL GetSoundPilotWnd(void)
{
	hWMPWnd = NULL;
	HANDLE hFile = CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
		PAGE_READONLY, 0, 1024, "BKA_WPMPRG_MMF");
	if (!hFile) return FALSE;

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		void *pMem = MapViewOfFile(hFile, FILE_MAP_READ, 0, 0, 0);
		if (pMem)
		{
			hWMPWnd = *((HWND*) pMem);
			UnmapViewOfFile(pMem);
		}
	}

	CloseHandle(hFile);
	return (hWMPWnd != NULL);
}


/**
 * Register class and create main window
 * @return TRUE/FALSE
 */
BOOL SetupMainWindow(void)
{
	WNDCLASSEX wc;
	wc.cbSize			= sizeof(wc);
	wc.style			= 0;
	wc.lpfnWndProc		= (WNDPROC) MainWndProc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0; 
	wc.hInstance		= (HINSTANCE) GetModuleHandle(NULL);
	wc.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH) (COLOR_WINDOW + 1);
	wc.lpszMenuName		= NULL; 
	wc.lpszClassName	= szMainWndClass;
	wc.hIconSm			= NULL;

	if (!RegisterClassEx(&wc)) 
			return FALSE;

	hHookProcWnd = CreateWindow(szMainWndClass, szMainWndTitle, WS_POPUP | WS_CAPTION |
		WS_MINIMIZE | WS_CLIPSIBLINGS, 0, 0, 0, 0, NULL, NULL, wc.hInstance, NULL);
	if (hHookProcWnd == NULL) 
			return FALSE;

	SetWindowPos(hHookProcWnd, 0, 0, 0, 0, 0, SWP_HIDEWINDOW | SWP_NOACTIVATE |
		SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER);
	return TRUE;
}

int EntryProc(void)
{
	HWND hOldWnd;
	MSG msg;

	// Close all active processes HookProc.exe
	while (hOldWnd = FindWindow(szMainWndClass, szMainWndTitle))
	{
		PostMessage(hOldWnd, WM_CLOSE, 0, 0);
		Sleep(1);
	}

	dwHookProcMsg = RegisterWindowMessage(HOOKPROC_MSGNAME);
	if (dwHookProcMsg && GetSoundPilotWnd() && SetupMainWindow())
	{
		PostMessage(hWMPWnd, dwHookProcMsg, (WPARAM) hHookProcWnd, 0);
		while (GetMessage(&msg, NULL, 0, 0))
			DispatchMessage(&msg);
		SetHooks(0, FALSE);
	}
	ExitProcess(0);
}