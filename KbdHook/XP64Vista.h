/*! @file XP64Vista.h */

/**
 * @addtogroup KbdHook
 * @brief Additional methods for Windows XP x64 and Windows Vista
 * @{
 */

/**
 * ��������, �������� �� �� Windows Vista ��� ����; ������� ���������� TRUE, ����
 * ��� �� Vista ��� ����, � ���������� FALSE, ���� Win2003 ��� ����
 */
BOOL WINAPI IsOSVistaOrHigher(void);

/**
 * ��������� ������� ��������� UIPI ��� OS Vista � ����; ����� ���� ������� �������������
 * ��������� ������ ��� ����� ���������� ����� (�.�. ����� �������	SetHooks); ����� ����
 * ������� ��������� ����� ������� ���������� ��������� ��	�����, ���������� � ��������
 * � low execution level � low-integrity �������� (�������� IE7); ������� ������ TRUE,
 * ���� �� ���� ��� Vista, � ����� � ������ �������� ��������� ������� ���������;
 * ������� ������ FALSE � ������ ������
 */
BOOL WINAPI SetupVistaUipiFilter(void);

/**
 * ��������, �������� �� ������� ������� ��� ����������� 64-������ ��;	������� ������
 * TRUE, ���� ��� 64-������ �������, ��� ���� 32-������, ���������� � ������ WOW64;
 * ������� ������ FALSE � ������ ������ ��� ���� ������� ������� �������� ���
 * ����������� 32-������ ��
 */
BOOL WINAPI IsOsX64(void);

/**
 * ��������, ����� �� ��������� ������� ������ elevated; ��� �� ���� Windows Vista,
 * � ����� � ������ ������ ������� ���������� FALSE; � ������ ������ �������
 * ���������� TRUE, � � ��������� pbElevation - ������ ��������
 */
BOOL WINAPI IsProcessElevated(HANDLE hProcess, BOOL* pbElevated);

/**
 * ������ ���������� szFile � execution level �������� �������� ��� bDefaultElevation
 * ������ TRUE, ���� ������ � �������� � bRunElevated elevation-�������� �����; �
 * ������ ������ ������� ������ ERROR_SUCCESS, � ������ ��� ������ � ������ ������;
 * ���� ������� ������� �������� 32-������ WOW64 elevated ���������, (����������
 * ��� ����������� 64-������ ��), �� ������� ������� ���������� ��� non-elevated
 * �������� ������� � ������� ������ ��� ������ ERROR_EXE_MACHINE_TYPE_MISMATCH
 */
DWORD WINAPI RunApplication(BOOL bDefaultElevation, BOOL bRunElevated, HWND hWnd,
							LPCSTR szFile, LPCSTR szParameters, LPCSTR szWorkPath);

/*! @} */