/*! @file KbdHook.h */

#include "KbdHookMsg.h"
#include "XP64Vista.h"

/**
 * @addtogroup KbdHook
 * @brief Header file KbdHook.dll and KbdHook64.dll
 * @{
 */

#define VK_ENDKEY VK_CANCEL

/**
 * Set or Unset hooks
 * @param [in] fSetHooks if FALSE then hooks are unset, otherwise hooks are set
 * @param [in] hMainWnd  main window handle, which will recieve message about hooks
 */
BOOL WINAPI SetHooks(HWND hMainWnd, BOOL fSetHooks);

/**
 * Reset hooks
 */
BOOL WINAPI ResetHooks(void);

/**
 * Suspend/Continue hooks
 * @param [in] fSuspend if TRUE the suspend, otherwise resume/continue
 */
void WINAPI SuspendHooks(BOOL fSuspend);

/**
 * Set or forced unset waiting flag of special key
 * ���� ���� ���� ����������, �� ������������ ��� �� ���������� ���������
 * HOOKMSG_NRMKEY (�.�. �� ������������ ������� ������� ������, ����� �����������);
 * ����� ������������� ����������� �� ��������� WAITFORENDKEY_INTERVAL �� � �������
 * ��� ���������, ���� ��� ��������� �	������� ��������� ���� ���������� �������
 * VK_ENDKEY (��� ���� �������� ���� ������������ ��������� HOOKMSG_ENDKEY)
 */
void WINAPI SetWaitForEndKey(BOOL fWait);

/*! @} */