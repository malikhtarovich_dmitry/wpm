// KbdHook.cpp

#define INCLUDED_IN_MAIN_CPP
#pragma comment(linker, "/ENTRY:DllMain /BASE:0x37910000")

#include <Windows.h>


#include "Globals.h"
#include "KbdHook.h"
#include "Log.h"

#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <fstream>
using namespace std;

// ������������ ���������� "�����������" ������������ ������� ���� ��� WH_KEYBOARD �
// WH_MOUSE ��������������, � ������������ ����� �������� (� ��) ������� "�����������"
// ������������ ������� ���� (�� �������� ��������� HOOKMSG_REHOOK ���� fOwnerWindow)
#define MAX_MISSING_KEYBDHOOK_EVENTS	20
#define MAX_MISSING_MOUSEHOOK_EVENTS	10
#define	MAX_MISSING_PAIRHOOK_TIME		5000

// �������� �������� (� ��) ��������������� ������ ����� fWaitForEndKey
#define	WAITFORENDKEY_INTERVAL	3000

static const byte bCrcMaskA[8] = {0x55, 0x8B, 0xEC, 0x83, 0xC4, 0xF0, 0x53, 0xA3};
static const char szCrcString[12] = "D)M0-EXECRC";
static const byte bCrcMaskB[4] = {0xCD, 0x3F, 0xE8, 0x01};

#pragma optimize("gs", on) // ���������� ����������� "�� ��������"

BOOL WINAPI DllMain(HINSTANCE hinstDll, DWORD fdwReason, PVOID fImpLoad)
{
/*	����� ����� dll
*/
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		char szMessage[10];
		hDllInstance = hinstDll;

		// ������������ ����� � ����������� ���������; �������� ���������
		// (bCrcMaskA[3] - bCrcMaskB[1] - 21) ������ ���� ����� 47
		int iDt = bCrcMaskA[3] - bCrcMaskB[1];
		for (int i = 0; i < 8; i++)
			szMessage[i+1] = szCrcString[i+4] + (iDt - 21);
		szMessage[0] = 'M'; szMessage[9] = 0;
		LOG (&szMessage[0]);
		dwKbdHookMsg = RegisterWindowMessage("NEW_HOOK_SYSTEM");
		// ����������� ����� .exe ��������, � ������� ���������� ����
		LOG_MODULE_FILE_NAME();
	}
	return TRUE;
}

//----------------------------------------------------------------------------------------

#pragma optimize("gt", on) // ��������� ����������� "�� ��������"

#define ABS_MACRO(number) (number >= 0 ? number : -number)

static void __fastcall CheckHooks(WORD* pwCounterA, WORD* pwCounterB,
						   DWORD* pdwTimerA, DWORD dwTimerB,
						   WORD wMaxEvents, BOOL fAllowNoPair)
{
/*	�������� ������������ ������ ��� �����; � ������ ���� ���� �� ����� ����� ����������
	��������� �������� (��-�� "�������" ����, �������������� ������ ����������), ����
	hOwnerWindow ����� ������� ��������� HOOKMSG_REHOOK; ���� �� �������� ��� 64-������
	��, �� ���� WH_KEYBOARD � WH_MOUSE ����� ���������� � ��������� ������ ����������,
	� �� ����, � ������� ��� �������� (� WH_GETMESSAGE � ���� ������ ���������� ��
	�����), ������� ��� ����� ��������������� ����������� ��������� �������� �����
	�� ������� �������� ���������� ������� WH_GETMESSAGE
*/
	*pdwTimerA = GetTickCount();

	if (*pwCounterB > 0)
		*pwCounterA = *pwCounterB = 0;
	else
		if ( ((*pwCounterA)++ >= wMaxEvents) &&
			(ABS_MACRO(int(*pdwTimerA - dwTimerB)) > MAX_MISSING_PAIRHOOK_TIME) )
		{
			*pwCounterA = *pwCounterB = 0;
			if (!fAllowNoPair)
#ifndef _WIN64
				PostMessage(hOwnerWindow, dwKbdHookMsg, MAKELONG(32, HOOKMSG_REHOOK), 0);
#else
				PostMessage(hOwnerWindow, dwKbdHookMsg, MAKELONG(64, HOOKMSG_REHOOK), 0);
#endif
		}
}

//----------------------------------------------------------------------------------------

static BOOL __fastcall IsConsoleWindow(HWND hWindow)
{
/*	��������, �������� �� ��������� ���� ����������. ���� ��� ���� ������
	ConsoleWindowClass, �� ������� ������ TRUE, ����� ������ FALSE
*/
	char szBuf[32];
	if (GetClassName(hWindow, szBuf, sizeof(szBuf)))
		if (lstrcmp(szBuf, "ConsoleWindowClass") == 0)
			return TRUE;
		
	return FALSE;
}

//----------------------------------------------------------------------------------------

static BOOL CheckForegroundWindow(void)
{
/*	��� ������� ��������� ����� foreground-����; � ������ ����� ����, ���� ����� ���� ��
	NULL, ������� ���������� ���� fOwnerWindow ��������� � ����� HOOKMSG_WNDFRG; ����� �
	������ ����� ���� ������� ���������, ����������� �� ��� ���� ����������� ����������
	� ������������� ��� ���������� ���� fIsInConsole; ������� ���������� FALSE, ����
	�����-���� �� ���� �������� ��������� �������� foreground, ����� ���������� TRUE
*/
	HWND hNewForeground = GetForegroundWindow();
	if (hLastForeground != hNewForeground)
	{
		hLastFocused = NULL;
		fOwnedDll = fIsInConsole = FALSE;
		hLastForeground = hNewForeground;
		if (hNewForeground != NULL)
		{
			DWORD pid;
			GetWindowThreadProcessId(hNewForeground, &pid);
			fOwnedDll = (pid == dwOwnerProcessId);
			if (!fOwnedDll)
			{
				PostMessage(hOwnerWindow, dwKbdHookMsg, MAKELONG(0, HOOKMSG_WNDFRG),
					(LPARAM) hNewForeground);
				fIsInConsole = IsConsoleWindow(hNewForeground);
			}
		}
	}
	return !fOwnedDll;
}

//----------------------------------------------------------------------------------------

static void __fastcall PostEndMessage(int wParamLow = 0)
{
/*	�������� ���� fOwnerWindow ��������� � ����� HOOKMSG_ENDKEY; �������� wParamLow
	������ �������� �������� ����� ��� wParam ��� ������������� ���������
*/
	PostMessage(hOwnerWindow, dwKbdHookMsg, MAKELONG(wParamLow, HOOKMSG_ENDKEY),
		(LPARAM) hLastFocused);
}

//----------------------------------------------------------------------------------------

static BOOL __fastcall CheckEndKey(WPARAM wPar, LPARAM lPar, HWND hOldFocused,
								   BOOL* fEndMsgPosted)
{
/*	�������� ������� ���������� ������ �������� ����������� �������; ��� ������� ��������
	HOOKMSG_ENDKEY � ������ ���������� ������, � ����� ���� ����� �������, �����������
	������� ���� ������ � ����� ����� ��������; ������� ������ TRUE, ���� ����������
	���� �� ������ ���������� ��������� ������, � ������ FALSE, ���� �� ������ �������
	��������� ������ CallNextHookEx
*/
	if (fWaitForEndKey && (wPar == VK_ENDKEY))
	{
		//MessageBox(0, "VK_ENDKEY", "1", 0);
		if (HIWORD(lPar) & KF_UP)
		{

			fWaitForEndKey = FALSE;
			*fEndMsgPosted = TRUE;
			PostEndMessage(1);
		}
		else
			if (hOldFocused != hLastFocused)
			{
				*fEndMsgPosted = TRUE;
				PostEndMessage();
			}
		return TRUE;
	}

	// ������ ������ �������� �� ��������� ��������� �������
	if (fWaitForEndKey && (GetTickCount() >= dwWaitKeyTimer))
	{
		fWaitForEndKey = FALSE;
		*fEndMsgPosted = TRUE;
		PostEndMessage(1);
	}
	return FALSE;
}

//----------------------------------------------------------------------------------------

static BOOL __fastcall GetKeyAscii(UINT vk, UINT sc, WORD* ss)
{
/*	��������� Ascii ���� (� *ss) ������� �������; vk - ��� ����������� �������, sc -
	��������������� �� ����-���; ������� ������ TRUE � ������ ������ � ������ FALSE
	� ������ ������ ��� � ������, ����� ��� ������� ������� ��� Ascii �������
*/
	if (!sc) return FALSE;

	BYTE bKeys[256];
	GetKeyboardState(bKeys);

	if (fIsInConsole)
	{
		bKeys[VK_SHIFT] = (BYTE) GetKeyState(VK_SHIFT);
		bKeys[VK_CONTROL] = (BYTE) GetKeyState(VK_CONTROL);
		bKeys[VK_MENU] = (BYTE) GetKeyState(VK_MENU);
		bKeys[VK_CAPITAL] = (BYTE) GetKeyState(VK_CAPITAL);
	}

	if ((bKeys[VK_CONTROL] & 0x80) || (bKeys[VK_MENU] & 0x80))
		return FALSE;

	HKL kl = hKbdLayout;
	if (!fIsInConsole) kl = GetKeyboardLayout(0);
	return (ToAsciiEx(vk, sc, bKeys, ss, 0, kl) == 1);
}

//----------------------------------------------------------------------------------------
//+
static void __fastcall ProcessKeyEvent(WPARAM wPar, LPARAM lPar,
									   HWND hOldFocused, BOOL fEndMsgPosted)
{
/*	��������� �������/���������� ������� ����������, ���������� � ������������ �����
	WH_KEYBOARD � WH_GETMESSAGE; ��������� wPar � lPar ����� ��������������� ����������
	�����������	����; hOldFocused - ����� ����������� ���� (��������), �������� �����;
	fEndMsgPosted == TRUE, ���� ��������� HOOKMSG_ENDKEY ��� ������������, ����� FALSE
*/
	// �������� ��������������� �����
	UINT chr = MapVirtualKey((UINT) wPar, 2);
	BOOL fDeacrit = (chr & 0x80008000);
	if (fDeacrit) fSkipNextKey = TRUE;

	// ��������� �������/���������� �������
	if (((HIWORD(lPar) & KF_UP) == 0) && !fSkipNextKey)
	{
		if ( (wPar == VK_BACK) || (wPar == VK_TAB) || (wPar == VK_RETURN) ||
			(wPar == VK_SPACE) || ((wPar >= 0x30) && (wPar <= 0x39)) ||
			((wPar >= 0x41) && (wPar <= 0x5A)) || ((wPar >= 0x60) && (wPar <= 0x6F)) ||
			((wPar >= 0x92) && (wPar <= 0x96)) || ((wPar >= 0xBA) && (wPar <= 0xC0)) ||
			((wPar >= 0xDB) && (wPar <= 0xDF)) )
		{
			WORD ss;
			UINT sc = MapVirtualKey((UINT) wPar, 0);
			if (GetKeyAscii((UINT) wPar, sc, &ss))
			{
				PostMessage(hOwnerWindow, dwKbdHookMsg, MAKELONG(MAKEWORD(LOBYTE(ss),
					LOBYTE(wPar)), HOOKMSG_NRMKEY), (LPARAM) hLastFocused);
			}
			else
				if (!fEndMsgPosted) PostEndMessage();
		}
		else 
			if ( !fEndMsgPosted && ((hOldFocused != hLastFocused) ||
				((wPar >= VK_PRIOR) && (wPar <= VK_DOWN)) || ((wPar >= VK_DELETE) &&
				(wPar <= VK_F24)) || (wPar == VK_ESCAPE)) )
				PostEndMessage();
	}
	else
		if (!fEndMsgPosted && (hOldFocused != hLastFocused))
			PostEndMessage();

	// ����� fSkipNextKey ��� ������� ������� ������
	if (!fDeacrit && ((HIWORD(lPar) & KF_UP) == 0))
		fSkipNextKey = FALSE;
}

//----------------------------------------------------------------------------------------

static BOOL __fastcall CheckKeyMsgDup(PMSG pMsg)
{
/*	�������� ������������ ��������� ����������; ���� ��������� ����������� (��� ��
	��� VirtualKey, �� �� ����� �������� � �� ���� ���������� ������), �� �������
	������ FALSE, ����� (���� ��������� - �� ��������), ������� ������ TRUE
*/
	if (pMsg->message == WM_KEYUP)
	{
		dwLastMsgTime = 0;
		return TRUE;
	}
	if ((dwLastVirtualKey != pMsg->wParam) || (dwLastMsgTime != pMsg->time))
	{
		dwLastMsgTime = pMsg->time;
		dwLastVirtualKey = (DWORD) pMsg->wParam;
		return TRUE;
	}
	return FALSE;
}

//----------------------------------------------------------------------------------------

static LRESULT CALLBACK ShellHookProc(int Code, WPARAM wPar, LPARAM lPar)
{
/*	� ���� ���� �������������� ������� ����� ��������� ���������� (������������ �����
	�����), ����������� ��� ����������� ��������� �������� �� ���������� ����������
*/
	if (Code == HSHELL_LANGUAGE)
		hKbdLayout = (HKL) lPar;
	return CallNextHookEx(hhShell, Code, wPar, lPar);
}

//----------------------------------------------------------------------------------------

static LRESULT CALLBACK MouseHookProc(int Code, WPARAM wPar, LPARAM lPar)
{
/*	���� ��� �������� ��� ��������� ����, �� ��� ���������� ������ 3 �� ���:
	WM_LBUTTONDOWN,	WM_RBUTTONDOWN � WM_MBUTTONDOWN; ���� fIsInConsole == TRUE
	(����� � ���� ����������� ����������), �� �� ������������� ��� ������� ���
	������� ������ � �� ������������ �������� �����; ����� ������ ������������
	�������� �����, � ������� ����� ���������� � ���� WH_GETMESSAGE
*/
	if ( (Code == HC_ACTION) && !fHookSuspended && ((wPar == WM_LBUTTONDOWN) ||
		(wPar == WM_RBUTTONDOWN) || (wPar == WM_MBUTTONDOWN)) )
		if (CheckForegroundWindow())
			if (!fIsInConsole)
				CheckHooks(&wMouse1Counter, &wMouse2Counter, &dwMouse1Timer,
					dwMouse2Timer, MAX_MISSING_MOUSEHOOK_EVENTS, fIsOs64bits);
			else
			{
#ifndef _WIN64
				// ���������� HOOKMSG_ENDKEY ������ �� 32-������� ����, ��� ��� � x64 ��
				// ���������� ����� ���� ���������� ������ - ��� x32 � x64 �����
				PostEndMessage();
#endif
			}
	return CallNextHookEx(hhMouse, Code, wPar, lPar);
}

//----------------------------------------------------------------------------------------

static LRESULT CALLBACK KeyboardHookProc(int Code, WPARAM wPar, LPARAM lPar)
{
/*	���� ��� ���������� �������� ��� ��������� WM_KEYDOWN � WM_KEYUP, ��������� �
	���������� wPar � lPar ���������� �� ����� �������; ���� fIsInConsole == TRUE
	(����� � ���� ����������� ����������), �� �� ������������� ��� ������� ���
	�������/���������� ������ � �� ������������ �������� �����; ����� ������
	������������ �������� �����, � ������� ����� ���������� � ���� WH_GETMESSAGE
*/

	if ((Code == HC_ACTION) && !fHookSuspended)
		if (CheckForegroundWindow())
		{
			// �������� �������� ����� ��� ������������� ����; ���������� ������ �������
			// Alt, ��� ��� ����������� � ������ ���� ���� ���� (��� ���� ��� ������� Alt
			// ���������� ���� WH_GETMESSAGE �������� ������ �� �����)
			if (!fIsInConsole && (wPar != VK_MENU))
			{
				
				CheckHooks(&wKey1Counter, &wKey2Counter, &dwKey1Timer,
					dwKey2Timer, MAX_MISSING_KEYBDHOOK_EVENTS, fIsOs64bits);
			}

			// �������� ������� ������ ������ �������� ����������� �������
			BOOL fEndMsgPosted = FALSE;
			if (CheckEndKey(wPar, lPar, hLastFocused, &fEndMsgPosted))
			{
				return 1;
			}

#ifndef _WIN64
			// ��������� �������/���������� ������� � ���������� ���� (������ ��� x86)
			if (fIsInConsole && !fWaitForEndKey)
				ProcessKeyEvent(wPar, lPar, 0, fEndMsgPosted);
#endif
		}
	return CallNextHookEx(hhKeyboard, Code, wPar, lPar);
}

//----------------------------------------------------------------------------------------

static LRESULT CALLBACK GetMessageHookProc(int Code, WPARAM wPar, LPARAM lPar)
{
/*	� ���� ���� �������������� ������� ���� � ����������; ���� ��� ������ ���������� �
	��������� ��������, ���� �� �������, � ������� �� ���������� ��� ���������� ����
*/
	if ((Code == HC_ACTION) && !fHookSuspended && (wPar != PM_NOREMOVE))
	{
		UINT msg = ((PMSG) lPar)->message;
		if ((msg == WM_KEYDOWN) || (msg == WM_KEYUP))
		{
			if (CheckForegroundWindow())
			{
				HWND hOldFocused = hLastFocused;
				if (!fIsInConsole)
						hLastFocused = GetFocus();
				CheckHooks(&wKey2Counter, &wKey1Counter, &dwKey2Timer, dwKey1Timer,
					MAX_MISSING_KEYBDHOOK_EVENTS, FALSE);
				if (CheckKeyMsgDup((PMSG) lPar) && !fWaitForEndKey)

				{

					ProcessKeyEvent(((PMSG) lPar)->wParam, ((PMSG) lPar)->lParam,
						hOldFocused, FALSE);
				}
			}
		}
		else
			if ( (msg == WM_LBUTTONDOWN) || (msg == WM_RBUTTONDOWN) ||
				(msg == WM_MBUTTONDOWN) )
			{
				if (CheckForegroundWindow())
				{
					if (!fIsInConsole) 
							hLastFocused = GetFocus();
					CheckHooks(&wMouse2Counter, &wMouse1Counter, &dwMouse2Timer,
						dwMouse1Timer, MAX_MISSING_MOUSEHOOK_EVENTS, FALSE);
					PostEndMessage();
				}
			}
	}
	return CallNextHookEx(hhGetMessage, Code, wPar, lPar);
}

//----------------------------------------------------------------------------------------

#pragma optimize("", on) // ����������� �� ��������� (��������� �������)

#define SET_HOOK(handle, hook, proc) \
	if (handle == NULL) \
		handle = SetWindowsHookEx(hook, proc, hDllInstance, 0);

BOOL WINAPI SetHooks(HWND hMainWnd, BOOL fSetHooks)
{
/*	������ ��� ��������� �����; ���� fSetHooks == FALSE, �� ���� ��������� (���� ���
	�����������), ���� fSetHooks == TRUE, �� ���� �������� (���� ��� ��� �� �����������);
	� ������ ������ ������� ������ FALSE � ������ ��� ������������� ����; ��������
	hMainWnd ������ ����� �������� ����, �������� ���� ����� �������� ���������
*/
	BOOL fOk = !fSetHooks;
	// ��������� �����
	if (fSetHooks && hMainWnd)
	{
		hOwnerWindow = hMainWnd;
		GetWindowThreadProcessId(hMainWnd, &dwOwnerProcessId);
		hKbdLayout = GetKeyboardLayout(0);
		hLastForeground = HWND(-1);
		fIsOs64bits = IsOsX64();

		fSkipNextKey = fWaitForEndKey = FALSE;
		wMouse1Counter = wMouse2Counter = wKey1Counter = wKey2Counter = 0;

		SET_HOOK(hhShell, WH_SHELL, ShellHookProc);
		if (!hhShell) MessageBox(0, "!hhShell", "0", 0);
		SET_HOOK(hhMouse, WH_MOUSE, MouseHookProc);
		if (!hhMouse) MessageBox(0, "!hhMouse", "0", 0);
		SET_HOOK(hhKeyboard, WH_KEYBOARD, KeyboardHookProc);
		if (!hhKeyboard) MessageBox(0, "!hhKeyboard", "0", 0);
		SET_HOOK(hhGetMessage, WH_GETMESSAGE, GetMessageHookProc);
		if (!hhGetMessage) MessageBox(0, "!hhGetMessage", "0", 0);
		fOk = fSetHooks = (hhShell && hhMouse && hhKeyboard && hhGetMessage);


		LOG(STARTING_HOOK);
	}

	// ������ �����
	if (!fSetHooks)
	{
		hOwnerWindow = NULL;
		if (hhShell) UnhookWindowsHookEx(hhShell);
		if (hhMouse) UnhookWindowsHookEx(hhMouse);
		if (hhKeyboard) UnhookWindowsHookEx(hhKeyboard);
		if (hhGetMessage) UnhookWindowsHookEx(hhGetMessage);
		hhShell = hhMouse = hhKeyboard = hhGetMessage = NULL;

		// �������� ������� ��������� ���� top-level ����� � ������� ��� ��������
		// dll �� �������� ����������� ��������������� ���������
		PostMessage(HWND_BROADCAST, WM_NULL, 0, 0);
	}

	return fOk;
}

//----------------------------------------------------------------------------------------

BOOL WINAPI ResetHooks(void)
{
/*	������������� ���� �����; � ������ ������ ���������� ��������,
	������� ������ FALSE � ������ ��� ������������� ����
*/
	if (!hOwnerWindow) return FALSE;

	hLastForeground = HWND(-1);
	hKbdLayout = GetKeyboardLayout(0);
	fSkipNextKey = fWaitForEndKey = FALSE;
	wMouse1Counter = wMouse2Counter = wKey1Counter = wKey2Counter = 0;

	// ������������� ���� �����
	if (hhShell) UnhookWindowsHookEx(hhShell);
	hhShell = SetWindowsHookEx(WH_SHELL, ShellHookProc, hDllInstance, 0);
	if (hhMouse) UnhookWindowsHookEx(hhMouse);
	hhMouse = SetWindowsHookEx(WH_MOUSE, MouseHookProc, hDllInstance, 0);
	if (hhKeyboard) UnhookWindowsHookEx(hhKeyboard);
	hhKeyboard = SetWindowsHookEx(WH_KEYBOARD, KeyboardHookProc, hDllInstance, 0);
	if (hhGetMessage) UnhookWindowsHookEx(hhGetMessage);
	hhGetMessage = SetWindowsHookEx(WH_GETMESSAGE, GetMessageHookProc, hDllInstance, 0);

	// �������� ���������� � ������ ����� ��� ������
	BOOL fOk = (hhShell && hhMouse && hhKeyboard && hhGetMessage);
	if (!fOk) SetHooks(0, FALSE);

	return fOk;
}

//----------------------------------------------------------------------------------------

void WINAPI SuspendHooks(BOOL fSuspend)
{
/*	������������ ������ ����� ��� fSuspend == TRUE ��� ������������� ����������������
	������ ��� fSuspend == FALSE; ��� ������� ������������� ������ �� ���������������
	������ ���������� ����� (�� ����������� ���� �������� �����)
*/
	fHookSuspended = fSuspend;
	fSkipNextKey = FALSE;
}

//----------------------------------------------------------------------------------------

void WINAPI SetWaitForEndKey(BOOL fWait)
{
/*	��������� (��� fWait == TRUE) ��� �������������� ����� (fWait == FALSE)	����� ��������
	����-�������; ���� ���� ���� ����������, �� ������������ ��� �� ���������� ���������
	HOOKMSG_NRMKEY (�.�. �� ������������ ������� ������� ������, ����� �����������);
	����� ������������� ����������� �� ��������� WAITFORENDKEY_INTERVAL �� � �������
	��� ���������, ���� ��� ��������� �	������� ��������� ���� ���������� �������
	VK_ENDKEY (��� ���� �������� ���� ������������ ��������� HOOKMSG_ENDKEY)
*/
	fWaitForEndKey = fWait;
	dwWaitKeyTimer = GetTickCount() + WAITFORENDKEY_INTERVAL;
	
}