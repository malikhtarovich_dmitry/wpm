// Log.cpp

#include <Windows.h>
#include "Globals.h"
#include "Log.h"

#ifdef NEED_LOG

void WriteLog(const char* szBuf)
{
/*	����� ������ szBuf � ���� ��������� LOG_FILE_NAME
*/
	HANDLE hFile;
	SYSTEMTIME time;
	char szFileBuf[256];
	DWORD dwWsz;

	hFile = CreateFile(LOG_FILE_NAME, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		GetLocalTime(&time);
		SetFilePointer(hFile, 0, 0, FILE_END);
		wsprintf(szFileBuf, "[%2d:%02d:%02d.%03d] %s\r\n", time.wHour,
			time.wMinute, time.wSecond, time.wMilliseconds, szBuf);
		WriteFile(hFile, szFileBuf, lstrlen(szFileBuf), &dwWsz, NULL);
		CloseHandle(hFile);
	}
}

//----------------------------------------------------------------------------------------

void LogModuleFileName(void)
{
/*	����� � �������� ����� .exe ��������, ���� ���������� ���
*/
	char szName[MAX_PATH], szBuf[32 + MAX_PATH];

	if (!fOwnedDll)
	{
		GetModuleFileName(NULL, szName, sizeof(szName));
		wsprintf(szBuf, ATTACHING_TO_PROCESS, szName);
		LOG(szBuf);
	}
}


#endif // NEED_LOG