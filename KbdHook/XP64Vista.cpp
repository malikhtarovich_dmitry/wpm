// XP64Vista.cpp

#include <Windows.h>
#include <intrin.h>
#include <winnt.h>
#include "Globals.h"
#include "Log.h"

#define MSGFLT_ADD		1

typedef BOOL (WINAPI *LPFN_CHANGEWINDOWMESSAGEFILTER) (
	UINT message,
	DWORD dwFlag);

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (
	HANDLE hProcess,
	PBOOL Wow64Process);

typedef BOOL (WINAPI *LPFN_OPENPROCESSTOKEN) (
	HANDLE ProcessHandle,
	DWORD DesiredAccess,
	PHANDLE TokenHandle);

typedef BOOL (WINAPI *LPFN_GETTOKENINFORMATION)	(
	HANDLE TokenHandle,
	/*MY*/_TOKEN_INFORMATION_CLASS TokenInformationClass,
	LPVOID TokenInformation,
	DWORD TokenInformationLength,
	PDWORD ReturnLength);

static LPFN_CHANGEWINDOWMESSAGEFILTER	fnChangeWindowMessageFilter	= NULL;
static LPFN_ISWOW64PROCESS				fnIsWow64Process			= NULL;
static LPFN_OPENPROCESSTOKEN			fnOpenProcessToken			= NULL;
static LPFN_GETTOKENINFORMATION			fnGetTokenInformation		= NULL;

//----------------------------------------------------------------------------------------

static void LoadNewNtFunctions(void)
{
/*	�������� ���� ������������� NT-������� fnXXX (���������� ����������)
*/
	static BOOL bNewNtFunctionsLoaded = FALSE;
	if (bNewNtFunctionsLoaded) return;
	bNewNtFunctionsLoaded = TRUE;

	HMODULE hAdvapi32 = LoadLibrary("Advapi32.dll");
	HMODULE hKernel32 = GetModuleHandle("Kernel32.dll");
	HMODULE hUser32 = GetModuleHandle("User32.dll");

	fnChangeWindowMessageFilter = (LPFN_CHANGEWINDOWMESSAGEFILTER)
		GetProcAddress(hUser32, "ChangeWindowMessageFilter");
	fnIsWow64Process = (LPFN_ISWOW64PROCESS)
		GetProcAddress(hKernel32, "IsWow64Process");
	fnOpenProcessToken = (LPFN_OPENPROCESSTOKEN)
		GetProcAddress(hAdvapi32, "OpenProcessToken");
	fnGetTokenInformation = (LPFN_GETTOKENINFORMATION)
		GetProcAddress(hAdvapi32, "GetTokenInformation");
}

//----------------------------------------------------------------------------------------

BOOL WINAPI IsOSVistaOrHigher(void)
{
/*	��������, �������� �� �� Windows Vista ��� ����; ������� ���������� TRUE,
	���� ��� �� Vista ��� ����, � ���������� FALSE, ���� Win2003 ��� ����
*/
	OSVERSIONINFO osver;

	osver.dwOSVersionInfoSize = sizeof(osver);
	return (GetVersionEx(&osver) && (osver.dwMajorVersion >= 6) &&
		(osver.dwPlatformId == VER_PLATFORM_WIN32_NT));
}

//----------------------------------------------------------------------------------------

BOOL WINAPI SetupVistaUipiFilter(void)
{
/*	��������� ������� ��������� UIPI ��� OS Vista � ����; ����� ���� ������� �������������
	��������� ������ ��� ����� ���������� ����� (�.�. ����� ������� SetHooks); ����� ����
	������� ��������� ����� ������� ���������� ��������� �� �����, ���������� � ��������
	� low execution level � low-integrity �������� (�������� IE7); ������� ������ TRUE,
	���� �� ���� ��� Vista, � ����� � ������ �������� ��������� ������� ���������;
	������� ������ FALSE � ������ ������
*/
	if (!IsOSVistaOrHigher()) return TRUE;

	LoadNewNtFunctions();
	if (fnChangeWindowMessageFilter)
	{
		BOOL fOk1 = fnChangeWindowMessageFilter(dwKbdHookMsg, MSGFLT_ADD);
		BOOL fOk2 = fnChangeWindowMessageFilter(WM_CLOSE, MSGFLT_ADD);
		BOOL fOk3 = fnChangeWindowMessageFilter(WM_COMMAND, MSGFLT_ADD);
		return (fOk1 && fOk2 && fOk3);
	}
	return FALSE;
}

//----------------------------------------------------------------------------------------

BOOL WINAPI IsOsX64(void)
{
/*	��������, �������� �� ������� ������� ��� ����������� 64-������ ��; �������
	������ TRUE, ���� ��� 64-������ �������, ��� ���� 32-������, ���������� �
	������ WOW64; ������� ������ FALSE � ������ ������ ��� ���� ������� �������
	�������� ��� ����������� 32-������ ��
*/
#ifdef _WIN64
	return TRUE;
#else
	BOOL bIsWow64 = FALSE;

	LoadNewNtFunctions();
	if (fnIsWow64Process)
		if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64))
			bIsWow64 = FALSE;
	return bIsWow64;
#endif
}

//----------------------------------------------------------------------------------------

BOOL WINAPI IsProcessElevated(HANDLE hProcess, BOOL* pfElevated)
{
/*	��������, ����� �� ��������� ������� ������ elevated; ��� �� ���� Windows
	Vista, � ����� � ������ ������ ������� ���������� FALSE; � ������ ������
	������� ���������� TRUE, � � ��������� pfElevation - ������ ��������
*/
	HANDLE hToken;
	TOKEN_ELEVATION te;
	DWORD dwReturnLength;
	BOOL fOk = FALSE;

	*pfElevated = FALSE;
	LoadNewNtFunctions();
	if (IsOSVistaOrHigher() && fnOpenProcessToken && fnGetTokenInformation)
		if (fnOpenProcessToken(hProcess, TOKEN_QUERY, &hToken))
		{
			if ( fnGetTokenInformation(hToken, TokenElevation,
				&te, sizeof(te), &dwReturnLength) )
			{
				*pfElevated = (te.TokenIsElevated != 0);
				fOk = TRUE;
			}
			CloseHandle(hToken);
		}
	return fOk;
}

//----------------------------------------------------------------------------------------

static BOOL __inline IsWeElevated(void)
{
/*	��������, ����� �� ������� ������� ������ elevated; ������� ����������
	FALSE � ������ ���� �� ���� ��� Vista, � ������ ������ ��� ���� �������
	�� �������� elevated; ������� ���������� TRUE, ���� ������� elevated
*/
	BOOL fElevated;

	IsProcessElevated(GetCurrentProcess(), &fElevated);
	return fElevated;
}

//----------------------------------------------------------------------------------------

static BOOL __fastcall FileExists(const char* pszFileName)
{
/*	�������� ������������� ����� � ������ pszFileName; ���� ���� ����������,
	������� ������ TRUE, � ������ FALSE, ���� ���������� ����� ���
*/
	DWORD Code = GetFileAttributes(pszFileName);
	BOOL fExists = ( (Code != INVALID_FILE_ATTRIBUTES) &&
		((Code & FILE_ATTRIBUTE_DIRECTORY) == 0) );
	return fExists;
}

//----------------------------------------------------------------------------------------

#define ZEROSTRUC_MACRO(dest) \
	__stosd((DWORD*) &dest, 0, sizeof(dest)/4); \
	if ((sizeof(dest) & 3) >= 2) ((WORD*) &dest)[sizeof(dest)/2-1] = 0; \
	if ((sizeof(dest) & 3) == 3) ((BYTE*) &dest)[sizeof(dest)-1] = 0;

static BOOL __fastcall MyShellExecute(HWND hWnd, LPCSTR szVerb, LPCSTR szFile,
									  LPCSTR szParameters, LPCSTR szWorkPath)
{
/*	������ ����� pszFile ����� ShellExecute; ������� ���������� TRUE � ������
	��������� �������, � FALSE � ������ ������ (��� ���� � dwRunAppError
	����������� ��� ������ GetLastError()
*/
	SHELLEXECUTEINFO shex;
	ZEROSTRUC_MACRO(shex);

	shex.cbSize			= sizeof(shex);
	shex.hwnd			= hWnd;
	shex.lpVerb			= szVerb;
	shex.lpFile			= szFile;
	shex.lpParameters	= szParameters;
	shex.lpDirectory	= szWorkPath;
	shex.nShow			= SW_NORMAL;

	dwRunAppError = ERROR_SUCCESS;
	BOOL fOk = ShellExecuteEx(&shex);
	if (!fOk) dwRunAppError = GetLastError();
	return fOk;
}

//----------------------------------------------------------------------------------------

#pragma optimize("gt", on) // ��������� ����������� "�� ��������"

static LRESULT CALLBACK RunAppHookProc(int Code, WPARAM wPar, LPARAM lPar)
{
/*	���������� ���� WH_CALLWNDPROCRET, ���������������� ��� �������
	���������� ����� RunApplication() � ��������� �������� Shell
*/
	if (Code == HC_ACTION)
	{
		CWPRETSTRUCT *msg = (CWPRETSTRUCT*) lPar;

		if (msg->message == dwElevatorMsg)
			bRunAppSuccess = MyShellExecute(msg->hwnd, "open", szRunAppFile,
				szRunAppParameters, szRunAppWorkPath);
	}
	return CallNextHookEx(hhElevator, Code, wPar, lPar);
}

//----------------------------------------------------------------------------------------

#pragma optimize("", on) // ����������� �� ��������� (��������� �������)

DWORD WINAPI RunApplication(BOOL bDefaultElevation, BOOL bRunElevated, HWND hWnd,
							LPCSTR szFile, LPCSTR szParameters, LPCSTR szWorkPath)
{
/*	������ ���������� szFile � execution level �������� �������� ��� bDefaultElevation
	������ TRUE, ���� ������ � �������� � bRunElevated elevation-�������� �����; �
	������ ������ ������� ������ ERROR_SUCCESS, � ������ ��� ������ � ������ ������;
	���� ������� ������� �������� 32-������ WOW64 elevated ��������� (�.�. ����������
	��� ����������� 64-������ ��), �� ������� ������� ���������� ��� non-elevated
	�������� ������� � ������� ������ ��� ������ ERROR_EXE_MACHINE_TYPE_MISMATCH
*/
	if (!szFile || !szFile[0] || !FileExists(szFile))
		return ERROR_FILE_NOT_FOUND;

	// ������ ���������� � ��� �� execution level, ��� � � ��������
	// ��������, ��� ������ ���������� �� ��� �� ����, ��� �� Vista
	if (bDefaultElevation || !IsOSVistaOrHigher() || (bRunElevated == IsWeElevated()))
	{
		MyShellExecute(hWnd, "open", szFile, szParameters, szWorkPath);
		return dwRunAppError;
	}

	// ������ �������� ��� elevated
	if (bRunElevated)
	{
		MyShellExecute(hWnd, "runas", szFile, szParameters, szWorkPath);
		return dwRunAppError;
	}

	// ������ �������� ��� "non-elevated" �� ��������� �������� Shell
#ifndef _WIN64
	// � 64-������ �� Shell �������� 64-������ ���������, ������� ���� ��
	// ����������� ��� 32-������ ����������, �� �� ������ ���������� ���
	// ��� ������� non-elevated �������� �� ��������� 64-������� Shell
	if (IsOsX64()) return ERROR_EXE_MACHINE_TYPE_MISMATCH;
#endif

	// ��������� ������ ���� Shell
	HWND hShellWnd = FindWindow("Progman", NULL);
	if (!hShellWnd) return GetLastError();

	// ����������� ����������� ��������� ��� ������ ����
	dwElevatorMsg = RegisterWindowMessage("M_SndPltRnElvtr");
	if (!dwElevatorMsg) return GetLastError();

	// ��������� ���� WH_CALLWNDPROCRET
	hhElevator = SetWindowsHookEx(WH_CALLWNDPROCRET,
		(HOOKPROC) RunAppHookProc, hDllInstance, 0);
	if (!hhElevator) return GetLastError();

	// ����������� ���������� ������� ���������� � .shared
	lstrcpyn(szRunAppFile, szFile, sizeof(szRunAppFile));
	lstrcpyn(szRunAppParameters, (szParameters ? szParameters : ""),
		sizeof(szRunAppParameters));
	lstrcpyn(szRunAppWorkPath, (szWorkPath ? szWorkPath : ""), sizeof(szRunAppWorkPath));

	// ����� ����� � �������� ��������� Shell; ����� ������ ����������
	// ���� AppRunHookProc, � ������� � ����� �������� ���� ����������
	bRunAppSuccess = FALSE;
	dwRunAppError = ERROR_INTERNAL_ERROR;
	SendMessage(hShellWnd, dwElevatorMsg, 0, 0);

	// ������ ������ ����
	UnhookWindowsHookEx(hhElevator);
	hhElevator = NULL;

	// ������� ������� ��������� ���� top-level ����� � ������� ���
	// �������� dll �� �������� ����������� ��������������� ���������
	PostMessage(HWND_BROADCAST, WM_NULL, 0, 0);

	return dwRunAppError;
}