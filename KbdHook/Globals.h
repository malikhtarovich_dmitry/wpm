/*! @file Globals.h */

/**
 * @addtogroup KbdHook
 * @brief Global variables
 * @{
 */

#ifdef INCLUDED_IN_MAIN_CPP
	#define GDECL(type, name, val) type name = val
	#define GDEXT
#else
	#define GDECL(type, name, val) extern type name
	#define GDEXT extern
#endif

/**
 * ����������, ����������� ����� ����� ����������, � ������� ������������ ������ dll.
 * ���������� ����������� ������ ������������������ ����������, ����� ������ ������
 * .shared ���������� MSVC ��������� �� � ������ .data
 */

#pragma data_seg(".shared")

GDECL(HWND,  hOwnerWindow,     NULL);  // ����� ���� ��� �������� ���������
GDECL(DWORD, dwOwnerProcessId, 0);     // ID ��������, ������������� ����
GDECL(HWND,  hLastForeground,  NULL);  // ����� �������� foreground-����
GDECL(HWND,  hLastFocused,     NULL);  // ����� �������� �������� � �������
GDECL(BOOL,  fIsInConsole,     FALSE); // ���� ������ � ���� ����������� ����������
GDECL(BOOL,  fOwnedDll,        FALSE); // ���� ������������ ����� � "�����" ��������
GDECL(BOOL,  fIsOs64bits,      FALSE); // ���� ������ ��� 64-������ ��

GDECL(BOOL,  fSkipNextKey,     FALSE); // ���� �������� ��������� ������
GDECL(BOOL,  fHookSuspended,   FALSE); // ���� "������������ ������" �����
GDECL(BOOL,  fWaitForEndKey,   FALSE); // ���� �������� "�����������" �������
GDECL(DWORD, dwWaitKeyTimer,   0);     // ����� ��������� �������� "�����������" �������

GDECL(DWORD, dwLastMsgTime,    0); // ����� ����������� ����������� ��������� WM_KEYxxx
GDECL(DWORD, dwLastVirtualKey, 0); // ��� ��������� ����������� ����������� �������

GDECL(HHOOK, hhShell,          NULL); // ����� ���� WH_SHELL
GDECL(HHOOK, hhMouse,          NULL); // ����� ���� WH_MOUSE
GDECL(HHOOK, hhKeyboard,       NULL); // ����� ���� WH_KEYBOARD
GDECL(HHOOK, hhGetMessage,     NULL); // ����� ���� WH_GETMESSAGE
GDECL(HHOOK, hhElevator,       NULL); // ����� ���� ��� RunApplication()

GDECL(WORD,  wMouse1Counter,   0); // ������� ������������ ���� WH_MOUSE
GDECL(WORD,  wMouse2Counter,   0); // �� ��, �� ��� �������� � WH_GETMESSAGE
GDECL(WORD,  wKey1Counter,     0); // ������� ������������ ���� WH_KEYBOARD
GDECL(WORD,  wKey2Counter,     0); // �� ��, �� ��� �������� � WH_GETMESSAGE

GDECL(DWORD, dwMouse1Timer,    0); // ����� ���������� ����� ��� WH_MOUSE
GDECL(DWORD, dwMouse2Timer,    0); // �� ��, �� ��� �������� � WH_GETMESSAGE
GDECL(DWORD, dwKey1Timer,      0); // ����� ���������� ����� ��� WH_KEYBOARD
GDECL(DWORD, dwKey2Timer,      0); // �� ��, �� ��� �������� � WH_GETMESSAGE

GDECL(UINT,  dwElevatorMsg,    0);     // ��������� ��� RunApplication()
GDECL(BOOL,  bRunAppSuccess,   FALSE); // ���� ��������� ������ ���������� � ����
GDECL(DWORD, dwRunAppError,    0);     // ��� GetLastError ��� ������ �������

GDECL(CHAR,  szRunAppFile[MAX_PATH],       ""); // ��� ������������ ����������
GDECL(CHAR,  szRunAppParameters[MAX_PATH], ""); // ��������� ��������� ������
GDECL(CHAR,  szRunAppWorkPath[MAX_PATH],   ""); // ������� �����

#pragma data_seg()
#pragma comment(linker, "/SECTION:.shared,RWS")

/**
 * ���������� ���������� ������ .data (���� ��� ������� ��������)
 */

GDEXT UINT		dwKbdHookMsg; // ��������� ��� ������ � hOwnerWindow
GDEXT HINSTANCE	hDllInstance; // ����������� Instance ���� dll
GDEXT HKL		hKbdLayout;   // ������� Keyboard layout

#undef GDECL
#undef GDEXT

/*! @} */