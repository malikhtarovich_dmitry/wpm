/*! @file Log.h */

/**
 * @addtogroup KbdHook
 * @brief Additional macros and functions for logging hooks
 * @{
 */

// ��������� #define NEED_LOG ��������� ���������������� ������ ���� � ����
// ������� LOG_FILE_NAME; NEED_LOG ������ ���� �������� � ������
#undef NEED_LOG

// ��� � ������ ���� � ����� ��������� ������ ����
#define LOG_FILE_NAME "C:\\Dev\\KbdHookDll.log"

#ifndef _WIN64
	// ��������� ��������� (#define) ��� x86 ������ dll
	#define STARTING_HOOK			"Setting x86 hooks from KbdHook.dll..."
	#define ATTACHING_TO_PROCESS	"Attaching to x86 process %s"
#else
	// ��������� ��������� (#define) ��� x64 ������ dll
	#define STARTING_HOOK			"Setting x64 hooks from KbdHook64.dll..."
	#define ATTACHING_TO_PROCESS	"Attaching to x64 process %s"
#endif

#ifdef NEED_LOG
	#define LOG(str) WriteLog(str)
	#define LOG_IF_FALSE(res, str) if (!res) LOG(str)
	#define LOG_MODULE_FILE_NAME() LogModuleFileName()
#else
	#define LOG(str)
	#define LOG_IF_FALSE(res, str)
	#define LOG_MODULE_FILE_NAME()
#endif

#ifdef NEED_LOG



/*
	����� ������ szBuf � ���� ��������� LOG_FILE_NAME
*/
void WriteLog(const char* szBuf);

/*
	����� � �������� ����� .exe ��������, ���� ���������� ���
*/
void LogModuleFileName(void);

#endif // NEED_LOG

/*! @} */