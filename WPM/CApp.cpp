
#include "StdAfx.h"

#include "resource.h"
#include "Help.h"
#include "Registry.h"
#include "Utils.h"
#include "..\HookProc\HookProc.h"
#include "..\KbdHook\KbdHook.h"
#include "TrayIcon.h"
#include "Lang.h"
#include "DlgMain.h"
#include "CApp.h"
#include <Windows.h>
#include "GlobalData.h"
#include "UsageStat.h"
#include "Lang.h"
#include "CFakeWindow.h"

//----------------------------------------------------------------------------------------

#define FOCUS_TIMER_ID 0x1B73 // Id ������� ������������ ����� ��������� ���� � ������

//----------------------------------------------------------------------------------------

static CApp *pApp; // C����� �� ��������� ������ CApp

CApp *GetApp(void)
{
/*	��������� ������ �� ��������� ������ CApp; ������ ����������
	�������������� ������ ����� ������ ����������� � WinMain()
*/
	return pApp;
}

//----------------------------------------------------------------------------------------

static LRESULT CALLBACK MainWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
/*	������� ��������� �������� ���� ���������
*/
	return pApp->WndProc(hWnd, uMsg, wParam, lParam);
} 

//----------------------------------------------------------------------------------------
// ���������� ������ CApp
//----------------------------------------------------------------------------------------

CApp::CApp(void)
{
	pApp = this;

	GetModuleFileName(NULL, m_szExeFileName, sizeof(m_szExeFileName));

	VerToLog();
	fMMF = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024, 
		"BKA_WPMPRG_MMF");
	fAlreadyRun = (GetLastError() == ERROR_ALREADY_EXISTS);

	m_dwFocusTimer = 0;
	m_hHookProcWnd = NULL;
	m_dwHookProcMsg = 0;
	m_dwKbdHookMsg = 0;
	m_dwTaskbarCreatedMsg = 0;
	m_fHooksSet = FALSE;

	m_hkCapture = 'Z';
	m_hkZoomOut = 'A';
	m_hkZoomIn = 'D';
	m_hkRelease = 'Q';
	mSettingsStillOpen = false;

	WPControl::Instance()->InitCmdKeys(m_hkCapture, m_hkZoomIn, m_hkZoomOut, m_hkRelease);

	SetLastForegroundWindow(GetForegroundWindow());
	SetLastFocusWindow(GetForegroundWindow());
}

CApp::~CApp(void)
{
	if (mLogOn == true){
		LOG_TRACE
	}
	FreeHelp();
	if (fMMF) CloseHandle(fMMF);
}

void CApp::VerToLog()
{
	char StrLog [256], *pWin;

	//�������� ������ ���������	
	char FileName[MAX_PATH]; 
	if (GetModuleFileName(NULL, FileName, sizeof(FileName)))
	{
		DWORD dwVerHi, dwVerLo;
		GetFileVersion(FileName, &dwVerHi, &dwVerLo);
		fdwVer = HIWORD(dwVerHi);
		fdwRelease = LOWORD(dwVerHi);
		fdwBuild = LOWORD(dwVerLo);
	}
	else
		fdwVer = fdwRelease = fdwBuild = 0;

	//�������� ������ �����
	OSVERSIONINFO osvi;
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx (&osvi);
	switch (osvi.dwPlatformId)
	{
		case VER_PLATFORM_WIN32_NT:
			pWin = "NT";
			break;
		case VER_PLATFORM_WIN32_WINDOWS:
			pWin = "9x";
			break;
		case VER_PLATFORM_WIN32s:
			pWin = "3.1";
			break;
	}
	SetFlagUnicode(osvi.dwPlatformId == VER_PLATFORM_WIN32_NT);
	char sTitle[MAXRES];
	LoadString(GetModuleHandle(NULL), IDS_APP_TITLE, sTitle, sizeof(sTitle));
	wsprintf (StrLog, "%s %d.%d.%d / Windows %s %d.%d.%d %s", 
		sTitle,	fdwVer, fdwRelease, fdwBuild,
		pWin, osvi.dwMajorVersion, osvi.dwMinorVersion, osvi.dwBuildNumber, osvi.szCSDVersion);
}

void MimimizeApp(HWND hWnd)
{
    DefWindowProc(hWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
	SetWindowPos(hWnd, 0, 0, 0, 100, 100, SWP_NOSIZE | SWP_NOMOVE |
		SWP_NOZORDER | SWP_NOACTIVATE | SWP_HIDEWINDOW);
}

bool CApp::Init(HINSTANCE hAInstance, LPSTR pszACmdLine)
{
	m_hInstance = hAInstance;
	m_pszCommandLine = pszACmdLine;

	m_fIsOsX64 = IsOsX64();

	fActiveWnd = NULL;
	LoadStringLangA(IDS_APP_TITLE, m_szTitle, sizeof(m_szTitle));

	if (!InitInstance() || !CheckAlreadyRun())
		return false;
	
	InitHelp();

	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(icc); 
	icc.dwICC = ICC_BAR_CLASSES|ICC_DATE_CLASSES|ICC_HOTKEY_CLASS|ICC_UPDOWN_CLASS|
		ICC_LISTVIEW_CLASSES|ICC_PROGRESS_CLASS|ICC_TREEVIEW_CLASSES | ICC_WIN95_CLASSES;
	InitCommonControlsEx(&icc);
	InitCommonControls(); 

	GetTrayIcon()->SetWndOwnerTrayIcon(m_hMainWnd);
	GetTrayIcon()->Show();
	SetGlobData_MainWnd(m_hMainWnd);
	GetDlgMain()->Init(m_hMainWnd);
	RegisterMessages();
	MimimizeApp(m_hMainWnd);
	mHPopupMenu = LoadMenu(GetInstLang(), MAKEINTRESOURCE(IDR_POPUP_MENU));
	mAlreadyAppended = false;
	mMuted = false;
	mStartup = GetStartUp();
	mLogOn = GetLogInfo();
	char *fullPath = new char[MAX_PATH+1];
	char *driveLetter = new char[3];
	char *directory = new char[MAX_PATH-2];
	char *szPath= new char[MAX_PATH+1];
	GetModuleFileName(NULL, fullPath, MAX_PATH);
	_splitpath(fullPath, driveLetter, directory, NULL, NULL);
	strcpy(szPath, driveLetter);
	strcat(szPath, directory);
	strcat(szPath, "log.txt");
	if (mLogOn == false && (access(szPath, 0) == -1))
		SetLogInfo(false);
	if (mLogOn == true){
		LOG_TRACE
	}
	return true;
}

bool CApp::InitInstance()
{
	if (mLogOn == true){
		LOG_TRACE
	}
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX); 
	wcex.style			= 0;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC) MainWindowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0; 
	wcex.hInstance		= m_hInstance;
	// TODO: fix HICON init
	wcex.hIcon			= (HICON)LoadImage(m_hInstance, (LPCTSTR)(IDI_TRAY_ICON), IMAGE_ICON, 16, 16, 0);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName	= NULL; 
	wcex.lpszClassName	= "BKA_WPMPRG_MAINWND";
	wcex.hIconSm		= (HICON)LoadImage(m_hInstance, (LPCTSTR)(IDI_TRAY_ICON), IMAGE_ICON, 16, 16, 0);//(HICON)MAKEINTRESOURCE(IDI_TRAYICON);
	m_hMainWnd = CreateWindow((char*) RegisterClassEx(&wcex), m_szTitle, 
		WS_POPUP | WS_CAPTION | WS_CLIPSIBLINGS | WS_SYSMENU | WS_MINIMIZEBOX,
		GetSystemMetrics(SM_CXSCREEN) / 2,
		GetSystemMetrics(SM_CYSCREEN) / 2,
		0, 0, NULL, NULL, m_hInstance, NULL);
	if (!m_hMainWnd) return false;
	HMENU vSysMenu = GetSystemMenu(m_hMainWnd, FALSE);
    DeleteMenu(vSysMenu, SC_MAXIMIZE, MF_BYCOMMAND);
    DeleteMenu(vSysMenu, SC_SIZE, MF_BYCOMMAND);
    DeleteMenu(vSysMenu, SC_MOVE, MF_BYCOMMAND);
	return true;
}

//----------------------------------------------------------------------------------------

void CApp::RegisterMessages(void)
{
/*	����������� ��������� ��� �������������� � KbdHook.dll � HookProc.exe,
	� ����� ��������� TaskbarCreated; ��� ��������� ��� KbdHook.dll
*/
	DWORD dwCrc = 0;
	char szMessage[10] = {'M'};
	char FileName [255];

	GetModuleFileName (0, &FileName[0], 255);
	wsprintf(&szMessage[1], "%.8X", dwCrc);
	m_dwKbdHookMsg = RegisterWindowMessage("NEW_HOOK_SYSTEM");
	m_dwHookProcMsg = RegisterWindowMessage(HOOKPROC_MSGNAME);
	m_dwTaskbarCreatedMsg = RegisterWindowMessage("TaskbarCreated");

}

//----------------------------------------------------------------------------------------

BOOL CApp::HkSetHooks(void)
{
/*	��������� �����; � ������ ������ ������� ������ TRUE, � ������
	������ ������� ������ FALSE, � ��� ���� ����� �����
*/
	if (mLogOn == true){
		LOG_TRACE
	}
	if (m_fHooksSet) return TRUE;

	// ��������� Vista UIPI
	SetupVistaUipiFilter();

	// ��������� 32-������ (x86) �����
	m_fHooksSet = SetHooks(m_hMainWnd, TRUE);

	// ��������� 64-������ (x64) ����� 
	if (m_fHooksSet && m_hHookProcWnd)
	{
		// �������� HookProc.exe ������� �� ��������� x64 �����
		m_fHooksSet = ( HIWORD(SendMessage(m_hHookProcWnd, m_dwHookProcMsg,
			HOOKPROC_CMD_SET, TRUE)) == HOOKPROC_RES_OK );

		// ������ 32-������ ����� � ������ ������
		if (!m_fHooksSet) 
			SetHooks(NULL, FALSE);
	}

	return m_fHooksSet;
}

//----------------------------------------------------------------------------------------

void CApp::HkUnsetHooks(void)
{
/*	������ ���� �����, ���� ��� �����������
*/
	if (m_fHooksSet)
	{
		m_fHooksSet = FALSE;

		// ������ 32-������ �����
		SetHooks(NULL, FALSE);

		// ������ 64-������ �����
		if (m_hHookProcWnd)
			SendMessage(m_hHookProcWnd, m_dwHookProcMsg, HOOKPROC_CMD_SET, FALSE);
	}
}

//----------------------------------------------------------------------------------------

BOOL CApp::HkResetHooks(void)
{
/*	������������� �����; ������� ������ TRUE � ������ ������; �
	������ ������ ������� ������ FALSE, � ��� ���� ����� �����
*/
	if (!m_fHooksSet) return FALSE;

	// ������������� 32-������ �����
	m_fHooksSet = ResetHooks();

	// ������������� 64-������ �����
	if (m_hHookProcWnd)
		if (m_fHooksSet)
		{
			// ������������� 64-������ �����
			m_fHooksSet = ( HIWORD(SendMessage(m_hHookProcWnd, m_dwHookProcMsg,
				HOOKPROC_CMD_RESET, 0)) == HOOKPROC_RES_OK);

			// ������ 32-������ ����� � ������ ������ ������������� 32-������
			if (!m_fHooksSet)
				SetHooks(NULL, FALSE);
		}
		else
			// ������ 64-������ ����� � ������ ������ ������������� 32-������
			SendMessage(m_hHookProcWnd, m_dwHookProcMsg, HOOKPROC_CMD_SET, FALSE);

	return m_fHooksSet;
}

//----------------------------------------------------------------------------------------

void CApp::HkSuspendHooks(BOOL fSuspend)
{
/*	������������ ������ ����� ��� fSuspend == TRUE ��� �������������
	���������������� ������ ��� fSuspend == FALSE
*/
	SuspendHooks(fSuspend);
	if (m_hHookProcWnd)
		SendMessage(m_hHookProcWnd, m_dwHookProcMsg, HOOKPROC_CMD_SUSPEND,
			(LPARAM) fSuspend);
}

//----------------------------------------------------------------------------------------
//
// ���������� ��������� �� KbdHook.dll
//
void CApp::HandleHook(WPARAM wPar, LPARAM lPar)
{
	// TODO: rework for new target
	static HWND LastFocusWndKbd = NULL;

	switch (HIWORD(wPar))
	{ 
	case HOOKMSG_NRMKEY:
		{
		SetLastFocusWindow((HWND) lPar);
		char buf[256] = {0};
		char *str = itoa(int(HIBYTE(wPar)),buf,10);
		//MessageBox(NULL,str,"KbdHook", NULL);
		// TODO: Handle Window Allocation
		HWND focus = GetForegroundWindow();
		WPControl::Instance()->allocateWindow(m_hInstance, focus, wPar);
		//SendMessage(focus, WM_WINDOWPOSCHANGED , NULL, (LPARAM)&wp);
		if (LastFocusWndKbd != (HWND) lPar)
		{
			SetLastFocusWindow((HWND) lPar);
			LastFocusWndKbd = (HWND) lPar;
		}
		}
		break;
	case HOOKMSG_ENDKEY:
		// ������ ������ �������� ����������� ������� � ����� (������������� �����
		// ��������� ������ � ��� �����, ����������� ������� ��������� � ������������
		// ����������, �������� ����� ����� �� ������ ��������������� ������ ������)
		if (LastFocusWndKbd != (HWND) lPar)
		{
			SetLastFocusWindow((HWND) lPar);
			LastFocusWndKbd = (HWND) lPar;
		}
		break;
	case HOOKMSG_WNDFRG:
		SetLastForegroundWindow((HWND) lPar);
		LastFocusWndKbd = NULL;
		break; 
	case HOOKMSG_REHOOK:
		if (m_fHooksSet) HkResetHooks();
		break;
	case FOCUS_TIMER_ID:
		// ��������� ������� �������
		HWND hFocus = NULL;
		HWND hForeground = GetForegroundWindow();
		DWORD dwThId = GetWindowThreadProcessId(hForeground, NULL);
		GUITHREADINFO ThInfo;
		ThInfo.cbSize = sizeof(ThInfo);
		if (GetGUIThreadInfo(dwThId, &ThInfo))
			hFocus = ThInfo.hwndFocus;
		SetLastForegroundWindow(hForeground);
		if (hFocus != LastFocusWndKbd)
		{
			SetLastFocusWindow(hFocus);
			LastFocusWndKbd = hFocus;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void CApp::OnCopyData(LPARAM ALPar)
{
}

bool CApp::CheckAlreadyRun()
{
	bool res = true;
	if (fAlreadyRun)
	{
		if (lstrlen(m_pszCommandLine) <= 0)
		{
			//��������� ��� ��������, ���������� ������ �����
			LPVOID pMem = MapViewOfFile(fMMF, FILE_MAP_READ, 0, 0, 0);
			if (pMem)
			{
				HWND hFirstWnd = *(HWND*)pMem;
				SetForegroundWindow(hFirstWnd);
				UnmapViewOfFile(pMem);
			}
		}
		res = false;
	}
	else
	{
		LPVOID pMem = MapViewOfFile(fMMF, FILE_MAP_WRITE, 0, 0, 0);
		if (pMem) *(HWND*)pMem = m_hMainWnd, UnmapViewOfFile (pMem);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

void CApp::UpdatePopupMenu(HMENU hmenu)
{ 
	HMENU hmenu1;
	hmenu1 = GetSubMenu(hmenu, 7);
	if (mStartup){
		CheckMenuItem(hmenu, ID_STARTUP, MF_BYCOMMAND|MF_CHECKED);		
	}else{
		CheckMenuItem(hmenu, ID_STARTUP, MF_BYCOMMAND|MF_UNCHECKED);
	}
};

void CApp::ShowTourOnRun()
{
}

void CApp::DisableOrEnable()
{
	if (m_fHooksSet) HkUnsetHooks(); else HkSetHooks();
	GetTrayIcon()->Modify(m_fHooksSet);
}

void CApp::HandleMenu(HWND hWnd, WORD wmId)
{
	// TODO: rework this code
	switch (wmId)
	{
	case ID_ABOUT:
			MessageBox(hWnd, "Version 1.0.0 (june, 2015) \nCopyright � 2015 Dmitry Malikhtarovich\n https://www.facebook.com/dimrandom ","About", MB_ICONINFORMATION);
		    //dlgAbout.DoModal(hWnd);
			//GetDlgAbout()->Execute(NULL);
			break;
	case ID_HELP:
		CallHelp (hWnd, "intro.htm");
		break;
	case ID_EXIT: PostQuitMessage(0); break;
	case ID_SETTINGS:
			if (!mSettingsStillOpen){
				mSettingsStillOpen = true;
				dlgSettings.InitValues(m_hkCapture, m_hkZoomIn, m_hkZoomOut, m_hkRelease);
				dlgSettings.DoModal(hWnd);
				m_hkCapture = dlgSettings.getHKCapture();
				m_hkZoomIn = dlgSettings.getHKZoomIn();
				m_hkZoomOut = dlgSettings.getHKZoomOut();
				m_hkRelease = dlgSettings.getHKRelease();
				WPControl::Instance()->InitCmdKeys(m_hkCapture, m_hkZoomIn, m_hkZoomOut, m_hkRelease);
				mSettingsStillOpen = false;
			}
			break;
	case ID_STARTUP:
			{
			mStartup = !mStartup;
			HMENU hmenu;
			hmenu =GetSubMenu(mHPopupMenu, 0);
			if (mStartup){
				CheckMenuItem(hmenu, ID_STARTUP, MF_BYCOMMAND|MF_CHECKED);
			}
			else{
				CheckMenuItem(hmenu, ID_STARTUP, MF_BYCOMMAND|MF_UNCHECKED);
			}
			SetStartUp(mStartup);
			}
			break;
		default:
			break;
	}
}

//----------------------------------------------------------------------------------------

void CApp::OnPowerBroadcast(WPARAM wParam)
{
/*	������� ��������� �� "�����������" ���������� (�������������� ��� ������
	"���������", ����������� (����� ������� �� ���������� PBT_APMSUSPEND) � �������
	(������������ ������������� ��� ���������� � ������� ������); �� ������ ������
	������������������� ���� ������� ������ ������
*/
	if ((wParam == PBT_APMRESUMECRITICAL) || (wParam == PBT_APMRESUMESUSPEND))
	{
		if (m_fHooksSet)
		{
			m_fHooksSet = ResetHooks();
		}
	}
}

//----------------------------------------------------------------------------------------

LRESULT CApp::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if ((uMsg == m_dwKbdHookMsg) && m_dwKbdHookMsg)
	{
		HandleHook(wParam, lParam);
		return 0;
	}
	if ((uMsg == m_dwTaskbarCreatedMsg) && m_dwTaskbarCreatedMsg)
	{
		GetTrayIcon()->Hide(); 
		GetTrayIcon()->Show();
		return 0;
	}
	if (uMsg == m_dwHookProcMsg)
	{
		m_hHookProcWnd = (HWND) wParam;
		return 0;
	}

	switch (uMsg)
	{
		case WM_COMMAND:
			HandleMenu(hWnd, LOWORD(wParam));
			break;
		case WM_MENUSELECT:
			WORD wmId; wmId = LOWORD(wParam);
			// TODO: nothing yet
			break;
		case WM_HELP:
			/*HELPINFO *lphi;	lphi = (LPHELPINFO) lParam;  
			if (lphi->iContextType == HELPINFO_MENUITEM) 
				CallHelp(hWnd, "menu.htm");
			else 
				CallHelp(hWnd, "intro.htm");*/
			break;
		case WM_TRAYICON:				
 			HMENU hmenu; 
			switch (lParam)
			{ 
			case WM_LBUTTONDOWN: break;
			case WM_LBUTTONUP: SetForegroundWindow(GetLastActiveWindow()); break;
			case WM_RBUTTONDOWN: 
				break;
			case WM_RBUTTONUP:
				POINT point;
				GetCursorPos(&point);
				if (!SetForegroundWindow(GetLastActiveWindow()))
					SetForegroundWindow(hWnd);
				hmenu =GetSubMenu(mHPopupMenu, 0);
				UpdatePopupMenu(hmenu);
				TrackPopupMenu(hmenu, 0, point.x, point.y, 0, hWnd, NULL);
				break;
			case WM_LBUTTONDBLCLK: 
				if (!SetForegroundWindow(GetLastActiveWindow()))
					SetForegroundWindow(hWnd);
				break;
			}
			break;
		case WM_TIMER:
			if (wParam == FOCUS_TIMER_ID)
			{
				HandleHook(MAKELONG(0, FOCUS_TIMER_ID), 0);
				return 0;
			}
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		case WM_DESTROY:
			DestroyMenu(mHPopupMenu);
			PostQuitMessage(0); 
			break;
		case WM_COPYDATA:
			OnCopyData(lParam);
			break;
		case WM_POWERBROADCAST: 
			OnPowerBroadcast(wParam); 
			return TRUE;
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

//----------------------------------------------------------------------------------------

void CApp::ProcessMessages(void)
{
/*	��������� ���� ��������� �� ������� ������; ������� ������������ ��������� ��
	������� �������� ������, ���� ��� �������, � ���������� ���������� ����������
*/
	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void CApp::ProcessMessages(HWND hWnd, bool fWaitIfNoMessages)
{
/*	��������� ���� ��������� �� ������� ������, ��������������� ��� ���� hWnd; ���
	fWaitIfNoMessages == false ������� ������������ ��������� �� ������� �������� ������,
	���� ��� �������, � ���������� ���������� ����������; ��� fWaitIfNoMessages == true
	������� ���� ��������� ��������� � �������, ����� ���� ������������ ��� ��������� �
	���������� ���������� ����������
*/
	MSG msg;

	if (fWaitIfNoMessages && GetMessage(&msg, hWnd, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	while (PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}


void CApp::SetFocusTimer(void)
{
	m_dwFocusTimer = SetTimer(m_hMainWnd, FOCUS_TIMER_ID, 250, NULL);
}

void CApp::KillFocusTimer(void)
{
	if (m_dwFocusTimer)
	{
		KillTimer(m_hMainWnd, FOCUS_TIMER_ID);
		m_dwFocusTimer = 0;
	}
}

//----------------------------------------------------------------------------------------

int CApp::Run(void)
{
	// ��������� Vista UIPI
	SetupVistaUipiFilter();
	// ������ � �������� ������������� (������� ��������� � �������
	// ����) HookProc.exe, ���� ��������� �������� � 64-������ ��
	//if (m_fIsOsX64)
	//{
	//	char szFileName[MAX_PATH];
	//	ExtractFilePath(m_szExeFileName, szFileName);
	//	lstrcat(szFileName, "HookProc.exe");

	//	if ( RunApplication(TRUE, FALSE, m_hMainWnd, szFileName, NULL,
	//		NULL) != ERROR_SUCCESS )
	//		ShowMessageU(m_hMainWnd, IDS_HOOKPROC_ERROR, MB_ICONERROR | MB_OK);
	//	else
	//	{
	//		// �������� ������������� ���� m_hHookProcWnd � ������� 2 ������; �����
	//		// ������� HookProc.exe ���������� �������� ���� WPM ��������� �
	//		// ������� ������ ����, ����������� �������, �� �������������� ��� ����
	//		DWORD dwTime = GetTickCount() + 2000;
	//		while (!m_hHookProcWnd && (GetTickCount() < dwTime))
	//		{
	//			Sleep(1);
	//			ProcessMessages();
	//		}
	//	}
	//}

	// ��������� �����
	if (!HkSetHooks())
		ShowMessageU(m_hMainWnd, IDS_SETHOOKS_ERROR, MB_ICONERROR | MB_OK);

	// �������� ��������� ��� ������ ������� ���������
	ShowTourOnRun();
	// ��������� ������� ������������ ����� ��������� ���� � ������ �����
	SetFocusTimer();
	// ���� ��������� ��������� ������
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// ������ ������� ������������ ����� ��������� ���� � ������ �����
	KillFocusTimer();

	// ������ �����
	HkUnsetHooks();

	// ���������� �������� HookProc.exe, ���� �� ��� �������
	if (m_hHookProcWnd)
		PostMessage(m_hHookProcWnd, WM_CLOSE, 0, 0);
	return msg.wParam;
}