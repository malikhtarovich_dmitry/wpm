////////////////////////////////////////////////////////////////////////////////
// TrayIcon.cpp: implementation of the CTrayIcon class.
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "resource.h"
#include "TrayIcon.h"
#include "Utils.h"
#include "Lang.h"
#include "Registry.h"

HMODULE g_hInst = NULL;

HICON hiIDI_MAIN		= NULL;

#define LOADICON(A, B, w, h) if (!A) A = (HICON)LoadImage(g_hInst, (LPCTSTR)B, IMAGE_ICON, w, h, 0);	return A;

////////////////////////////////////////////////////////////////////////////////

#define NUMANIMICON	6
#define IDICON	389//10 - ����� ��������, ����� ��-�� ����� ����� ��������� ������ �� ����

////////////////////////////////////////////////////////////////////////////////

CTrayIcon g_TrayIcon;
CTrayIcon *GetTrayIcon() { return &g_TrayIcon; }
HWND g_hWndOwnerTrayIcon = NULL;
HICON g_AnimIcons[NUMANIMICON] = {NULL, NULL, NULL, NULL, NULL, NULL};

////////////////////////////////////////////////////////////////////////////////

void ShowAnimate (int AInd) {
	if ((AInd < 0) || (AInd >= NUMANIMICON) || !g_TrayIcon.IsEnabled()) return;
	NOTIFYICONDATA NID;
	NID.cbSize = sizeof (NOTIFYICONDATA);
	NID.hWnd = g_hWndOwnerTrayIcon;
	NID.uID = IDICON;
	NID.uFlags = NIF_ICON;
	NID.hIcon = g_AnimIcons[AInd];
    Shell_NotifyIcon(NIM_MODIFY, &NID);
}

////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI AnimateThread (LPVOID lpParameter) {
	Sleep(200);
	for (int i=0; i<NUMANIMICON*2; ++i) Sleep(100), ShowAnimate(i % NUMANIMICON);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

void StartAnimateTrayIcon () {
	if (!GetAnimated()) return;
	DWORD TID;
	CloseHandle (CreateThread(NULL, 0x4000, AnimateThread, NULL, 0, &TID));
}

////////////////////////////////////////////////////////////////////////////////
// CTrayIcon
////////////////////////////////////////////////////////////////////////////////

CTrayIcon::CTrayIcon() {
	fShow = false;
	fEnabled = TRUE;
	LoadStringLangA (IDS_APP_TITLE, fHintEnable, sizeof(fHintEnable));
	char s[MAX_RESSTR_SIZE];
	//LoadStringLangA (IDS_HINTTRAYICONDISABLE, s, sizeof(s));
	lstrcpy(fHintDisable, fHintEnable);
	lstrcat(fHintDisable, s);
	InitIcons();
}

////////////////////////////////////////////////////////////////////////////////

CTrayIcon::~CTrayIcon() { Hide(); }

////////////////////////////////////////////////////////////////////////////////

void CTrayIcon::InitIcons() {
	int ResIcon = GetSystemMetrics(SM_CXSMICON);
	if (!g_hInst) g_hInst = GetModuleHandle (NULL);
	hiIDI_MAIN = (HICON)LoadImage(g_hInst, (LPCTSTR)IDI_TRAY_ICON, IMAGE_ICON, 32, 32, 0);
	fIconEnable = hiIDI_MAIN;
}

////////////////////////////////////////////////////////////////////////////////

void CTrayIcon::SetWndOwnerTrayIcon(HWND hWnd) { g_hWndOwnerTrayIcon  = hWnd; }

////////////////////////////////////////////////////////////////////////////////

void CTrayIcon::Show() {
	if (fShow) return;
	HICON hIconSmall = (HICON)::LoadImage(g_hInst, MAKEINTRESOURCE(IDI_TRAY_ICON), 
			IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
	NOTIFYICONDATA NID;
	NID.cbSize = sizeof (NOTIFYICONDATA);
	NID.hWnd = g_hWndOwnerTrayIcon;
	NID.uID = IDR_POPUP_MENU;
	NID.uFlags = NIF_MESSAGE | NIF_ICON;
	NID.uCallbackMessage = WM_TRAYICON;
	NID.hIcon = hIconSmall;//(HICON)LoadImage(g_hInst, (LPCTSTR)IDI_TRAYICON, 1, 32, 32, 0);//fEnabled?fIconEnable:fIconDisable;
	LPSTR hint = fEnabled?fHintEnable:fHintDisable;
	if (hint) {
		int l = sizeof (NID.szTip)-1;
		strncpy(NID.szTip, hint, l); 
		NID.szTip[l-1] = 0;
		NID.uFlags = NID.uFlags | NIF_TIP;
	}
	Shell_NotifyIcon(NIM_ADD, &NID);
	fShow = true;
}

////////////////////////////////////////////////////////////////////////////////

void CTrayIcon::Hide() {
	if (!fShow) return;
	NOTIFYICONDATA NID;
	NID.cbSize = sizeof (NOTIFYICONDATA);
	NID.hWnd = g_hWndOwnerTrayIcon;
	NID.uID = IDICON;
	fShow = false;
}

////////////////////////////////////////////////////////////////////////////////


void CTrayIcon::Modify(BOOL AEnable) {
	fEnabled = AEnable;
	if (!fShow) return;
	NOTIFYICONDATA NID;
	NID.uFlags = NIF_ICON;
	NID.cbSize = sizeof (NOTIFYICONDATA);
	NID.hWnd = g_hWndOwnerTrayIcon;
	NID.uID = IDICON;
	NID.hIcon = (HICON)LoadImage(g_hInst, (LPCTSTR)IDI_TRAY_ICON, IMAGE_ICON, 32, 32, 0);//fEnabled?fIconEnable:fIconDisable;
	LPSTR hint = fEnabled?fHintEnable:fHintDisable;
	if (hint) {
		int l = sizeof (NID.szTip)-1;
		strncpy(NID.szTip, hint, l); 
		NID.szTip[l-1] = 0;
		NID.uFlags = NID.uFlags | NIF_TIP;
	}
}