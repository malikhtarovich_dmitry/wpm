#include "StdAfx.h"
#include <vector>
#include "resource.h"
#include "Utils.h"
#include "Lang.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

vector <HWND> gvLastActiveWindow;
HWND		g_LastForegroundWnd = NULL;
HWND		g_LastFocusWnd = NULL;
int			g_FWndX = 0;
int			g_FWndY = 0;

//----------------------------------------------------------------------------------------
// ��������� �������
//----------------------------------------------------------------------------------------

void ExtractFilePath(LPCSTR pszFileName, LPSTR pszFilePath)
{
/*	���������� �� ������� ����� ����� pszFileName ������� ���� � pszFilePath
	(���� ���� ������������ �� ����������� \ ��� :, �� �� ���������� � ����)
*/
	if (pszFilePath)
		if (pszFileName)
		{
			int k = 0;
			for (int i = 0; pszFileName[i] != 0; i++)
				if ((pszFileName[i] == ':') || (pszFileName[i] == '\\')) k = i;
			if (pszFileName != pszFilePath)
				memcpy(pszFilePath, pszFileName, k + 1);
			pszFilePath[k+1] = 0;
		}
		else
			pszFilePath[0] = 0;
}

//----------------------------------------------------------------------------------------
// �������� �������
//----------------------------------------------------------------------------------------

bool FileExists(LPCSTR pszFileName)
{
/*	�������� ������������� ����� � ������ pszFileName; ���� ���� ����������,
	������� ������ TRUE, � ������ FALSE, ���� ���������� ����� ���
*/
	DWORD Code = GetFileAttributes(pszFileName);
	bool fExists = ( (Code != INVALID_FILE_ATTRIBUTES) &&
		((Code & FILE_ATTRIBUTE_DIRECTORY) == 0) );
	return fExists;
}

//----------------------------------------------------------------------------------------

bool DirectoryExists(LPCSTR pszPathName)
{
/*	�������� ������������� ���������� � ������ pszFileName; ���� ���������� ����������,
	������� ������ TRUE, � ������ FALSE, ���� ��������� ���������� ���
*/
	DWORD Code = GetFileAttributes(pszPathName);
	bool fExists = ( (Code != INVALID_FILE_ATTRIBUTES) &&
		((Code & FILE_ATTRIBUTE_DIRECTORY) != 0) );
	return fExists;
}

//----------------------------------------------------------------------------------------

BOOL CreateComplexDirectory(LPCSTR pszPathName)
{
/*	�������� ���������� ��������� �����; pszPathName ��������� �� ������, ����������
	���� � ��� ����������� �����; ������� ������ TRUE ���� ����� ��� ���������� ���
	���� ������� � ������ FALSE ���� ����� ������� �� �������
*/
	if (CreateDirectory(pszPathName, NULL) || (GetLastError() == ERROR_ALREADY_EXISTS))
		return TRUE;

	int i = lstrlen(pszPathName) - 2;
	while ((i > 0) && (pszPathName[i] != '\\')) i--;
	if (i > 0)
	{
		char szParent[MAX_PATH];
		lstrcpyn(szParent, pszPathName, i+1);
		if (CreateComplexDirectory(szParent))
			return CreateDirectory(pszPathName, NULL);
	}
	return FALSE;
}

//----------------------------------------------------------------------------------------
// ������ �������
//----------------------------------------------------------------------------------------

void GetFileVersion(const char* pszFileName, DWORD* dwVerHi, DWORD* dwVerLo)
{
/*	��������� ������ ����� .exe pszFileName; � *dwVerHi ������� ���������� ���
	������� ����� ������ �����, � *dwVerLo - ��� ������� ����� ������ �����
*/
	DWORD dwZero, dwInfoSize;
	UINT dwFileInfoSize;

	*dwVerHi = *dwVerLo = 0;
	if ((dwInfoSize = GetFileVersionInfoSize(pszFileName, &dwZero)) > 0)
	{
		VS_FIXEDFILEINFO *FixedFileInfo;
		void *pInfo = malloc(dwInfoSize);
		if ( GetFileVersionInfo(pszFileName, 0, dwInfoSize, pInfo) &&
			VerQueryValue(pInfo, "\\", (void**) &FixedFileInfo, &dwFileInfoSize) &&
			(dwFileInfoSize > 0) )
		{
			*dwVerHi = FixedFileInfo->dwFileVersionMS;
			*dwVerLo = FixedFileInfo->dwFileVersionLS;
		}
		free(pInfo);
	}
}

//----------------------------------------------------------------------------------------

int GetTextWidth (HWND hWnd, char *Text)
{
	SIZE sz;
	HDC hDC = GetDC(hWnd);
	GetTextExtentPoint32(hDC, Text, strlen(Text), &sz);
	ReleaseDC (hWnd, hDC);
	return sz.cx;
}

int GetTextHeight (HWND hWnd, char *Text)
{
	SIZE sz;
	HDC hDC = GetDC(hWnd);
	GetTextExtentPoint32(hDC, Text, strlen(Text), &sz);
	ReleaseDC(hWnd, hDC);
	return sz.cy;
}

int GetTextWidth (HWND hWnd, HANDLE AFont, char *Text)
{
	SIZE sz;
	HDC hDC = GetDC(hWnd);
	HANDLE hOldF = SelectObject(hDC, AFont);
	GetTextExtentPoint32(hDC, Text, strlen(Text), &sz);
	SelectObject(hDC, hOldF);
	ReleaseDC(hWnd, hDC);
	return sz.cx;
}

SIZE GetTextSize (HWND hWnd, HANDLE AFont, char *Text) {
	SIZE sz;
	HDC hDC = GetDC(hWnd);
	HANDLE hOldF = SelectObject(hDC, AFont);
	GetTextExtentPoint32(hDC, Text, strlen(Text), &sz);
	SelectObject(hDC, hOldF);
	ReleaseDC (hWnd, hDC);
	return sz;
}

BOOL IsHighColor (HWND AWnd) {
	HDC hDC; hDC = GetDC(AWnd);
	int dc = GetDeviceCaps(hDC, BITSPIXEL);
	ReleaseDC(AWnd, hDC);
	return (dc > 8);
}

BOOL IsComctl470AndLater() {
	HMODULE hDll = LoadLibrary("Comctl32.dll");
	BOOL res = (hDll) && (GetProcAddress(hDll, "InitCommonControlsEx"));
	if (hDll) FreeLibrary(hDll);
	return res;
}

//////////////////////////////////////////////////////////////////////
// ������� ������� �������� ����. ����� ��� ����������� � ��� ����, ����� 
// �������������� ��� �������� ���� ����� �� ������ ��������.
//////////////////////////////////////////////////////////////////////

HWND GetLastActiveWindow() {
	if (gvLastActiveWindow.size() == 0)
		return NULL;
	else
		return *(gvLastActiveWindow.end()-1);
}

void SetLastActiveWindow(HWND hWnd) {
	gvLastActiveWindow.push_back(hWnd);
}

void RestoreLastActiveWindow() {
	gvLastActiveWindow.pop_back();
}

BOOL CloseDialog (HWND hDlg, INT_PTR nResult) {
	BOOL res = EndDialog(hDlg, nResult);
	DefWindowProc(hDlg, WM_SYSCOMMAND, SC_MINIMIZE, 0);
	return res;
}

////////////////////////////////////////////////////////////////////////////////

void InitDlgIconAndMenu(HWND ADlg)
{
	/*SendMessage(ADlg, WM_SETICON, ICON_SMALL, (LPARAM) GetIcon(IDI_TRAYICON));
	SendMessage(ADlg, WM_SETICON, ICON_BIG, (LPARAM) GetIcon(IDI_TRAYICON));
	HMENU vSysMenu = GetSystemMenu(ADlg, FALSE);
    DeleteMenu(vSysMenu, SC_MAXIMIZE, MF_BYCOMMAND);
    DeleteMenu(vSysMenu, SC_SIZE, MF_BYCOMMAND);
    DeleteMenu(vSysMenu, SC_RESTORE, MF_BYCOMMAND);	*/
}

//////////////////////////////////////////////////////////////////////////////// 
// ����������� ���������
//////////////////////////////////////////////////////////////////////////////// 
/*
void CreateHint (HWND hwnd, char *AHint) {
    TOOLINFO ti; RECT rect; HINSTANCE vInst = GetModuleHandle (NULL); HWND hwndTT;
    unsigned int uid = 0;           
	if ((!GetModuleHandle("Comctl32.dll")) && (!LoadLibrary ("Comctl32.dll"))) return;
    hwndTT = CreateWindowEx(WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, 
		WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,		
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, NULL, vInst, NULL);
    SetWindowPos(hwndTT, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
    GetClientRect (hwnd, &rect);
	ti.cbSize = sizeof(TOOLINFO);
    ti.uFlags = TTF_SUBCLASS;
    ti.hwnd = hwnd;
    ti.hinst = vInst;
    ti.uId = uid;
    ti.lpszText = AHint;
    ti.rect.left = rect.left;
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
    ti.rect.bottom = rect.bottom;
    SendMessage(hwndTT, TTM_ADDTOOL, 0, (LPARAM) (LPTOOLINFO) &ti);
} 

void CreateHint (HWND hwnd, UINT uID) {
	char s[MAX_RESSTR_SIZE];
	LoadStringLang (uID, s, sizeof(s));
	CreateHint (hwnd, s);
}
*/
////////////////////////////////////////////////////////////////////////////////

HWND CreateHintWnd(HWND hwnd) {
    HWND res = CreateWindowEx(WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, 
		WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,		
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, 
		NULL, GetModuleHandle(NULL), NULL);
    SetWindowPos(res, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
	return res;
}

void AddHint (HWND AWndTT, HWND AWndCtrl, UINT AuID) {
    TOOLINFO ti; RECT rect; 
    GetClientRect (AWndCtrl, &rect);
	ti.cbSize = sizeof(TOOLINFO);
    ti.uFlags = TTF_SUBCLASS;
    ti.hwnd = AWndCtrl;
    ti.hinst = GetInstLang();//GetModuleHandle (NULL);
    ti.uId = 0;
    ti.lpszText = MAKEINTRESOURCE(AuID);
    ti.rect.left = rect.left;
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
    ti.rect.bottom = rect.bottom;
    SendMessage(AWndTT, TTM_ADDTOOL, 0, (LPARAM) (LPTOOLINFO) &ti);
} 


void SetLastForegroundWindow(HWND AWnd)
{ 
	DWORD TID = GetWindowThreadProcessId(AWnd, NULL);
	if ((GetCurrentThreadId() != TID) &&
		(GetWindowThreadProcessId(FindWindow("Shell_TrayWnd",NULL), NULL) != TID))
		g_LastForegroundWnd = AWnd;
}

////////////////////////////////////////////////////////////////////////////////

void SetLastFocusWindow(HWND AWnd)
{
	DWORD TID = GetWindowThreadProcessId(AWnd, NULL);
	if ((GetCurrentThreadId() != TID) &&
		(GetWindowThreadProcessId(FindWindow("Shell_TrayWnd",NULL), NULL) != TID))
		g_LastFocusWnd = AWnd;
}

HWND GetLastFocusWindow(void)
{
	return g_LastFocusWnd;
}

void SetLastForegroundWindowSelf(HWND AWnd)
{
	g_LastForegroundWnd = AWnd;
	g_LastFocusWnd = AWnd;
}

////////////////////////////////////////////////////////////////////////////////

BOOL SwitchToLastFocusWnd()
{
	SetForegroundWindow(g_LastForegroundWnd);
	int j = 0;
	while ((j < 50) && (g_LastForegroundWnd != GetForegroundWindow())) { Sleep(0); ++j; }
	if (g_LastForegroundWnd == GetForegroundWindow())
	{
		if (IsWindowVisible(g_LastFocusWnd))
		{
			DWORD ThID1 = GetCurrentThreadId();
			DWORD ThID2 = GetWindowThreadProcessId(g_LastForegroundWnd, NULL);
			if (AttachThreadInput(ThID1, ThID2, TRUE))
			{
				SetFocus(g_LastFocusWnd);
				Sleep(0);
				AttachThreadInput(ThID1, ThID2, FALSE);
			}
		}
		return TRUE;
	}
	else
		return FALSE;
}


void SetForegroundWindowExt(HWND AWnd) {
	if (GetForegroundWindow() == AWnd) return;
	SetForegroundWindow(AWnd);
	HWND hFWnd = GetForegroundWindow(); 
	if (hFWnd == AWnd) return;
	int TID1 = GetCurrentThreadId(), TID2 = GetWindowThreadProcessId(hFWnd,0);
	AttachThreadInput(TID1, TID2, TRUE);
	SetForegroundWindow(AWnd);
	AttachThreadInput(TID1, TID2, FALSE);
	BringWindowToTop(AWnd);
}

void GetFocusWnd(HWND &AWndForeground, HWND &AWndFocus)
{
	AWndForeground = GetForegroundWindow();
	int TID1 = GetCurrentThreadId();
	int TID2 = GetWindowThreadProcessId(AWndForeground, 0);
	AttachThreadInput(TID1, TID2, TRUE);
	AWndFocus = GetFocus();
	AttachThreadInput(TID1, TID2, FALSE);
}

void SetFocusWnd(HWND AWndForeground, HWND AWndFocus) {
	int TID1 = GetCurrentThreadId(), TID2 = GetWindowThreadProcessId(AWndForeground,0);
	AttachThreadInput(TID2, TID1, TRUE);
	BringWindowToTop(AWndForeground);
	SetForegroundWindow(AWndForeground);
	SetFocus(AWndFocus);
	AttachThreadInput(TID2, TID1, FALSE);
}

BOOL AppIsForeground() {
	HWND hFWnd = GetForegroundWindow(); 
	int TID1 = GetCurrentThreadId(), TID2 = GetWindowThreadProcessId(hFWnd,0);
	return TID1 == TID2;
}

BOOL AppIsLastForeground() {
	HWND hFWnd = g_LastForegroundWnd; 
	int TID1 = GetCurrentThreadId(), TID2 = GetWindowThreadProcessId(hFWnd,0);
	return TID1 == TID2;
}

////////////////////////////////////////////////////////////////////////////////

void SetCheckBox (HWND AWnd, BOOL ASet) {
	if (ASet)
		SendMessage (AWnd, BM_SETCHECK, BST_CHECKED, 0);
	else
		SendMessage (AWnd, BM_SETCHECK, BST_UNCHECKED, 0);
}

BOOL GetCheckBox(HWND AWnd) {
	return ( (SendMessage (AWnd, BM_GETCHECK, 0, 0) & BST_CHECKED) == BST_CHECKED );
}

/// 

void ShowLastError() {
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR) &lpMsgBuf,	0, NULL); 
	MessageBox(0, (LPCTSTR)lpMsgBuf, "", 0);
	LocalFree( lpMsgBuf );
}

/////////////////////////////////// End File ///////////////////////////////////