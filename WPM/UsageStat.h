#ifndef CLASS_CUSAGESTAT
#define CLASS_CUSAGESTAT

#pragma once

#define NAG_OPENMAINWINDOW		1
#define NAG_IMPORT				2
#define NAG_PRINT				3

#include <windows.h>

class CUsageStat {
private:
	FILETIME fgStartDate;	
	FILETIME fgLastNag;		

	WORD fgCntRun;			
	WORD fgCntPhr;		
	WORD fgCntFld;			
	WORD fgCntRpl;		
	WORD fgCntNag;			

	char fPackStat[12];

	DWORD GetCountDaysAfterStart();
	DWORD GetCountHoursAfterNag();
	double GetReliance();
	BOOL RecentNag();
	BOOL TooSmall();
	BOOL TooBig();
	void SaveParToReg();
public:
	CUsageStat();
	virtual ~CUsageStat();

	void AddRun() { fgCntRun++; }
	void AddPhr() { fgCntPhr++; }
	void AddFld() { fgCntFld++; }
	void AddRpl() { if (fgCntRpl<60000) fgCntRpl++; SaveParToReg(); }

	BOOL IsShowingNag();
	BOOL IsExpired();
	void UdpateLastNagDate();
	void UpdateCountNag();

	void ShowStat();

	DWORD GetSizeMemLoad();
	DWORD GetSizeMemSave();
	void ReadFromMem (PBYTE APtr);
	void WriteToMem (PBYTE APtr);

	char *GetPackStat();
};

CUsageStat *GetUsageStat();

#endif // !defined(CLASS_CUSAGESTAT)
