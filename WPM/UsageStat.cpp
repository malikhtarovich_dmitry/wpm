#include "StdAfx.h"
#include "UsageStat.h"
#include "Registry.h"
#include <stdio.h>

#define usMINPHRASE		5
#define usMAXPHRASE		15
#define usMINDAYS		15
#define usMAXDAYS		60
#define usMINREPLACE	50
#define usMAXREPLACE	200
#define usMAXNAG		100 //�.�. ������������ ���-�� ������� ���� - 100. ����� ����� - ������ ���� �����������������.

////////////////////////////////////////////////////////////////////////////////

CUsageStat g_UsageStat;
CUsageStat *GetUsageStat() { return &g_UsageStat; };

////////////////////////////////////////////////////////////////////////////////

CUsageStat::CUsageStat()
{
	SYSTEMTIME st; GetLocalTime(&st);
	SystemTimeToFileTime(&st, &fgStartDate); 
	srand(fgStartDate.dwLowDateTime);
	fgLastNag.dwLowDateTime = fgLastNag.dwHighDateTime = 0;
	fgCntRun = fgCntPhr = fgCntFld = fgCntRpl = fgCntNag = 0;

	//������ ��������� �� �������.
	LPVOID lpPar = malloc(GetSizeMemLoad());
	if (lpPar) {
		if (ReadMemFromRegHKCU (REGPATH, REGPAR_USAGESTAT1, lpPar, GetSizeMemLoad()))
			ReadFromMem((PBYTE)lpPar);
		free(lpPar);
	}
}

CUsageStat::~CUsageStat() {
	SaveParToReg();
}

////////////////////////////////////////////////////////////////////////////////

void CUsageStat::SaveParToReg()
{
	LPVOID lpPar = malloc(GetSizeMemLoad());
	if (lpPar)
	{
		WriteToMem ((PBYTE)lpPar);
		WriteMemToRegHKCU (REGPATH, REGPAR_USAGESTAT1, lpPar, GetSizeMemLoad());
		free(lpPar);
	}
	WriteStrToRegHKCU(REGPATH, REGPAR_USAGESTAT2, GetPackStat());
}

////////////////////////////////////////////////////////////////////////////////

DWORD CUsageStat::GetSizeMemLoad() { return (2*sizeof(FILETIME) + sizeof(WORD)*5); }
DWORD CUsageStat::GetSizeMemSave() { return (2*sizeof(FILETIME) + sizeof(WORD)*5); }

////////////////////////////////////////////////////////////////////////////////

void CUsageStat::ReadFromMem (PBYTE APtr) {
	if (!APtr) return;
	LPFILETIME pT1, pT2;

	pT1 = (LPFILETIME)APtr;
	APtr += sizeof(FILETIME);

	pT2 = (LPFILETIME)APtr;
	APtr += sizeof(FILETIME);

	if (fgCntRun < *(PWORD)APtr) fgCntRun = *(PWORD)APtr;	
	APtr += sizeof(WORD);

	if (fgCntPhr < *(PWORD)APtr) fgCntPhr = *(PWORD)APtr;				
	APtr += sizeof(WORD);

	if (fgCntFld < *(PWORD)APtr) fgCntFld = *(PWORD)APtr;				
	APtr += sizeof(WORD);

	if (fgCntRpl < *(PWORD)APtr) fgCntRpl = *(PWORD)APtr;
	APtr += sizeof(WORD);

	if (fgCntNag < *(PWORD)APtr) fgCntNag = *(PWORD)APtr;

	if ((pT1->dwHighDateTime) && (pT1->dwLowDateTime) &&
		(CompareFileTime(pT1, &fgStartDate) == -1))	fgStartDate = *pT1;		
	if ((pT2->dwHighDateTime) && (pT2->dwLowDateTime) &&
		(CompareFileTime(pT2, &fgLastNag) == 1))	fgLastNag = *pT2;

	SYSTEMTIME st; GetLocalTime(&st); FILETIME ft; SystemTimeToFileTime(&st, &ft);
	if (CompareFileTime(&fgStartDate, &ft) == 1) fgStartDate = ft;
	if (CompareFileTime(&fgLastNag, &ft) == 1) fgLastNag = ft;
}

////////////////////////////////////////////////////////////////////////////////

void CUsageStat::WriteToMem (PBYTE APtr) {
	if (!APtr) return;
	*(LPFILETIME)APtr = fgStartDate;		APtr += sizeof(FILETIME);
	*(LPFILETIME)APtr = fgLastNag;			APtr += sizeof(FILETIME);
	*(PWORD)APtr = fgCntRun;				APtr += sizeof(WORD);
	*(PWORD)APtr = fgCntPhr;				APtr += sizeof(WORD);
	*(PWORD)APtr = fgCntFld;				APtr += sizeof(WORD);
	*(PWORD)APtr = fgCntRpl;				APtr += sizeof(WORD);
	*(PWORD)APtr = fgCntNag;
}

void CUsageStat::UdpateLastNagDate() {
	SYSTEMTIME st; GetLocalTime(&st); SystemTimeToFileTime(&st, &fgLastNag);
	//fgCntNag++; - ����� ��������. � �� ��� ��������� ���������� ������ ����� 
	//����� �� Agree.
	SaveParToReg();
}

void CUsageStat::UpdateCountNag() {
	fgCntNag++;
	SaveParToReg();
}

////////////////////////////////////////////////////////////////////////////////

DWORD CUsageStat::GetCountDaysAfterStart() {
	SYSTEMTIME st; GetLocalTime(&st);
	FILETIME ft; SystemTimeToFileTime(&st, &ft);
	//���� ���� ������ ������ ������� ����, �� ������� ��� ���-�� ��������� 
	//���� ����� ������ = ������������� ��������
	if (CompareFileTime(&fgStartDate, &ft) == 1) return usMAXDAYS;
	
	UINT64 u2 =(((UINT64)ft.dwHighDateTime << 32) + (DWORD)ft.dwLowDateTime -
				((UINT64)fgStartDate.dwHighDateTime << 32) - 
				(DWORD)fgStartDate.dwLowDateTime )/(60*60*24*(UINT64)10000000);

	return (DWORD)u2;
}

////////////////////////////////////////////////////////////////////////////////

DWORD CUsageStat::GetCountHoursAfterNag() {
	if (CompareFileTime(&fgLastNag, &fgStartDate) == -1) fgLastNag = fgStartDate;
	SYSTEMTIME st; GetLocalTime(&st);
	FILETIME ft; SystemTimeToFileTime(&st, &ft);
	if (CompareFileTime(&fgLastNag, &ft) == 1) return 30*24;
	
	UINT64 u2 =(((UINT64)ft.dwHighDateTime << 32) + (DWORD)ft.dwLowDateTime -
				((UINT64)fgLastNag.dwHighDateTime << 32) - (DWORD)fgLastNag.dwLowDateTime
				)/(60*60*(UINT64)10000000);
	return (DWORD)u2;
}

////////////////////////////////////////////////////////////////////////////////
// ���������� ������� ������������� ���������. �������� - [0, 1], 
// 0 - �� ����������, 1 - ������� ����������.
////////////////////////////////////////////////////////////////////////////////

double CUsageStat::GetReliance() {
	double db1, db2, db3;
	DWORD d1 = GetCountDaysAfterStart();
	if (d1 < usMINDAYS) d1 = 0; else d1 -= usMINDAYS;
	if (d1 > (usMAXDAYS-usMINDAYS)) d1 = usMAXDAYS-usMINDAYS;
	db1 = d1/(usMAXDAYS-usMINDAYS); // [0..1]
	DWORD d2 = fgCntPhr;
	if (d2 < usMINPHRASE) d2 = 0; else d2 -= usMINPHRASE;
	if (d2 > usMAXPHRASE-usMINPHRASE) d2 = usMAXPHRASE-usMINPHRASE;
	db2 = d2/(usMAXPHRASE-usMINPHRASE); // [0..1]
	DWORD d3 = fgCntRpl;
	if (d3 < usMINREPLACE) d3 = 0; else d3 -= usMINREPLACE;
	if (d3 > usMAXREPLACE-usMINREPLACE) d3 = usMAXREPLACE-usMINREPLACE;
	db3 = d3/(usMAXREPLACE-usMINREPLACE); // [0..1]
	return (db1+db2+db3)/3;
}

////////////////////////////////////////////////////////////////////////////////
// ���������� TRUE ���� ��� ������� �����������, � ���� ���������� ��� �� ����.
// ���������� �������� "�������" ������������ ������ �� ������� ������������� ���������.
////////////////////////////////////////////////////////////////////////////////

BOOL CUsageStat::RecentNag() {
	DWORD maxhrs;
	double rel = GetReliance();
	if (rel < 0.1) maxhrs = 5*24; else 
	if (rel < 0.2) maxhrs = 4*24; else 
	if (rel < 0.3) maxhrs = 3*24; else 
	if (rel < 0.4) maxhrs = 2*24; else 
	if (rel < 0.8) maxhrs = 1*24; else 
	if (rel < 0.99) maxhrs = 3;	  else maxhrs = 2;
	return (GetCountHoursAfterNag() < maxhrs);
}

////////////////////////////////////////////////////////////////////////////////
// ������ TRUE ���� ������������ ���� ��� ���� �������� ���-�������
////////////////////////////////////////////////////////////////////////////////

BOOL CUsageStat::TooSmall() {
	return ((GetCountDaysAfterStart() < usMINDAYS) || 
			(fgCntPhr < usMINPHRASE) || 
			(fgCntRpl < usMINREPLACE) ||
			RecentNag()
			);
}

////////////////////////////////////////////////////////////////////////////////
// ������ TRUE ���� ������������ ����� ������� ���������
////////////////////////////////////////////////////////////////////////////////

BOOL CUsageStat::TooBig() {
	return (((GetCountDaysAfterStart() > usMAXDAYS) && 
				((fgCntPhr > usMINPHRASE) || (fgCntRpl > usMINREPLACE))
			) || (fgCntPhr > usMAXPHRASE) || (fgCntRpl > usMAXREPLACE)
			);
}

////////////////////////////////////////////////////////////////////////////////

BOOL CUsageStat::IsShowingNag()
{
	if (TooSmall()) return FALSE;
	BOOL bOk;
	if (TooBig())
		bOk = TRUE;
	else
	{
		double d = (double)rand()/RAND_MAX;
		bOk = d < GetReliance();
	}
	return bOk;
}

////////////////////////////////////////////////////////////////////////////////

BOOL CUsageStat::IsExpired() { return (fgCntNag >= /*20000+*/usMAXNAG); }

////////////////////////////////////////////////////////////////////////////////

WORD GetCountDaysAfter2000 (FILETIME &ATime) {
	UINT64 u; FILETIME ft; DosDateTimeToFileTime((20 << 9) + (1 << 5) + 1, 0 , &ft);
	if (CompareFileTime(&ft, &ATime) == -1)
		u = (((UINT64)ATime.dwHighDateTime << 32) + (DWORD)ATime.dwLowDateTime -
			 ((UINT64)ft.dwHighDateTime << 32) - (DWORD)ft.dwLowDateTime 
			)/(60*60*24*(UINT64)10000000);
	else u = 0;	
	return (WORD)u;
}

////////////////////////////////////////////////////////////////////////////////

char *CUsageStat::GetPackStat() {
#define NUMBASE 62
	char SetChar[NUMBASE]; WORD w; int Ind = 0;
	char ch; int i = 0;
	for (ch='A'; ch <= 'Z'; ++ch, ++i) SetChar[i] = ch;
	for (ch='a'; ch <= 'z'; ++ch, ++i) SetChar[i] = ch;
	for (ch='0'; ch <= '9'; ++ch, ++i) SetChar[i] = ch;

	w = GetCountDaysAfter2000(fgStartDate)%(NUMBASE*NUMBASE);
	fPackStat[Ind++] = SetChar[w/NUMBASE]; 
	fPackStat[Ind++] = SetChar[w%NUMBASE];
	w = fgCntRun; if (w > ((NUMBASE*NUMBASE)-1)) w = (NUMBASE*NUMBASE)-1;
	fPackStat[Ind++] = SetChar[w/NUMBASE];
	fPackStat[Ind++] = SetChar[w%NUMBASE];
	w = fgCntPhr; if (w > ((NUMBASE*NUMBASE)-1)) w = (NUMBASE*NUMBASE)-1;
	fPackStat[Ind++] = SetChar[w/NUMBASE];
	fPackStat[Ind++] = SetChar[w%NUMBASE];
	w = fgCntFld; if (w > ((NUMBASE*NUMBASE)-1)) w = (NUMBASE*NUMBASE)-1;
	fPackStat[Ind++] = SetChar[w/NUMBASE];
	fPackStat[Ind++] = SetChar[w%NUMBASE];
	w = fgCntRpl; if (w > ((NUMBASE*NUMBASE)-1)) w = (NUMBASE*NUMBASE)-1;
	fPackStat[Ind++] = SetChar[w/NUMBASE];
	fPackStat[Ind++] = SetChar[w%NUMBASE];
	w = fgCntNag; if (w > ((NUMBASE*NUMBASE)-1)) w = (NUMBASE*NUMBASE)-1;
	fPackStat[Ind++] = SetChar[w/NUMBASE];
	fPackStat[Ind++] = SetChar[w%NUMBASE];

	return fPackStat;	
}

/////////////////////////////////// End File ///////////////////////////////////
