/*! @file CApp.h */

#ifndef __WPCONTROL_H__
#define __WPCONTROL_H__
#pragma once

#include <Windows.h>
#include <WinDef.h>
#include "CFakeWindow.h"

/**
 * @addtogroup Window's Position Controller
 * @brief
 * @{
 */

class WPControl{
public:
	static WPControl* Instance();
	void allocateWindow(HINSTANCE hInst, HWND hwndTarget, WORD wPar);
	bool InitCmdKeys(char cmdCD, char cmdZI, char cmdZO, char cmdR);
protected:
	WPControl();
private:
	static WPControl* instance;
	WORD cmdZoomOut;
	WORD cmdZoomIn;
	WORD cmdCaptured;
	WORD cmdRelease;
	int parZoom;
};

#endif // !defined(__WPCONTROL_H__)

/*! @} */