// DlgSettings.h: interface for the DlgSettings class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __DLGSETTINGS_H__
#define __DLGSETTINGS_H__

#pragma once

#include "StdAfx.h"
#include "resource.h"
#include "CApp.h"

class DlgSettings :    public CSimpleDialog<IDD_SETTINGS>,
                       public CWinDataExchange<DlgSettings>,
					   public COwnerDraw<DlgSettings>
{
private:
	char hkCapture[2];
	char hkZoomIn[2];
	char hkZoomOut[2];
	char hkRelease[2];
public:
	BEGIN_MSG_MAP_EX(DlgSettings)
		MSG_WM_INITDIALOG(OnInitDialog)
		MSG_WM_HSCROLL(OnHScroll)
		COMMAND_RANGE_CODE_HANDLER_EX(IDOK, IDCANCEL, BN_CLICKED, OnOK)
		CHAIN_MSG_MAP(CSimpleDialog<IDD_SETTINGS>)
		CHAIN_MSG_MAP(COwnerDraw<DlgSettings>)
	END_MSG_MAP()

	BEGIN_DDX_MAP(DlgSettings)
		DDX_TEXT(IDC_EDIT_CAPTURE, (LPTSTR)hkCapture)
		DDX_TEXT(IDC_EDIT_ZOOMOUT, (LPTSTR)hkZoomOut)
		DDX_TEXT(IDC_EDIT_ZOOMIN, (LPTSTR)hkZoomIn)
		DDX_TEXT(IDC_EDIT_RELEASE, (LPTSTR)hkRelease)
	END_DDX_MAP()

	bool InitValues(char chCap, char chZI, char chZO, char chR) {
		hkCapture[0] = chCap;
		hkZoomIn[0] = chZI;
		hkZoomOut[0] = chZO;
		hkRelease[0] = chR;

		hkCapture[1] = 0;
		hkZoomIn[1] = 0;
		hkZoomOut[1] = 0;
		hkRelease[1] = 0;
		return true;
	} 

	char getHKCapture() { return hkCapture[0]; }
	char getHKZoomIn() { return hkZoomIn[0]; }
	char getHKZoomOut() { return hkZoomOut[0]; }
	char getHKRelease() { return hkRelease[0]; }

	LRESULT OnInitDialog(HWND hWnd, LPARAM)
	{
		DoDataExchange(FALSE);
		return 0;
	}

	LRESULT OnHScroll(WPARAM wParam, LPARAM, HWND)
	{
		DoDataExchange(FALSE);
		return 0;
	}

	LRESULT OnOK(UINT, int, HWND)
	{
		if(DoDataExchange(TRUE))
			SetMsgHandled(FALSE);
		return 0;
	}
};

#endif // !defined __DLGSETTINGS_H__