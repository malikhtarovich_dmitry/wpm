#pragma once
#include <windows.h>

#define REGPATH				"Software\\StRandom\\WPM"
#define REGPATH_DLGMAIN		"Software\\StRandom\\WPM\\DlgMain"
#define REGPATH_DEFAULT		"Software\\StRandom\\WPM\\Default"
#define REGPATH_STARTUP		"Software\\Microsoft\\Windows\\CurrentVersion\\Run"
#define REGPAR_LOGFILE		"LogFile"
#define REGPAR_LANG			"Language"
#define REGPAR_ANIM			"AnimatedTray"
#define REGPAR_CUSTOMBUILD	"CustomBuild"

#define REGPAR_SHOWCMD		"showcmd"
#define REGPAR_POSMAXX		"max.x"
#define REGPAR_POSMAXY		"max.y"
#define REGPAR_POSMINX		"min.x"
#define REGPAR_POSMINY		"min.y"
#define REGPAR_POSL			"left"
#define REGPAR_POST			"top"
#define REGPAR_POSR			"right"
#define REGPAR_POSB			"bottom"

#define REGPAR_CHECKEXT		"CheckExt"
#define REGPAR_DEF_FONTNAME	"FontName"
#define REGPAR_DEF_FONTSIZE	"FontSize"


#define REGPAR_USAGESTAT1	"US1" //������ ����������
#define REGPAR_USAGESTAT2	"US2" //� ������ ����, �� ��� ���������� �� ������.

#define PROGID				"WPM.1"


void WriteStrToReg (HKEY hKeyGlob, LPCTSTR lpSubKey, LPCTSTR lpValueName, LPCSTR lpStr);
void ReadFromReg (HKEY hKeyGlob, LPCTSTR lpSubKey, LPCTSTR lpValueName, 
				  LPSTR lpStr, DWORD nSzStr);

void WriteStrToRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, LPCSTR lpStr);
void ReadStrFromRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, LPSTR lpStr, DWORD nSzStr);

void WriteIntToRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, UINT AVal);
int ReadIntFromRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, int ADefVal);

BOOL ReadMemFromRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, LPVOID lpBuf, DWORD nSzBuf);
BOOL WriteMemToRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, LPVOID lpBuf, DWORD nSzBuf);

BOOL SetStartUp(BOOL AStartUp);
BOOL GetStartUp();
BOOL GetAnimated();
void SetAnimated(BOOL AAnim);
BOOL GetLogInfo();
void SetLogInfo(BOOL LogOn);

void ReadPosWindowFromReg(int ADefaultWidth, int ADefaultHeight, LPCTSTR ASubKey,
						  WINDOWPLACEMENT &APos);
void WritePosWindowToReg(LPCTSTR ASubKey, WINDOWPLACEMENT &APos);

char* GetAffiliateCode();
BOOL IsBannerExists();
char *GetBannerTitle();
char *GetBannerDescr();
char *GetBannerURL();
