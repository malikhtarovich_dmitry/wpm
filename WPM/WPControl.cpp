#include "WPControl.h"

WPControl* WPControl::instance = 0;

WPControl* WPControl::Instance() 
{
	if (instance == 0){
		instance = new WPControl;
	}
	return instance;
}

void WPControl::allocateWindow(HINSTANCE hInst, HWND hwndTarget, WORD wPar)
{
	if (HIBYTE(wPar) == cmdCaptured)
	{
		CFakeWindow *cfw = new CFakeWindow();
		cfw->Init(hwndTarget, cmdZoomIn, cmdZoomOut, cmdRelease);
		cfw->DoModal(hwndTarget);
	}
}

bool WPControl::InitCmdKeys(char cmdCD, char cmdZI, char cmdZO, char cmdR)
{
	cmdCaptured = cmdCD;
	cmdZoomIn = cmdZI;
	cmdZoomOut = cmdZO;
	cmdRelease = cmdR;
	return true;
}

WPControl::WPControl(){
	parZoom = 100;
}

