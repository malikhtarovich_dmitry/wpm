/*! @file TraceLogger.h */
#pragma once

#include <fstream>
#include <time.h>

#define LOG_TRACE TraceLogger logger(__FILE__, __FUNCTION__, __LINE__);

class TraceLogger {
public:
    TraceLogger(const char* fileName, const char* funcName, int lineNumber);
    ~TraceLogger();

private:
    const char* _fileName;
    const char* _funcName;
};