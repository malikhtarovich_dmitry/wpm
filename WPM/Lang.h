#include <windows.h>

#define SIZEOFW(X) sizeof(X)/sizeof(WCHAR)
//#define LoadStringLang LoadStringLangA

HINSTANCE GetInstLang();
int LoadStringLangA (UINT uID, LPTSTR lpBuffer, int nBufferMax);
int LoadStringLangW (UINT uID, LPWSTR lpBuffer, int nBufferMax);
int DialogBoxLang (UINT uID, HWND AWnd, DLGPROC ADlgProc);

void SelectLanguageItem (WORD AId);
void SelectLanguage ();

void SetFlagUnicode (BOOL AUnicode);
BOOL IsOSUnicode();
LPWSTR AnsiToUnicode (LPSTR AStr);
LPSTR UnicodeToAnsi (LPWSTR AWStr);
LPWSTR EmptyUnicode ();
LPSTR EmptyAnsi ();
DWORD GetLenUnicodeWithZero(LPWSTR AWStr);

int ShowMessageU (HWND hWnd, UINT uID, UINT uType);
int ShowMessageU (HWND hWnd, UINT uID, UINT uType, int AInt);
int ShowMessageU (HWND hWnd, UINT uID, UINT uType, char *AStr);
int ShowMessageU (HWND hWnd, LPWSTR AStr, UINT uType);
int DialogBoxLangU (UINT uID, HWND AWnd, DLGPROC ADlgProc);
HWND CreateDialogLangU (UINT uID, HWND AWnd, DLGPROC ADlgProc);
BOOL SetWindowTextU (HWND AWnd, LPWSTR AStr);
int GetWindowTextU (HWND AWnd, LPWSTR AStr, int Sz);
int LoadStringLangU (UINT uID, LPWSTR lpBuffer, int nBufferMax);
BOOL TextOutU(HDC hdc, int nXStart, int nYStart, LPWSTR lpString, int cbString);
BOOL GetTextExtentPoint32U(HDC hdc, LPWSTR lpString, int cbString, LPSIZE lpSize);
BOOL AppendMenuU (HMENU hMenu, UINT uFlags, UINT_PTR uIDNewItem, LPWSTR lpNewItem);
BOOL ModifyMenuU (HMENU hMenu, UINT uPosition, UINT uFlags, UINT_PTR uIDNewItem, LPWSTR lpNewItem);
BOOL InsertMenuU (HMENU hMenu, HMENU hSub, UINT uPosition, UINT uFlags, UINT uIDStr);
BOOL InsertMenuU (HMENU hMenu, HMENU hSub, UINT uPosition, UINT uFlags, LPWSTR lpStr);
int GetMenuStringU (HMENU hMenu, UINT uPosition, LPWSTR lpString, int nMaxCount, 
					 UINT uFlags);

LPWSTR GetTextRangeU (HWND AWnd, LONG AMin, LONG AMax);

HWND CreateWindowExU(DWORD dwExStyle, LPWSTR lpClassName, LPWSTR lpWindowName, 
					DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent,
					HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam);

void TabCtrl_InsertItemU(HWND ATabCtrl, HWND APage, UINT uID);

int ListBox_AddStringU(HWND ALB, LPWSTR AStr);
LPWSTR ListBox_GetTextU(HWND ALB, int AInd);

