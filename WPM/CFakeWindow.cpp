#include "CFakeWindow.h"

LRESULT CFakeWindow::OnInitDialog(HWND hWnd, LPARAM)
{
	SetWindowLong(GWL_STYLE, WS_DLGFRAME | DS_NOIDLEMSG );
	LONG lExStyle = GetWindowLong(GWL_EXSTYLE);
	lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
	SetWindowLong(GWL_EXSTYLE, lExStyle);
	SetWindowPos(hwndRelated, rcDefault.left, rcDefault.top, 
		         rcDefault.right - rcDefault.left,
				 rcDefault.bottom - rcDefault.top,
				 SWP_FRAMECHANGED);
	//SetLayout(GetDC(), LAYOUT_BITMAPORIENTATIONPRESERVED);
	//SetTimer(1, 5000, NULL);
	return 0;
}

HWND CFakeWindow::getRelatedHWND()
{
	return hwndRelated;
}

LRESULT CFakeWindow::OnHScroll(WPARAM wParam, LPARAM, HWND)
{
	return 0;
}

LRESULT CFakeWindow::OnMove(CPoint &pt)
{
	//RECT rc;
	//GetWindowRect(&rc);
	//::SetWindowPos(hwndRelated, NULL, rc.left, rc.top,  rc.right - rc.left + 1, rc.bottom - rc.top + 1, 0);
	return DefWindowProc();
}

LRESULT CFakeWindow::OnSize(UINT nType, CSize csz)
{
	Invalidate(false);
	return DefWindowProc();
}

LRESULT CFakeWindow::OnLButtonDown(UINT nFlags, CPoint point)
{
	fCaptured = true;
	SetCapture();
	ClientToScreen(&point);
	ptMousePos.x = point.x;
	ptMousePos.y = point.y;
	return 0;
}

LRESULT CFakeWindow::OnLButtonUp(UINT nFlags, CPoint point)
{
	ReleaseCapture();
	fCaptured = false;
	return 0;
}

LRESULT CFakeWindow::OnMouseLeave()
{
	ReleaseCapture();
	fCaptured = false;
	return 0;
}

LRESULT CFakeWindow::OnMouseMove(UINT nFlags, CPoint point)
{
	if (fCaptured){
		RECT rc;
        POINT ptCursor;
        POINT ptDelta;
 
        GetWindowRect(&rc);
        GetCursorPos(&ptCursor);
        ptDelta.x = ptMousePos.x - ptCursor.x;
        ptDelta.y = ptMousePos.y - ptCursor.y;
		/*rcTemp.bottom = rc.bottom - ptDelta.y;
		rcTemp.right = rc.right - ptDelta.x;
		rcTemp.top = rc.top - ptDelta.y;
		rcTemp.left = rc.left - ptDelta.x;*/
		SetWindowPos(HWND_TOP,  rc.left - ptDelta.x, rc.top - ptDelta.y, rc.right - rc.left,
				           rc.bottom - rc.top, 0);
		SetFocus();
        ptMousePos.x = ptCursor.x;
        ptMousePos.y = ptCursor.y;
	}
	return 0;
}

LRESULT CFakeWindow::OnWindowPosChanging(LPWINDOWPOS lpwp)
{
    /* Continue with default handling */
    return DefWindowProc();
}

LRESULT CFakeWindow::OnCreate(LPCREATESTRUCT kpcrstruct)
{
	return 0;
}

LRESULT CFakeWindow::OnKeyDown(WPARAM wParam, LPARAM lParam,  UINT nFlags)
{
	if (wParam == hkRelease){
		::SetWindowPos(hwndRelated, NULL, rcDefault.left, rcDefault.top,  
			           rcDefault.right - rcDefault.left + 1, rcDefault.bottom - rcDefault.top + 1, SWP_SHOWWINDOW);
		KillTimer(1);
		EndDialog(m_hWnd, 0);
	} else if (wParam == hkZoomOut){
		// go back  0x41 equals to 'A'
		RECT rc;
		GetWindowRect(&rc);
		if (!zoomWindow(rc, ZM_OUT))
			return 0;
		//::SetWindowPos(hwndRelated, HWND_BOTTOM, rc.left, rc.top,  rc.right - rc.left, rc.bottom - rc.top, 0);
		SetWindowPos(HWND_TOP, rc.left, rc.top, rc.right - rc.left,
				           rc.bottom - rc.top, 0);
		SetFocus();
	} else if (wParam == hkZoomIn){
		// go forward 0x44 equals to 'D'
		RECT rc;
		GetWindowRect(&rc);
		if (!zoomWindow(rc, ZM_IN))
			return 0;
		//::SetWindowPos(hwndRelated, HWND_BOTTOM, rc.left, rc.top,  rc.right - rc.left, rc.bottom - rc.top, 0);
		SetWindowPos(HWND_TOP, rc.left, rc.top, rc.right - rc.left,
				           rc.bottom - rc.top, 0);
		SetFocus();
	}
	return 0;
}

LRESULT CFakeWindow::OnNCPaint(HRGN wParam)
{
	return 0;
}

LRESULT CFakeWindow::OnPaint(HDC hDC)
{
	PAINTSTRUCT ps;
	RECT rect;
	hDC = BeginPaint(&ps);
	GetWindowRect(&rect);
		
	StretchBlt(hDC, 0,0, rect.right - rect.left, rect.bottom  - rect.top, hdcOffscreen, 0,0, 
		                 rcTemp.right - rcTemp.left, rcTemp.bottom - rcTemp.top, SRCCOPY); 

	EndPaint(&ps);
	return 0;
}

LRESULT CFakeWindow::OnTimer(UINT_PTR wParam)
{
	// TODO : Working on it
	HDC hDC = ::GetDC(hwndRelated);
	RECT rc;
	GetWindowRect(&rc);
    ::SetWindowPos(hwndRelated, HWND_BOTTOM, rc.left, rc.top,  rc.right - rc.left, rc.bottom - rc.top, SWP_NOACTIVATE | SWP_SHOWWINDOW);
	RECT rect;
	::GetWindowRect(hwndRelated, &rect);
	rcTemp.bottom = rect.bottom;
	rcTemp.left = rect.left;
	rcTemp.right = rect.right;
	rcTemp.top = rect.top;
	hdcOffscreen = CreateCompatibleDC(hDC);
	hBmpOffscreen = CreateCompatibleBitmap(hDC, rect.right - rect.left, rect.bottom - rect.top);
	HBITMAP hBmpOld = (HBITMAP)SelectObject(hdcOffscreen, hBmpOffscreen);
	PrintWindow(hwndRelated, hdcOffscreen, 0); 
	//BitBlt(hdcOffscreen, 0, 0, rect.right - rect.left, rect.bottom - rect.top, hDC, 0, 0, SRCCOPY);
	::ReleaseDC(hwndRelated, hDC);
	::ShowWindow(hwndRelated, SW_HIDE);
	Invalidate(true);
	return 0;
}

LRESULT CFakeWindow::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 1.0f * (rcDefault.right - rcDefault.left);
	lpMMI->ptMaxTrackSize.y = 1.0f * (rcDefault.bottom - rcDefault.top);
	lpMMI->ptMinTrackSize.x = 0.1f * (rcDefault.right - rcDefault.left);
	lpMMI->ptMinTrackSize.y = 0.1f * (rcDefault.bottom - rcDefault.top);
	return 0;
}

LRESULT CFakeWindow::OnEraseBackground(HDC hDC)
{
	return 0;
}

bool CFakeWindow::Init(HWND hwndRel, char chZI, char chZO, char chR){
	hwndRelated = hwndRel;

	hkZoomIn = chZI;
	hkZoomOut = chZO;
	hkRelease = chR;

	fCaptured = false;
	RECT rc;
	::GetWindowRect(hwndRel, &rc);
	rcDefault.bottom = rc.bottom;
	rcDefault.left = rc.left;
	rcDefault.right = rc.right;
	rcDefault.top = rc.top;
	rcTemp.bottom = rc.bottom;
	rcTemp.left = rc.left;
	rcTemp.right = rc.right;
	rcTemp.top = rc.top;
	HDC hDC = ::GetDC(hwndRelated);
	RECT rect;
	::GetWindowRect(hwndRelated, &rect);
	hdcOffscreen = CreateCompatibleDC(hDC);
	hBmpOffscreen = CreateCompatibleBitmap(hDC, rect.right - rect.left, rect.bottom - rect.top);
	HBITMAP hBmpOld = (HBITMAP)SelectObject(hdcOffscreen, hBmpOffscreen);
	PrintWindow(hwndRelated, hdcOffscreen, 0); 
	//BitBlt(hdcOffscreen, 0,0, rcDefault.right - rcDefault.left, rcDefault.bottom - rcDefault.top, hDC, 0, 0, SRCCOPY);
	::ReleaseDC(hwndRelated, hDC);
	::ShowWindow(hwndRelated, SW_HIDE);
	return true;
}

LRESULT CFakeWindow::OnClose()
{
	::SetWindowPos(hwndRelated, NULL, rcDefault.left, rcDefault.top,  
			           rcDefault.right - rcDefault.left, rcDefault.bottom - rcDefault.top, SWP_SHOWWINDOW);
	KillTimer(1);
	EndDialog(m_hWnd, 0);
	return 0;
}

LRESULT CFakeWindow::OnDestroy()
{
	::SetWindowPos(hwndRelated, NULL, rcDefault.left, rcDefault.top,  
			           rcDefault.right - rcDefault.left, rcDefault.bottom - rcDefault.top, SWP_SHOWWINDOW);
	KillTimer(1);
	EndDialog(m_hWnd, 0);
	return 0;
}

bool CFakeWindow::zoomWindow(RECT &rc, int zoomMode)
{
	RECT rcRes;
	int w = rc.right - rc.left;
	int h = rc.bottom - rc.top;
	float coefX = 9 * (rcDefault.right - rcDefault.left) / 1000;
	float coefY = 9 * (rcDefault.bottom - rcDefault.top) / 1000;
	int cxMinWndSize = (rcDefault.right - rcDefault.left)/10; //GetSystemMetrics(SM_CXMINTRACK);
	int cyMinWndSize = (rcDefault.bottom - rcDefault.top)/10; //GetSystemMetrics(SM_CYMINTRACK);
	int cxMaxWndSize = rcDefault.right - rcDefault.left; //GetSystemMetrics(SM_CXMAXTRACK);
	int cyMaxWndSize = rcDefault.bottom - rcDefault.top; //GetSystemMetrics(SM_CYMAXTRACK);

	switch(zoomMode){
	case ZM_NONE:
		return false;
		break;
	case ZM_OUT:
		rcRes.left = rc.left + int(coefX);
		rcRes.right = rc.right - int(coefX);
		rcRes.bottom = rc.bottom - int(coefY);
		rcRes.top = rc.top + int(coefY);
		if (((rcRes.right - rcRes.left) < cxMinWndSize + SHIFT_APPROX)||
			((rcRes.bottom - rcRes.top) < cyMinWndSize + SHIFT_APPROX)){
			// GETMINMAXINFO
			return false;
		}
		break;
	case ZM_IN:
		rcRes.left = rc.left - int(coefX);
		rcRes.right = rc.right + int(coefX);
		rcRes.bottom = rc.bottom + int(coefY);
		rcRes.top = rc.top - int(coefY);
		if (((rcRes.right - rcRes.left) > cxMaxWndSize)||
			((rcRes.bottom - rcRes.top) > cyMaxWndSize)){
			// GETMINMAXINFO
			return false;
		}
		break;
	default:
		return false;
	}
	rc.bottom = rcRes.bottom;
	rc.left = rcRes.left;
	rc.right = rcRes.right;
	rc.top = rcRes.top;
	return true;
}

int CFakeWindow::getGCD(int iA, int iB)
{
	int iC;
	while (iB) {
		iC = iA % iB;
		iA = iB;
		iB = iC;
    }
	iC = iA;
	return iC;
}