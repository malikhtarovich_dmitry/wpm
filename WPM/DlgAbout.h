/*! @file DlgAbout.h */

#ifndef __DLGABOUT_H__
#define __DLGABOUT_H__
#pragma once

/**
 * @addtogroup Dialog
 * @brief Dialog About
 * @{
 */

#include "StdAfx.h"
#include "resource.h"

class DlgAbout :     public CSimpleDialog<IDD_DLGABOUT>
{
public:
	BEGIN_MSG_MAP_EX(CDlgAbout)
		MSG_WM_INITDIALOG(OnInitDialog)
		COMMAND_RANGE_CODE_HANDLER_EX(IDOK, IDOK, BN_CLICKED, OnOK)
		CHAIN_MSG_MAP(CSimpleDialog<IDD_DLGABOUT>)
	END_MSG_MAP()

	LRESULT OnInitDialog(HWND hWnd, LPARAM)
	{
		return 0;
	}

	LRESULT OnOK(UINT, int, HWND)
	{
		return 0;
	}
};

#endif // !defined __DLGABOUT_H__

/*! @} */