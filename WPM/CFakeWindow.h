/*! @CFakeWindow.h */
#ifndef __CFAKEWINDOW_H__
#define __CFAKEWINDOW_H__
#pragma once

#include <Windows.h>
#include "StdAfx.h"
#include "resource.h"

#define SHIFT_APPROX		50
#define ZM_NONE				 0
#define ZM_OUT				 1
#define ZM_IN				 2
/**
 * @addtogroup Fake Window
 * @brief 
 * @{
 */

class CFakeWindow :    public CSimpleDialog<IDD_FAKEWINDOW>,
					   public COwnerDraw<CFakeWindow>
{
private:
	HWND hwndRelated;
	RECT rcDefault;
	RECT rcTemp;
	HBITMAP hbm;
	HBITMAP hBmpOffscreen;
	HDC hdcOffscreen;
	bool fCaptured;
	POINT ptMousePos;

	char hkZoomIn;
	char hkZoomOut;
	char hkRelease;
public:
	BEGIN_MSG_MAP_EX(CFakeWindow)
		MSG_WM_INITDIALOG(OnInitDialog)
		MSG_WM_HSCROLL(OnHScroll)
		MSG_WM_MOVE(OnMove)
		MSG_WM_WINDOWPOSCHANGING(OnWindowPosChanging)
		MSG_WM_KEYDOWN(OnKeyDown)
		MSG_WM_NCPAINT(OnNCPaint)
		MSG_WM_PAINT(OnPaint)
		MSG_WM_TIMER(OnTimer)
		MSG_WM_SIZE(OnSize)
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_MOUSEMOVE(OnMouseMove)
		MSG_WM_MOUSELEAVE(OnMouseLeave)
		MSG_WM_GETMINMAXINFO(OnGetMinMaxInfo)
		MSG_WM_CREATE(OnCreate)
		MSG_WM_ERASEBKGND(OnEraseBackground)
		MSG_WM_CLOSE(OnClose)
		MSG_WM_DESTROY(OnDestroy)


		CHAIN_MSG_MAP(CSimpleDialog<IDD_FAKEWINDOW>)
		CHAIN_MSG_MAP(COwnerDraw<CFakeWindow>)
	END_MSG_MAP()

	HWND getRelatedHWND();
	bool Init(HWND hwndRel, char chZI, char chZO, char chR);
	LRESULT OnCreate(LPCREATESTRUCT kpcrstruct);
	LRESULT OnMove(CPoint &pt);
	LRESULT OnSize(UINT nType, CSize csz); 
	LRESULT OnLButtonDown(UINT nFlags, CPoint point);
	LRESULT OnLButtonUp(UINT nFlags, CPoint point);
	LRESULT OnMouseMove(UINT nFlags, CPoint point);
	LRESULT OnMouseLeave();
	LRESULT OnWindowPosChanging(LPWINDOWPOS lParam);
	LRESULT OnNCPaint(HRGN wParam);
	LRESULT OnPaint(HDC hDC);
	LRESULT OnTimer(UINT_PTR wParam);
	LRESULT OnEraseBackground(HDC hDC);
	LRESULT OnInitDialog(HWND hWnd, LPARAM);
	LRESULT OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	LRESULT OnKeyDown(WPARAM wParam, LPARAM lParam,  UINT nFlags);
	LRESULT OnHScroll(WPARAM wParam, LPARAM, HWND);
	LRESULT OnClose();
	LRESULT OnDestroy();
private:
	bool zoomWindow(RECT &rc, int zoomMode);
	// Greatest Common Divisor
	int getGCD(int iA, int iB);
};

#endif // !defined __CFAKEWINDOW_H__

/*! @} */