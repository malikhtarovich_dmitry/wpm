/*! @file CApp.h */

#ifndef __CAPP_H__
#define __CAPP_H__
#pragma once

#include "DlgAbout.h"
#include "DlgSettings.h"
#include "WPControl.h"
#include <io.h>

#define MAX_RESSTR_SIZE		256
#define MAX_STR_SIZE		256
#define SHIFT_SIZE			30000

/**
 * @addtogroup Application
 * @brief The main class of application (initialization, messages handle, hooks management)
 * @{
 */

class CApp;
CApp *GetApp();

class CApp
{
public:
	CApp();
	~CApp();

	// full initialization
	bool Init(HINSTANCE hAInstance, LPSTR pszACmdLine);

	// loop of messages handle
	int Run();

	LRESULT WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	void ProcessMessages();

	void ProcessMessages(HWND hWnd, bool fWaitIfNoMessages = false);

	// Set Hooks; if hooks were successfully set then function returns TRUE, 
	// overwise, returns FALSE and all hooks will be unset
	BOOL HkSetHooks();
	
	// unset all hooks, if they were set
	void HkUnsetHooks();
	
	// Reset hooks;  if hooks were successfully reset then function returns TRUE,
	// overwise, returns FALSE and all hooks will be unset
	BOOL HkResetHooks();
		
	// Hooks work pauses (if fSuspend == TRUE) or
	// start up (if fSuspend == FALSE)
	void HkSuspendHooks(BOOL fSuspend);

	HWND getMainWnd() const { return m_hMainWnd; }
	HINSTANCE getInstance() { return m_hInstance; }
private:
	HINSTANCE m_hInstance;				  /**< Application Instance */ 
	char	  m_szExeFileName[MAX_PATH];  /**< Full Path and Name .exe */
	char*	  m_pszCommandLine;			  /**< full comand line */
	BOOL	  m_fIsOsX64;				  /**< Flag of x64 OS */

	char	  m_szTitle[MAX_RESSTR_SIZE]; /**< Text for button in Tray */
	HWND	  m_hMainWnd;				  /**< handle of the main window */
	HWND	  m_hHookProcWnd;			  /**< handle of the main window HookProc.exe */
	UINT	  m_dwKbdHookMsg;			  /**< message for KbdHook.dll */
	UINT	  m_dwHookProcMsg;			  /**< message for HookProc.exe */
	UINT	  m_dwTaskbarCreatedMsg;	  /**< message TaskbarCreated */
	BOOL	  m_fHooksSet;				  /**< flag of set hooks */
	UINT_PTR  m_dwFocusTimer;             /**< special timer */

	DlgSettings dlgSettings;
	DlgAbout dlgAbout;
	HWND fActiveWnd;
	HANDLE fMMF;
	DWORD fdwVer, fdwRelease, fdwBuild;
	BOOL fAlreadyRun;

	BOOL mMuted;
	BOOL mStartup;
	BOOL mLogOn;
	BOOL mAlreadyAppended;
	HMENU mHPopupMenu;
	BOOL mSettingsStillOpen;

	void SetFocusTimer();
	void KillFocusTimer();

	void VerToLog();
	bool InitInstance();
	void RegisterMessages();
	bool CheckAlreadyRun ();
	void DisableOrEnable();
	void HandleHook(WPARAM wPar, LPARAM lPar);
	void HandleMenu(HWND hWnd, WORD wmId);
	void UpdatePopupMenu(HMENU hmenu);
	void ShowTourOnRun();
	void OnCopyData(LPARAM ALPar);
	void OnPowerBroadcast(WPARAM wParam);
	void Register();
private:
	char m_hkCapture;
	char m_hkZoomOut;
	char m_hkZoomIn;
	char m_hkRelease;
};

#endif // !defined(__CAPP_H__)

/*! @} */