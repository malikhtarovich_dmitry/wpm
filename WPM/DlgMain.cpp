

#include "StdAfx.h"
#include "resource.h"
#include "DlgAbout.h"
#include "Help.h"
#include "Utils.h"
#include "Registry.h"
#include "DlgMain.h"
#include "UsageStat.h"
#include "Lang.h"

#define WIDTHSPLIT 3

////////////////////////////////////////////////////////////////////////////////

CDlgMain g_DlgMain;
CDlgMain *GetDlgMain() { return &g_DlgMain; };

////////////////////////////////////////////////////////////////////////////////

CDlgMain *gDlgMain;

LRESULT CALLBACK DialogMainProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	return gDlgMain->DlgProc(hDlg, message, wParam, lParam);}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDlgMain::CDlgMain() {
	gDlgMain = this;
	fDlg = NULL;
	fInst = GetModuleHandle (NULL);
	fBlockClose = FALSE;
	fBlockNotify = FALSE;
	fNeedForegroundDlgMain = FALSE;
	ReadPosFromReg();
}

////////////////////////////////////////////////////////////////////////////////

CDlgMain::~CDlgMain() {
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::ReadPosFromReg() {
	HDC hDC = GetDC(NULL);
	int vPixInchX = GetDeviceCaps(hDC, LOGPIXELSX);
	int vPixInchY = GetDeviceCaps(hDC, LOGPIXELSY);
	ReleaseDC(NULL, hDC);
	ReadPosWindowFromReg(7*vPixInchX, 5*vPixInchY, REGPATH_DLGMAIN, fPos);
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::WritePosToReg() {
	WritePosWindowToReg (REGPATH_DLGMAIN, fPos);
}

////////////////////////////////////////////////////////////////////////////////

void ShowMsgNotLoad (HWND AWnd, char *AFileName) {
	WCHAR bufText [MAXRES], bufText2[MAXRES+MAX_PATH], sHint[MAXRES];
	WCHAR *pFileNameW = AnsiToUnicode(AFileName);
	swprintf(bufText2, L"%s:\n\n%s\n\n%s", bufText, pFileNameW, sHint);
	free(pFileNameW);
	ShowMessageU(AWnd, bufText2, MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);
}

BOOL CDlgMain::Init(HWND AWnd)
{
	hParent = AWnd;
////	GetGlobData_Version(dwVer, dwRelease, dwBuild);
	LoadStringLangA(IDS_APP_TITLE, fTitle, sizeof(fTitle));


	GetUsageStat()->AddRun();
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::UpdateTitle() {
	char sTitle[MAX_RESSTR_SIZE];
	lstrcpy(sTitle, fTitle);
	SetWindowText(fDlg, sTitle);
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::RealignControls()
{
	RECT r;
	GetClientRect (fDlg, &r);
}

void AddHintDlg (HWND AWndTT, HWND AWnd, WORD AID) {
    TOOLINFO ti; 
	ti.cbSize = sizeof(TOOLINFO);
    ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND;
    ti.hinst = GetInstLang();
    ti.uId = (WPARAM)AWnd;
	ti.lpszText = MAKEINTRESOURCE(AID);
    SendMessage(AWndTT, TTM_ADDTOOL, 0, (LPARAM) (LPTOOLINFO) &ti);
} 

void CDlgMain::OnInitDialog()
{
	SetLastActiveWindow(fDlg);
	UpdateTitle();
	SendMessage(fDlg, WM_SETICON, ICON_SMALL, (LPARAM) IDI_TRAY_ICON);
	SendMessage(fDlg, WM_SETICON, ICON_BIG, (LPARAM) IDI_TRAY_ICON);

	//SetWindowLongPtr(fTreeView.GetHandle(), GWL_STYLE, GetWindowLongPtr(fTreeView.GetHandle(), GWL_STYLE) - TVS_SHOWSELALWAYS);

	HWND hTT = CreateHintWnd(fDlg); 
	
	ShowWindow(fPanelPwd, SW_HIDE);
	SetWindowPlacement(fDlg, &fPos);
	RECT r;	GetClientRect (fDlg, &r); int w = r.right-r.left; int h = r.bottom - r.top;
	
	RECT rH = {0, 0, 10, 8}; 

    SetWindowPos (fDlg, HWND_TOP, 0, 0, 1000, 600, SWP_NOMOVE);
	MapDialogRect(fDlg, &rH);
	RealignControls();
    RECT rect;
}





void CDlgMain::OnSize (WPARAM wParam, LPARAM lParam)
{
    if (wParam & SIZE_MINIMIZED)
        return;
	//�����, ����� ����� � ������� ������� ��������� - ������� ����� ���������.
	RealignControls();	
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnGetMinMaxInfo (LPARAM lParam)
{
	MINMAXINFO *mmi = (MINMAXINFO *) lParam;
	mmi->ptMinTrackSize.x = WIDTH_TEXT_IN_STATUS_BAR;
	mmi->ptMinTrackSize.y = 300;
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnMenuStat() {
	DWORD dwVol, dwSpeed; char sMsg[128];
	wsprintf(sMsg, "Volume: %d symbols\nSpeed: %d symbols per minute", dwVol, dwSpeed);
	//MessageBox(fDlg, sMsg,"", 0);
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnCommand (WPARAM wParam, LPARAM lParam) {
	if (HIWORD(wParam) == 0)
    { 
		switch (LOWORD(wParam)) {
//����
		case ID_ABOUT: 
			/*GetDlgAbout()->Execute(fDlg);*/ 
			break;
//������
		case IDOK: 
			break;			
		case IDCANCEL: if (fBlockClose) fBlockClose = FALSE; else CloseMainDlg();	break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::CloseMainDlg() {
	//CloseHelp(); //���-�� ����� ����� ���������. ��������, ����� ������ 
	//� ������ ���� ��� CallHelp ��� ������.
	GetWindowPlacement(fDlg, &fPos);
	WritePosToReg();
	RestoreLastActiveWindow();
	DefWindowProc(fDlg, WM_SYSCOMMAND, SC_MINIMIZE, 0);
	EndDialog(fDlg, IDCANCEL); 
}

void CDlgMain::OnSysCommand (WPARAM wParam, LPARAM lParam) {
	if (wParam == SC_MINIMIZE) CloseMainDlg();
}



////////////////////////////////////////////////////////////////////////////////

LRESULT CDlgMain::OnNotify (WPARAM wParam, LPARAM lParam)
{
	if (fBlockNotify) return 0;
	LPNMHDR pnm = (LPNMHDR) lParam;
	switch (pnm->idFrom)
    {
	}
	switch (pnm->code)
    {
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnInitMenu (HMENU AMenu) {
#define MF_CHECK MF_BYCOMMAND | MF_CHECKED
#define MF_UNCHECK MF_BYCOMMAND | MF_UNCHECKED 
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnMenuSelect(WORD AID) {
	UINT idH;
	switch (AID) {
	case ID_EXIT:			/*idH = IDH_FILE_EXIT;*/ break;
	default: return; 
	}
}

////////////////////////////////////////////////////////////////////////////////
// ����� ���������� ���� � ���������� � ����������� �� ���� ��� ����� ����� ������ ������.
////////////////////////////////////////////////////////////////////////////////

BOOL CDlgMain::OnSetCursor (HWND AWnd)
{
	return FALSE;
}


void CDlgMain::OnContextMenu (HWND AWnd, LPARAM lParam) {
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnMouseMove (WPARAM wParam, LPARAM lParam) {
	POINT pt = {LOWORD(lParam), HIWORD(lParam)};
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::OnMouseLUp (WPARAM wParam, LPARAM lParam) {
}



LRESULT CALLBACK CDlgMain::DlgProc (HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	fDlg = hDlg;
    static bool findExec = false;

	switch (message)
    {
        case WM_ACTIVATE:
            //GetDlgFindSimple ()->Focus ();
            break;
		case WM_TIMER:
			{
			KillTimer(fDlg, 1);
			// TODO: single NM_CLICK message handler
			}
			break;
		case WM_INITDIALOG:
			OnInitDialog();
			findExec = true;
            break;
		case WM_COMMAND:
            OnCommand (wParam, lParam);
            break;
		case WM_SYSCOMMAND:
            OnSysCommand (wParam, lParam);
            break;
		case WM_SIZE:
            OnSize (wParam, lParam);
            break;
		case WM_GETMINMAXINFO:
            OnGetMinMaxInfo (lParam);
            break;
		case WM_NOTIFY:
            return OnNotify (wParam, lParam); 
		case WM_INITMENU:
            OnInitMenu ((HMENU) wParam);
            break;  
		case WM_MENUSELECT: OnMenuSelect (LOWORD(wParam)); break;
		case WM_SETCURSOR: return OnSetCursor ((HWND) wParam); 
		case WM_CONTEXTMENU: OnContextMenu ((HWND)wParam, lParam); break;
		case WM_MOUSEMOVE: OnMouseMove (wParam, lParam); break; 
		case WM_LBUTTONUP: OnMouseLUp (wParam, lParam); break;
		case WM_HELP:	/*CallHelp(hDlg, "mainwnd.htm"); return TRUE;*/
		default: fDialogIsShowing = FALSE;
            break;
	}
    return 0;
}



void CDlgMain::SetModified()
{
}

////////////////////////////////////////////////////////////////////////////////

void CDlgMain::ShowDialog() {
}

void CDlgMain::Execute ()
{
	if (fDialogIsShowing)
        SetForegroundWindowExt (fDlg);
    else
        ShowDialog ();
}

