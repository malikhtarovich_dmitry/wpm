#include "StdAfx.h"
#include "resource.h"
#include "Registry.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

int g_Anim = -1;
int g_LogOn = -1;
BOOL g_ReadCustomBuild = FALSE;
char g_AffCode[32] = {0};
char g_AffBannerTitle[512] = {0};
char g_AffBannerDescr[512] = {0};
char g_AffBannerURL[512] = {0};
//----------------------------------------------------------------------------------------

void ReadFromReg(HKEY hKeyGlob, LPCTSTR lpSubKey, LPCTSTR lpValueName, 
				 LPSTR lpStr, DWORD nSzStr)
{
	HKEY hKey;
	DWORD Disp;
	lpStr[0] = 0;
	if (RegCreateKeyEx(hKeyGlob, lpSubKey, 0, NULL,	REG_OPTION_NON_VOLATILE, 
		KEY_QUERY_VALUE, NULL, &hKey, &Disp) != ERROR_SUCCESS) return;
	__try {		
		if (RegQueryValueEx(hKey, lpValueName, 0, NULL, (BYTE*)lpStr, &nSzStr) 
			!= ERROR_SUCCESS) lpStr[0] = 0;
	} __finally {RegCloseKey(hKey);}
}

////////////////////////////////////////////////////////////////////////////////

void WriteStrToReg (HKEY hKeyGlob, LPCTSTR lpSubKey, LPCTSTR lpValueName, LPCSTR lpStr)
{
	HKEY hKey;
	DWORD Disp;
	if (RegCreateKeyEx(hKeyGlob, lpSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, 
		KEY_QUERY_VALUE | KEY_SET_VALUE, NULL, &hKey, &Disp) != ERROR_SUCCESS) return;
	__try {		
		RegSetValueEx(hKey, lpValueName, 0, REG_SZ, (BYTE*)lpStr, strlen(lpStr)+1);
	} __finally {RegCloseKey(hKey);}
}

////////////////////////////////////////////////////////////////////////////////

void ReadStrFromRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, LPSTR lpStr, DWORD nSzStr) {
	ReadFromReg (HKEY_CURRENT_USER, lpSubKey, lpValueName, lpStr, nSzStr);
}

////////////////////////////////////////////////////////////////////////////////

void WriteStrToRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, LPCSTR lpStr) {
	WriteStrToReg (HKEY_CURRENT_USER, lpSubKey, lpValueName, lpStr);
}

////////////////////////////////////////////////////////////////////////////////

int ReadIntFromRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, int ADefVal) {
	int res = ADefVal; char sInt[32];
	ReadFromReg (HKEY_CURRENT_USER, lpSubKey, lpValueName, sInt, sizeof(sInt));
	if (lstrlen(sInt)) res = atoi(sInt);
	return res;
}

////////////////////////////////////////////////////////////////////////////////

void WriteIntToRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, UINT AVal)
{
	HKEY hKey;
	DWORD Disp;
	char sInt[32];

	_itoa(AVal, sInt, 10);
	if (RegCreateKeyEx(HKEY_CURRENT_USER, lpSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, 
		KEY_QUERY_VALUE | KEY_SET_VALUE, NULL, &hKey, &Disp) != ERROR_SUCCESS) return;
	__try {		
		RegSetValueEx(hKey, lpValueName, 0, REG_SZ, (BYTE*)sInt, strlen(sInt)+1);
	} __finally {RegCloseKey(hKey);}
}

////////////////////////////////////////////////////////////////////////////////

BOOL ReadMemFromRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, 
						 LPVOID lpBuf, DWORD nSzBuf) {
	HKEY hKey; DWORD Disp; BOOL bOk;
	ZeroMemory(lpBuf, nSzBuf);
	if (RegCreateKeyEx(HKEY_CURRENT_USER, lpSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, 
		KEY_QUERY_VALUE, NULL, &hKey, &Disp) != ERROR_SUCCESS) return FALSE;
	__try {		
		bOk = RegQueryValueEx(hKey, lpValueName, 0, NULL, (BYTE*)lpBuf, &nSzBuf) 
			== ERROR_SUCCESS;
	} __finally {RegCloseKey(hKey);}
	return bOk;
}

////////////////////////////////////////////////////////////////////////////////

BOOL WriteMemToRegHKCU (LPCTSTR lpSubKey, LPCTSTR lpValueName, 
						LPVOID lpBuf, DWORD nSzBuf) {
	HKEY hKey; DWORD Disp; BOOL bOk;	
	if (RegCreateKeyEx(HKEY_CURRENT_USER, lpSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, 
		KEY_QUERY_VALUE | KEY_SET_VALUE, NULL, &hKey, &Disp) != ERROR_SUCCESS) return FALSE;
	__try {		
		bOk = RegSetValueEx(hKey, lpValueName, 0, REG_BINARY, (BYTE*)lpBuf, nSzBuf)
			== ERROR_SUCCESS;
	} __finally {RegCloseKey(hKey);}
	return bOk;
}

////////////////////////////////////////////////////////////////////////////////

BOOL SetStartUp(BOOL AStartUp)
{
	HKEY hKey;
	BOOL fRes = FALSE;
	char szRunPath[MAX_PATH];
	
	if ( RegOpenKeyEx(HKEY_CURRENT_USER, REGPATH_STARTUP, 0, KEY_WRITE,
		&hKey) == ERROR_SUCCESS )
	{
		szRunPath[0] = '\"';
		GetModuleFileName(NULL, &szRunPath[1], sizeof(szRunPath) - 1);
		ExtractFilePath(&szRunPath[1], &szRunPath[1]);
		lstrcat(szRunPath, "Launcher.exe\"");
		if (AStartUp)
			fRes = ( RegSetValueEx(hKey, "WPM", 0, REG_SZ, (BYTE*) szRunPath,
				lstrlen(szRunPath)) == ERROR_SUCCESS );
		else
			fRes = (RegDeleteValue(hKey, "WPM") == ERROR_SUCCESS);
		RegCloseKey(hKey);
	}
	return fRes;
}

////////////////////////////////////////////////////////////////////////////////

BOOL GetStartUp()
{
	HKEY hKey;
	BOOL fRes = FALSE;
	char szRunPath[MAX_PATH], szBuf[MAX_PATH];
	DWORD dwBufLen = sizeof(szBuf);

	if ( RegOpenKeyEx(HKEY_CURRENT_USER, REGPATH_STARTUP, 0, KEY_QUERY_VALUE,
		&hKey) == ERROR_SUCCESS )
	{
		if ( RegQueryValueEx(hKey, "WPM", NULL, NULL, (BYTE*) szBuf,
			&dwBufLen) == ERROR_SUCCESS )
		{
			GetModuleFileName(NULL, szRunPath, sizeof(szRunPath));
			ExtractFilePath(szRunPath, szRunPath);
			lstrcat(szRunPath, "Launcher.exe\"");
			fRes = (lstrcmpi(szRunPath, &szBuf[1]) == 0);
		}
		RegCloseKey(hKey);
	}
	return fRes;
}

////////////////////////////////////////////////////////////////////////////////

BOOL GetAnimated() {
	if (g_Anim == -1) g_Anim = ReadIntFromRegHKCU(REGPATH, REGPAR_ANIM, FALSE);
	return g_Anim;
}

////////////////////////////////////////////////////////////////////////////////

void SetAnimated(BOOL AAnim) {
	g_Anim = AAnim;
	WriteIntToRegHKCU(REGPATH, REGPAR_ANIM, AAnim);
}

////////////////////////////////////////////////////////////////////////////////

BOOL GetLogInfo() {
	if (g_LogOn == -1) g_LogOn = ReadIntFromRegHKCU(REGPATH, REGPAR_LOGFILE, FALSE);
	return g_LogOn;
}

////////////////////////////////////////////////////////////////////////////////

void SetLogInfo(BOOL LogOn) {
	g_LogOn = LogOn;
	WriteIntToRegHKCU(REGPATH, REGPAR_LOGFILE, LogOn);
}

////////////////////////////////////////////////////////////////////////////////

void ReadPosWindowFromReg(int ADefaultWidth, int ADefaultHeight, LPCTSTR ASubKey,
						  WINDOWPLACEMENT &APos)
{
	int DefW = ADefaultWidth, DefH = ADefaultHeight, 
		DefL = (GetSystemMetrics(SM_CXSCREEN) - DefW) / 2,
		DefT = (GetSystemMetrics(SM_CYSCREEN) - DefH) / 2;
	APos.length = sizeof(WINDOWPLACEMENT);
	APos.flags = 0;
	APos.showCmd = ReadIntFromRegHKCU (ASubKey, REGPAR_SHOWCMD, SW_SHOWNORMAL);
	APos.ptMaxPosition.x = ReadIntFromRegHKCU (ASubKey, REGPAR_POSMAXX, -1);
	APos.ptMaxPosition.y = ReadIntFromRegHKCU (ASubKey, REGPAR_POSMAXY, -1);
	APos.ptMinPosition.x = ReadIntFromRegHKCU (ASubKey, REGPAR_POSMINX, -1);
	APos.ptMinPosition.y = ReadIntFromRegHKCU (ASubKey, REGPAR_POSMINY, -1);
	APos.rcNormalPosition.left=ReadIntFromRegHKCU(ASubKey, REGPAR_POSL, DefL);
	APos.rcNormalPosition.top=ReadIntFromRegHKCU(ASubKey, REGPAR_POST, DefT);
	APos.rcNormalPosition.right=ReadIntFromRegHKCU(ASubKey, REGPAR_POSR, DefL+DefW);
	APos.rcNormalPosition.bottom=ReadIntFromRegHKCU(ASubKey, REGPAR_POSB, DefT+DefH);
// �������� �� ������ ������
	int w = GetSystemMetrics(SM_CXFULLSCREEN),
		h = GetSystemMetrics(SM_CYFULLSCREEN), g = 20;
	if (APos.rcNormalPosition.right < g) APos.rcNormalPosition.right = g;
	if (APos.rcNormalPosition.bottom < g) APos.rcNormalPosition.bottom = g;
	if (APos.rcNormalPosition.left > w-g) APos.rcNormalPosition.left = w-g;
	if (APos.rcNormalPosition.top > h-g) APos.rcNormalPosition.top = h-g;
	if (APos.showCmd == SW_HIDE) APos.showCmd = SW_SHOW;
}

////////////////////////////////////////////////////////////////////////////////

void WritePosWindowToReg(LPCTSTR ASubKey, WINDOWPLACEMENT &APos)
{
	WriteIntToRegHKCU (ASubKey, REGPAR_SHOWCMD, APos.showCmd);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSMAXX, APos.ptMaxPosition.x);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSMAXY, APos.ptMaxPosition.y);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSMINX, APos.ptMinPosition.x);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSMINY, APos.ptMinPosition.y);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSL, APos.rcNormalPosition.left);
	WriteIntToRegHKCU (ASubKey, REGPAR_POST, APos.rcNormalPosition.top);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSR, APos.rcNormalPosition.right);
	WriteIntToRegHKCU (ASubKey, REGPAR_POSB, APos.rcNormalPosition.bottom);
}

////////////////////////////////////////////////////////////////////////////////

void ReadCustomBuild() {
	g_ReadCustomBuild = TRUE;
	char s[1024] = {0}, *p1, *p2; int l;
	ReadStrFromRegHKCU (REGPATH, REGPAR_CUSTOMBUILD, s, sizeof(s)-1);
	if (lstrlen(s) < 1) return;
	p1 = s;	p2 = strchr(p1, '|'); if (!p2) p2 = p1 + lstrlen(p1);
	l = sizeof(g_AffCode)-1; if (l > (p2-p1)) l = (p2-p1);
	lstrcpyn(g_AffCode, p1, l+1); 

	if (*p2 == 0) return;
	p1 = p2+1;	p2 = strchr(p1, '|'); if (!p2) p2 = p1 + lstrlen(p1);
	l = sizeof(g_AffBannerTitle)-1; if (l > (p2-p1)) l = (p2-p1);
	lstrcpyn(g_AffBannerTitle, p1, l+1); 

	if (*p2 == 0) return;
	p1 = p2+1;	p2 = strchr(p1, '|'); if (!p2) p2 = p1 + lstrlen(p1);
	l = sizeof(g_AffBannerDescr)-1; if (l > (p2-p1)) l = (p2-p1);
	lstrcpyn(g_AffBannerDescr, p1, l+1); 

	if (*p2 == 0) return;
	p1 = p2+1;	p2 = strchr(p1, '|'); if (!p2) p2 = p1 + lstrlen(p1);
	l = sizeof(g_AffBannerURL)-1; if (l > (p2-p1)) l = (p2-p1);
	lstrcpyn(g_AffBannerURL, p1, l+1); 
}

char* GetAffiliateCode()
{
	if (!g_ReadCustomBuild) ReadCustomBuild();
	return g_AffCode;
}

BOOL IsBannerExists()
{
	if (!g_ReadCustomBuild) ReadCustomBuild();

	return (lstrlen(g_AffBannerTitle) && 
			lstrlen(g_AffBannerDescr) && 
			lstrlen(g_AffBannerURL));
}

char *GetBannerTitle() {
	if (!g_ReadCustomBuild) ReadCustomBuild();
	return g_AffBannerTitle;
}

char *GetBannerDescr() {
	if (!g_ReadCustomBuild) ReadCustomBuild();
	return g_AffBannerDescr;
}

char *GetBannerURL() {
	if (!g_ReadCustomBuild) ReadCustomBuild();
	return g_AffBannerURL;
}

////////////////////////////////////////////////////////////////////////////////

void GetFolderMyDoc(char *pcPath, DWORD nSzBuf)
{
	ReadFromReg (HKEY_CURRENT_USER, 
		"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders",
		"Personal", pcPath, nSzBuf);
	if (!lstrlen(pcPath)) 
		ReadFromReg (HKEY_USERS, 
		".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders",
		"Personal", pcPath, nSzBuf);
	if (!lstrlen(pcPath))
		lstrcpy(pcPath, "C:\\My Documents");
}