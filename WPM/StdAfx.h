/** @file StdAfx.h */ 

#ifndef __STDAFX_H__
#define __STDAFX_H__
#pragma once

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <CommCtrl.h>
#include <ShellApi.h>
#include <stdlib.h>
#include <ctype.h>

#include "TraceLogger.h"

#ifdef NDEBUG
	#pragma optimize("gy", on)
#endif

#include <atlbase.h>
#include <atlapp.h>

#include <atlwin.h>
#include <atlframe.h>
#include <atlctrls.h>
#include <atlcrack.h>
#include <atlmisc.h>
#include <atlddx.h>
#include <atldlgs.h>


#define BUILD_LANG_ENG				/**< Language (RUS, ENG). */

#define MAXRES				256
#define MAX_RESSTR_SIZE		256		/**< Max size of string in resourse, including \0 */

#define STR(X) (LPSTR)X.c_str()
#define MALLOCWSTR(L) (LPWSTR)malloc((L)*sizeof(WCHAR))

#define WM_TRAYICON		0xB040

#endif // !defined(__STDAFX_H__)