#ifndef __UTILS_H__
#define __UTILS_H__

//----------------------------------------------------------------------------------------
// ��������� �������
//----------------------------------------------------------------------------------------

/*
	���������� �� ������� ����� ����� pszFileName ������� ���� � pszFilePath
	(���� ���� ������������ �� ����������� \ ��� :, �� �� ���������� � ����)
*/
void ExtractFilePath(LPCSTR pszFileName, LPSTR pszFilePath);

//----------------------------------------------------------------------------------------
// �������� �������
//----------------------------------------------------------------------------------------

/*
	�������� ������������� ����� � ������ pszFileName; ���� ���� ����������,
	������� ������ TRUE, � ������ FALSE, ���� ���������� ����� ���
*/
bool FileExists(LPCSTR pszFileName);

/*
	�������� ������������� ���������� � ������ pszFileName; ���� ���������� ����������,
	������� ������ TRUE, � ������ FALSE, ���� ��������� ���������� ���
*/
bool DirectoryExists(LPCSTR pszPathName);

/*
	�������� ���������� ��������� �����; pszPathName ��������� �� ������, ����������
	���� � ��� ����������� �����; ������� ������ TRUE ���� ����� ��� ���������� ���
	���� ������� � ������ FALSE ���� ����� ������� �� �������
*/
BOOL CreateComplexDirectory(LPCSTR pszPathName);

//----------------------------------------------------------------------------------------
// ������ �������
//----------------------------------------------------------------------------------------

/*
	��������� ������ ����� .exe pszFileName; � *dwVerHi ������� ���������� ���
	������� ����� ������ �����, � *dwVerLo - ��� ������� ����� ������ �����
*/
void GetFileVersion(const char* pszFileName, DWORD* dwVerHi, DWORD* dwVerLo);

//----------------------------------------------------------------------------------------

int GetTextWidth (HWND hWnd, char *Text);
int GetTextHeight(HWND hWnd, char *Text);
int GetTextWidth (HWND hWnd, HANDLE AFont, char *Text);
SIZE GetTextSize (HWND hWnd, HANDLE AFont, char *Text);
BOOL IsHighColor (HWND AWnd);
BOOL IsComctl470AndLater();

HWND GetLastActiveWindow();
void SetLastActiveWindow(HWND hWnd);
void RestoreLastActiveWindow();
BOOL CloseDialog (HWND hDlg, INT_PTR nResult);

HWND CreateHintWnd(HWND hwnd);
void AddHint (HWND AWndTT, HWND AWndCtrl, UINT AuID);

void InitDlgIconAndMenu (HWND ADlg);


void SetLastForegroundWindow(HWND AWnd);
void SetLastFocusWindow(HWND AWnd);
HWND GetLastFocusWindow();
void SetLastForegroundWindowSelf (HWND AWnd);
BOOL SwitchToLastFocusWnd();
void SetForegroundWindowExt(HWND AWnd);
void GetFocusWnd(HWND &AWndForeground, HWND &AWndFocus);
void SetFocusWnd(HWND AWndForeground, HWND AWndFocus);
BOOL AppIsForeground();
BOOL AppIsLastForeground();

void SetCheckBox (HWND AWnd, BOOL ASet);
BOOL GetCheckBox(HWND AWnd);

void ShowLastError();

#endif //!defined __UTILS_H__