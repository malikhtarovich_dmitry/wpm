//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WPM.rc
//
#define IDR_POPUP_MENU                  101
#define IDS_APP_TITLE                   104
#define IDS_HOOKPROC_ERROR              105
#define IDD_MAIN                        105
#define IDS_SETHOOKS_ERROR              106
#define IDD_ABOUT                       106
#define IDD_DLGABOUT                    106
#define IDS_HELPNAME                    107
#define IDH_HELP_CONTENTS               108
#define IDB_PNG                         108
#define IDH_HELP_SUPPORT                109
#define IDD_DIALOG1                     109
#define IDD_SETTINGS                    109
#define IDH_HELP_ABOUT                  110
#define IDI_TRAY_ICON                   110
#define IDS_ORDERLINK                   111
#define IDD_DIALOG2                     111
#define IDD_FAKEWINDOW                  111
#define IDS_VERSIONABOUT                112
#define IDC_COPYRIGHT                   1001
#define IDC_SYSLINK                     1002
#define IDC_STATICHKGR                  1003
#define IDC_EDIT_CAPTURE                1004
#define IDC_EDIT_ZOOMOUT                1005
#define IDC_EDIT_ZOOMIN                 1006
#define IDC_EDIT_CAPTURE4               1007
#define IDC_EDIT_RELEASE                1007
#define ID_ABOUT                        40005
#define ID_STARTUP                      40006
#define ID_EXIT                         40007
#define ID_SETTINGS                     40009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40010
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
