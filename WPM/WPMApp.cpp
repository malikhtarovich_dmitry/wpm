#include "StdAfx.h"
#include "CApp.h"

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
					 LPSTR lpCmdLine, int nCmdShow)
{
	CApp App;
	if (App.Init(hInstance, lpCmdLine))
		return App.Run();
	else
		return 0;
}