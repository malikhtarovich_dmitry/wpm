#include "StdAfx.h"
#include <Richedit.h>
#include "resource.h"
#include "Lang.h"
#include "Help.h"
#include "Utils.h"
#include "Registry.h"

#define MAXLENLANGNAME 64

BOOL g_IsOSUnicode = FALSE;
HINSTANCE g_InstLang = NULL;
HINSTANCE g_InstLangDef = NULL;
char g_sLang [MAXLENLANGNAME] = {0};
char g_sLangItem [MAXLENLANGNAME] = {0};
char g_sMaskPathLang[MAX_PATH] = {0};
HMENU g_hSubMenu = NULL;

void TryLoadLang()
{
	static BOOL bFirst = TRUE;
	if ( (g_InstLang) && (g_InstLang != g_InstLangDef) ) FreeLibrary (g_InstLang);
	if ( (lstrlen(g_sLang) > 0) && (lstrcmpi(g_sLang, "English")!=0) ) {
		g_InstLang = g_InstLangDef;
		char sPathLang[MAX_RESSTR_SIZE], sFileNameApp[MAX_PATH] = {0};
		DWORD dwDllVerHi, dwDllVerLo, dwAppVerHi, dwAppVerLo;
		lstrcpy(sPathLang, "Lngs\\");
		lstrcat(sPathLang, g_sLang);
		lstrcat(sPathLang, ".lng");
		GetFileVersion(sPathLang, &dwDllVerHi, &dwDllVerLo);
		GetModuleFileName(NULL, sFileNameApp, sizeof(sFileNameApp));
		GetFileVersion(sFileNameApp, &dwAppVerHi, &dwAppVerLo);
		if ((dwDllVerHi == dwAppVerHi) && (HIWORD(dwDllVerLo) == HIWORD(dwAppVerLo)))
			g_InstLang = LoadLibrary(sPathLang);
	}
	else
		g_InstLang = g_InstLangDef;
	if (!g_InstLang) g_InstLang = g_InstLangDef;
	if (g_InstLang == g_InstLangDef) lstrcpy(g_sLang, "English");
	if (bFirst)	bFirst = FALSE;
}

void InitInstLang() {
	g_InstLang = NULL;
	g_InstLangDef = GetModuleHandle (NULL);
	ReadFromReg (HKEY_CURRENT_USER, REGPATH, REGPAR_LANG, g_sLang, sizeof(g_sLang));
	char path[MAX_PATH], drive[_MAX_DRIVE],	dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	GetModuleFileName(0, path, sizeof(path));
	_splitpath(path, drive, dir, fname, ext);
	lstrcpy(g_sMaskPathLang, drive);
	lstrcat(g_sMaskPathLang, dir);
	lstrcat(g_sMaskPathLang, "Lngs\\*.lng");
	TryLoadLang();
}

HINSTANCE GetInstLang() { if (!g_InstLang) InitInstLang(); return g_InstLang; }

int LoadStringLangW (UINT uID, LPWSTR lpBuffer, int nBufferMax) {
	if (!g_InstLang) InitInstLang();
	int res = LoadStringW(g_InstLang, uID, lpBuffer, nBufferMax);
	if (!res) res = LoadStringW(g_InstLangDef, uID, lpBuffer, nBufferMax);
	return res;
}

int LoadStringLangA (UINT uID, LPSTR lpBuffer, int nBufferMax) {
	if (!g_InstLang) InitInstLang();
	int res = LoadStringA(g_InstLang, uID, lpBuffer, nBufferMax);
	if (!res) res = LoadStringA(g_InstLangDef, uID, lpBuffer, nBufferMax);
	return res;
}

////////////////////////////////////////////////////////////////////////////////

void SelectLanguageItem (WORD AId) {
	char s[64];
	GetMenuString(g_hSubMenu, AId, s, sizeof(s), MF_BYCOMMAND);
	lstrcpy(g_sLangItem, s);
}

////////////////////////////////////////////////////////////////////////////////

void SelectLanguage () {
	//char s[64];	GetMenuString(g_hSubMenu, AId, s, sizeof(s), MF_BYCOMMAND);	lstrcpy(g_sLang, s);
	lstrcpy(g_sLang, g_sLangItem);
	TryLoadLang();
	WriteStrToReg (HKEY_CURRENT_USER, REGPATH, REGPAR_LANG, g_sLang);
	UpdateHelpFileName();
}

////////////////////////////////////////////////////////////////////////////////

void SetFlagUnicode (BOOL AUnicode)
{
	g_IsOSUnicode = AUnicode;
}

BOOL IsOSUnicode()
{
	return g_IsOSUnicode;
}

////////////////////////////////////////////////////////////////////////////////

int ShowMessageU(HWND hWnd, UINT uID, UINT uType) {
	if (IsOSUnicode()) {
		WCHAR bufText [MAX_RESSTR_SIZE], bufCapt [MAX_RESSTR_SIZE];
		LoadStringLangW (IDS_APP_TITLE, bufCapt, SIZEOFW(bufCapt));
		LoadStringLangW (uID, bufText, SIZEOFW(bufText));
		return MessageBoxW(hWnd, bufText, bufCapt, uType);
	} else {
		char bufText [MAX_RESSTR_SIZE], bufCapt [MAX_RESSTR_SIZE];
		LoadStringLangA (IDS_APP_TITLE, bufCapt, sizeof(bufCapt));
		LoadStringLangA (uID, bufText, sizeof(bufText));
		return MessageBoxA(hWnd, bufText, bufCapt, uType);
	}
}

////////////////////////////////////////////////////////////////////////////////

int ShowMessageU(HWND hWnd, UINT uID, UINT uType, int AInt) {
	if (IsOSUnicode()) {
		WCHAR bufMsg[MAX_RESSTR_SIZE], bufText[MAX_RESSTR_SIZE], bufCapt[MAX_RESSTR_SIZE];
		LoadStringLangW (IDS_APP_TITLE, bufCapt, SIZEOFW(bufCapt));
		LoadStringLangW (uID, bufText, SIZEOFW(bufText));
		wsprintfW(bufMsg, bufText, AInt);
		return MessageBoxW(hWnd, bufMsg, bufCapt, uType);
	} else {
		char bufMsg [MAX_RESSTR_SIZE], bufText [MAX_RESSTR_SIZE], bufCapt [MAX_RESSTR_SIZE];
		LoadStringLangA (IDS_APP_TITLE, bufCapt, sizeof(bufCapt));
		LoadStringLangA (uID, bufText, sizeof(bufText));
		wsprintfA(bufMsg, bufText, AInt);
		return MessageBoxA(hWnd, bufMsg, bufCapt, uType);
	}
}

////////////////////////////////////////////////////////////////////////////////

int ShowMessageU(HWND hWnd, UINT uID, UINT uType, char *AStr)
{
	if (IsOSUnicode()) {
		WCHAR bufMsg[MAX_RESSTR_SIZE], bufText[MAX_RESSTR_SIZE], bufCapt[MAX_RESSTR_SIZE],
			bufStr[MAX_RESSTR_SIZE];
		wsprintfW(bufStr, L"%hs", AStr);
		LoadStringLangW (IDS_APP_TITLE, bufCapt, SIZEOFW(bufCapt));
		LoadStringLangW (uID, bufText, SIZEOFW(bufText));
		wsprintfW(bufMsg, bufText, bufStr);
		return MessageBoxW(hWnd, bufMsg, bufCapt, uType);
	} else {
		char bufMsg [MAX_RESSTR_SIZE], bufText [MAX_RESSTR_SIZE], bufCapt [MAX_RESSTR_SIZE];
		LoadStringLangA (IDS_APP_TITLE, bufCapt, sizeof(bufCapt));
		LoadStringLangA (uID, bufText, sizeof(bufText));
		wsprintfA(bufMsg, bufText, AStr);
		return MessageBoxA(hWnd, bufMsg, bufCapt, uType);
	}
}

////////////////////////////////////////////////////////////////////////////////

int ShowMessageU (HWND hWnd, LPWSTR AStr, UINT uType)
{
	if (IsOSUnicode())
	{
		WCHAR bufCapt[MAXRES];
		LoadStringLangW(IDS_APP_TITLE, bufCapt, SIZEOFW(bufCapt));
		return MessageBoxW(hWnd, AStr, bufCapt, uType);
	}
	else
	{
		char bufCapt[MAXRES], *pA = UnicodeToAnsi(AStr);
		LoadStringLangA (IDS_APP_TITLE, bufCapt, sizeof(bufCapt));
		int res = MessageBoxA(hWnd, pA, bufCapt, uType);
		free(pA);
		return res;
	}
}

////////////////////////////////////////////////////////////////////////////////

int DialogBoxLangU (UINT uID, HWND AWnd, DLGPROC ADlgProc)
{
	if (!g_InstLang) InitInstLang();
	int res;
	if (IsOSUnicode())
	{
		if (((res = DialogBoxW(g_InstLang, (LPCWSTR)uID, AWnd, ADlgProc)) == -1) && 
			(GetLastError() == ERROR_RESOURCE_NAME_NOT_FOUND) ) 
			res = DialogBoxW(g_InstLangDef, (LPCWSTR)uID, AWnd, ADlgProc);
	} else {
		if (((res = DialogBoxA(g_InstLang, (LPCTSTR)uID, AWnd, ADlgProc)) == -1) && 
			(GetLastError() == ERROR_RESOURCE_NAME_NOT_FOUND) ) 
			res = DialogBoxA(g_InstLangDef, (LPCTSTR)uID, AWnd, ADlgProc);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

HWND CreateDialogLangU (UINT uID, HWND AWnd, DLGPROC ADlgProc) {
	if (!g_InstLang) InitInstLang();
	HWND res;
	if (IsOSUnicode()) {
		if (!(res = CreateDialogW(g_InstLang, (LPCWSTR)uID, AWnd, ADlgProc)) && 
			(GetLastError() == ERROR_RESOURCE_NAME_NOT_FOUND) ) 
			res = CreateDialogW(g_InstLangDef, (LPCWSTR)uID, AWnd, ADlgProc);
	} else {
		if (!(res = CreateDialogA(g_InstLang, (LPCTSTR)uID, AWnd, ADlgProc)) && 
			(GetLastError() == ERROR_RESOURCE_NAME_NOT_FOUND) ) 
			res = CreateDialogA(g_InstLangDef, (LPCTSTR)uID, AWnd, ADlgProc);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

LPWSTR AnsiToUnicode (LPSTR AStr) {
	LPWSTR res;
	int lw = MultiByteToWideChar(CP_ACP, 0, AStr, -1, NULL, 0);
	if (lw > 0) {
		res = (LPWSTR) malloc(lw*sizeof(WCHAR));
		MultiByteToWideChar(CP_ACP, 0, AStr, -1, res, lw);
	} else res = (LPWSTR) malloc(sizeof(WCHAR)), res[0] = 0; 
	return res;
}

LPSTR UnicodeToAnsi (LPWSTR AWStr) {
	LPSTR res;
	int lw = WideCharToMultiByte(CP_ACP, 0, AWStr, -1, NULL, 0, NULL, NULL);
	if (lw > 0) {
		res = (LPSTR) malloc(lw);
		WideCharToMultiByte(CP_ACP, 0, AWStr, -1, res, lw, NULL, NULL);
	} else res = (LPSTR) malloc(1), res[0] = 0; 
	return res;
}

LPWSTR EmptyUnicode () {
	LPWSTR res = (LPWSTR) malloc(sizeof(WCHAR)); res[0] = 0; return res;
}

LPSTR EmptyAnsi () {
	LPSTR res = (LPSTR) malloc(1); res[0] = 0; return res;
}

DWORD GetLenUnicodeWithZero(LPWSTR AWStr) { return (wcslen(AWStr)+1)*sizeof(WCHAR);}

BOOL SetWindowTextU (HWND AWnd, LPWSTR AStr) {
	BOOL res;
	if (IsOSUnicode())
		res = SetWindowTextW(AWnd, AStr);
	else {
		LPSTR pA = UnicodeToAnsi(AStr);
		res = SetWindowTextA(AWnd, pA);
		free(pA);
	}
	return res;
}

int GetWindowTextU (HWND AWnd, LPWSTR AStr, int Sz) {
	int res;
	if (IsOSUnicode())
		res = GetWindowTextW(AWnd, AStr, Sz);
	else {
		char *sTemp; if (!(sTemp = (char *)malloc(Sz))) return 0;
		res = GetWindowTextA(AWnd, sTemp, Sz);
		LPWSTR pW = AnsiToUnicode(sTemp);
		int l1 = Sz, l2 = wcslen(pW)+1, l = l1 < l2? l1:l2;
		CopyMemory(AStr, pW, l*sizeof(WCHAR));
		free(pW);
	}
	return res;
}

int LoadStringLangU (UINT uID, LPWSTR lpBuffer, int nBufferMax)
{
	int res;
	if (IsOSUnicode())
		res = LoadStringLangW (uID, lpBuffer, nBufferMax);
	else {
		char *pA = (char *)malloc(nBufferMax);
		res = LoadStringLangA (uID, pA, nBufferMax);
		WCHAR *pW = AnsiToUnicode(pA);
		int l1 = nBufferMax, l2 = wcslen(pW)+1, l = l1 < l2 ? l1:l2;
		CopyMemory(lpBuffer, pW, l*sizeof(WCHAR));
		free(pW); free(pA);
	}
	return res;
}

BOOL TextOutU(HDC hdc, int nXStart, int nYStart, LPWSTR lpString, int cbString) {
	BOOL res;
	if (IsOSUnicode()) 
		res = TextOutW(hdc, nXStart, nYStart, lpString, cbString);
	else {
		char *pA = UnicodeToAnsi(lpString);
		res = TextOutA(hdc, nXStart, nYStart, pA, lstrlenA(pA));
		free(pA);
	}
	return res;
}

BOOL GetTextExtentPoint32U(HDC hdc, LPWSTR lpString, int cbString, LPSIZE lpSize) {
	BOOL res;
	if (IsOSUnicode()) 
		res = GetTextExtentPoint32W(hdc, lpString, cbString, lpSize);
	else {
		char *pA = UnicodeToAnsi(lpString); int l = lstrlenA(pA);
		res = GetTextExtentPoint32A(hdc, pA, (l<cbString)?l:cbString, lpSize);
		free(pA);
	}
	return res;
}

BOOL AppendMenuU (HMENU hMenu, UINT uFlags, UINT_PTR uIDNewItem, LPWSTR lpNewItem) {
	BOOL res;
	if (IsOSUnicode()) 
		res = AppendMenuW(hMenu, uFlags, uIDNewItem, lpNewItem);
	else if ((uFlags & MF_STRING) == MF_STRING) {
		char *pA = UnicodeToAnsi(lpNewItem);
		res = AppendMenuA(hMenu, uFlags, uIDNewItem, pA);
		free(pA);
	} else res = AppendMenuA(hMenu, uFlags, uIDNewItem, (LPSTR)lpNewItem);
	return res;
}

BOOL ModifyMenuU (HMENU hMenu, UINT uPosition, UINT uFlags, UINT_PTR uIDNewItem, 
				  LPWSTR lpNewItem) { 
	BOOL res;
	if (IsOSUnicode()) 
		res = ModifyMenuW(hMenu, uPosition, uFlags, uIDNewItem, lpNewItem);
	else if ((uFlags & MF_STRING) == MF_STRING) {
		char *pA = UnicodeToAnsi(lpNewItem);
		res = ModifyMenuA(hMenu, uPosition, uFlags, uIDNewItem, pA);
		free(pA);
	} else res = ModifyMenuA(hMenu, uPosition, uFlags, uIDNewItem, (LPSTR)lpNewItem);
	return res;
}

BOOL InsertMenuU (HMENU hMenu, HMENU hSub, UINT uPosition, UINT uFlags, UINT uIDStr) { 
	BOOL res;
	if (IsOSUnicode()) {
		WCHAR bufRes[MAX_RESSTR_SIZE];
		LoadStringLangW(uIDStr, bufRes, SIZEOFW(bufRes));
		res = InsertMenuW(hMenu, uPosition, uFlags, (UINT) hSub, bufRes);
	} else {
		char bufRes[MAX_RESSTR_SIZE];
		LoadStringLangA(uIDStr, bufRes, sizeof(bufRes));
		res = InsertMenuA(hMenu, uPosition, uFlags, (UINT) hSub, bufRes);
	}
	return res;
}

BOOL InsertMenuU (HMENU hMenu, HMENU hSub, UINT uPosition, UINT uFlags, LPWSTR lpStr) { 
	BOOL res;
	if (IsOSUnicode()) {
		res = InsertMenuW(hMenu, uPosition, uFlags, (UINT) hSub, lpStr);
	} else {
		char *pA = UnicodeToAnsi(lpStr);
		res = InsertMenuA(hMenu, uPosition, uFlags, (UINT) hSub, pA);
		free(pA);
	}
	return res;
}

int GetMenuStringU (HMENU hMenu, UINT uPosition, LPWSTR lpString, int nMaxCount, 
					 UINT uFlags) { 
	int res;
	if (IsOSUnicode()) 
		res = GetMenuStringW(hMenu, uPosition, lpString, nMaxCount, uFlags);
	else {
		char *pA = (char *)malloc(nMaxCount);
		res = GetMenuStringA(hMenu, uPosition, pA, nMaxCount, uFlags);
		WCHAR *pW = AnsiToUnicode(pA);
		int l1 = nMaxCount, l2 = wcslen(pW)+1, l = l1 < l2 ? l1:l2;
		CopyMemory(lpString, pW, l*sizeof(WCHAR));
		free(pW); free(pA);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

LPWSTR GetTextRangeU (HWND AWnd, LONG AMin, LONG AMax) {
	LPWSTR  res;
	if (IsWindowUnicode(AWnd)) {
		TEXTRANGEW tr; 
		tr.chrg.cpMin = AMin; tr.chrg.cpMax = AMax; 
		res = (LPWSTR) malloc((tr.chrg.cpMax-tr.chrg.cpMin+2)*sizeof(WCHAR));
		tr.lpstrText = res;
		SendMessageW(AWnd, EM_GETTEXTRANGE, 0, (LPARAM)&tr);
	} else {
		TEXTRANGEA tr; 
		tr.chrg.cpMin = AMin; tr.chrg.cpMax = AMax; 
		LPSTR sA= (LPSTR)malloc(tr.chrg.cpMax-tr.chrg.cpMin+2);
		tr.lpstrText = sA;
		SendMessageA(AWnd, EM_GETTEXTRANGE, 0, (LPARAM)&tr);
		res = AnsiToUnicode(sA);
		free(sA);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

HWND CreateWindowExU(DWORD dwExStyle, LPWSTR lpClassName, LPWSTR lpWindowName, 
					DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent,
					HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam) {
	HWND res;
	if (IsOSUnicode()) 
		res = CreateWindowExW(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, 
					nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
	else {
		char *lpClassNameA= UnicodeToAnsi(lpClassName);
		char *lpWindowNameA= UnicodeToAnsi(lpWindowName);
		res = CreateWindowExA(dwExStyle, lpClassNameA, lpWindowNameA, dwStyle, x, y, 
					nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
		free(lpClassNameA);
		free(lpWindowNameA);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

void CharLowerBuffU (LPWSTR AStr) {
	if (IsOSUnicode()) 
		CharLowerBuffW(AStr, wcslen(AStr));
	else {
		LPSTR pA = UnicodeToAnsi(AStr);
		CharLowerBuffA(pA, lstrlen(pA));
		LPWSTR pW = AnsiToUnicode(pA);
		free(pA);
		wcsncpy(AStr, pW, wcslen(AStr));
		free(pW);
	}
}

BOOL IsCharAlphaNumericU(WCHAR AChar) {
	if (IsOSUnicode()) 
		return IsCharAlphaNumericW(AChar);
	else {
		char sA[16];
		WideCharToMultiByte(CP_ACP, 0, &AChar, 1, sA, 16, NULL, NULL);
		return IsCharAlphaNumericA(sA[0]);
	}
}

BOOL IsFindWholeStr(LPWSTR str1, LPWSTR str2, LPWSTR strIn) {
	LPWSTR p = strIn;
	if ((str1 < p)&&(IsCharAlphaNumericU(*(p-1)))) return FALSE;
	p = strIn+wcslen(str2);
	if ((p < str1+wcslen(str1))&&(IsCharAlphaNumericU(*p))) return FALSE;
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

void TabCtrl_InsertItemU(HWND ATabCtrl, HWND APage, UINT uID) {
	WCHAR sW[MAXRES]; LoadStringLangU (uID, sW, SIZEOFW(sW));
	if (IsOSUnicode()) {
		TCITEMW titW;
		titW.mask = TCIF_TEXT | TCIF_PARAM;
		titW.pszText = sW; 
		titW.lParam = (LPARAM)APage;
		SendMessage(ATabCtrl, TCM_INSERTITEMW, TabCtrl_GetItemCount(ATabCtrl), 
			(LPARAM)&titW);
	} else {
		LPSTR sA = UnicodeToAnsi(sW);
		TCITEMA titA;
		titA.mask = TCIF_TEXT | TCIF_PARAM;
		titA.pszText = sA; 
		titA.lParam = (LPARAM)APage;
		SendMessage(ATabCtrl, TCM_INSERTITEMA, TabCtrl_GetItemCount(ATabCtrl), 
			(LPARAM)&titA);
		free(sA);
	}
}

////////////////////////////////////////////////////////////////////////////////

int ListBox_AddStringU(HWND ALB, LPWSTR AStr) {
	int res;
	if (IsOSUnicode()) 
		res = SendMessageW(ALB, LB_ADDSTRING, 0, (LPARAM)AStr);
	else {
		char *pA = UnicodeToAnsi(AStr);
		res = SendMessageA(ALB, LB_ADDSTRING, 0, (LPARAM)pA);
		free(pA);
	}
	return res;
}

////////////////////////////////////////////////////////////////////////////////

LPWSTR ListBox_GetTextU(HWND ALB, int AInd) {
	int l = SendMessage(ALB, LB_GETTEXTLEN, AInd, 0);
	LPWSTR res;
	if (IsOSUnicode()) {
		res = MALLOCWSTR(l+1);
		SendMessageW(ALB, LB_GETTEXT, AInd, (LPARAM)res);
	} else {
		char *pA = (char*)malloc(l+1);
		SendMessageA(ALB, LB_GETTEXT, AInd, (LPARAM)pA);
		res = AnsiToUnicode(pA);
		free(pA);
	}
	return res;
}

/////////////////////////////////// End File ///////////////////////////////////