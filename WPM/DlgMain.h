/*! @DlgMain */
#ifndef __DLGMAIN_H__
#define __DLGMAIN_H__

#include <string>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIDTH_TEXT_IN_STATUS_BAR 700


/**
 * @addtogroup Dialog
 * @brief The main window dialog
 * @{
 */

class CDlgMain
{
public:
	LRESULT CALLBACK DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

	BOOL Init(HWND AWnd);
	void Execute();

	void SetModified();

	//string* GetMyPhrasesFolder() { return &fMyPhrasesFolder; }	
	CDlgMain();
	virtual ~CDlgMain();
private:
    int width, height;

	HMODULE fInst;
	HWND fDlg;    
    HWND hParent;
	HWND fPanelPwd;
    HWND hHint;
	HWND fWndToolbarFolder;
	WINDOWPLACEMENT fPos;
	BOOL fBlockClose;
	BOOL fBlockNotify;
	BOOL fNeedForegroundDlgMain;
	BOOL fDialogIsShowing;
	WNDPROC fDefWndFolderProc;

	char fTitle[128];

	void OnCommand (WPARAM wParam, LPARAM lParam);
	void OnSysCommand (WPARAM wParam, LPARAM lParam);
	void OnInitDialog ();
	void OnGetMinMaxInfo (LPARAM lParam);
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnWindowPosChanged (LPARAM lParam);
	LRESULT OnNotify (WPARAM wParam, LPARAM lParam);
	void OnMouseMove (WPARAM wParam, LPARAM lParam);
	void OnMouseLUp (WPARAM wParam, LPARAM lParam);
	void OnInitMenu (HMENU AMenu);
	void OnMenuSelect(WORD AID);
	BOOL OnSetCursor (HWND AWnd);
	void OnContextMenu (HWND AWnd, LPARAM lParam);
	void OnMenuStat();
	void OnPaint();
	void OnDblClick (WPARAM wParam, LPARAM lParam);

	void CALLBACK Tree_OnSelChange (LPNMTREEVIEW ApnmTV);

	void ReadPosFromReg();
	void WritePosToReg();
	void RealignControls ();
	void CloseMainDlg();
	void UpdateTitle();
	void UpdateButtons ();
	void UpdatePanFolder();
	void UpdateStatusBar();
	void UpdatePhrasesFlags();
	HWND GetHandle() { return ((fDialogIsShowing) ? fDlg:NULL); }
	void ShowDialog();
};

#endif // !defined __DLGMAIN_H__

CDlgMain *GetDlgMain();

/*! @} */