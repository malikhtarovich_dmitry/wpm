#include "TraceLogger.h"


TraceLogger::TraceLogger(const char* fileName, const char* funcName, int lineNumber) {
    _fileName = fileName;
    _funcName = funcName;
	std::ofstream fLog;
	fLog.open("log.txt", std::ios_base::app);
	fLog << "Entering " << _funcName << "() - (" << _fileName << ":" << lineNumber << ")" << std::endl;
	fLog.close();
}

TraceLogger::~TraceLogger() {
	std::ofstream fLog;
	fLog.open("log.txt", std::ios_base::app);
    fLog << "Leaving  " << _funcName << "() - (" << _fileName << ")" << std::endl;
	fLog.close();
}