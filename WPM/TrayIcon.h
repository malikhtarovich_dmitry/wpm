// TrayIcon.h: interface for the CTrayIcon class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __TRAYICON_H__
#define __TRAYICON_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CTrayIcon  
{
public:
	CTrayIcon();
	virtual ~CTrayIcon();
	void SetWndOwnerTrayIcon(HWND hWnd);
	void Show();
	void Hide();
	void Modify(BOOL AEnable);
	BOOL IsEnabled() { return fEnabled; }

private:
	BOOL fShow;
	BOOL fEnabled;
	HICON fIconEnable;
	HICON fIconDisable;
	char fHintEnable[MAX_RESSTR_SIZE];
	char fHintDisable[MAX_RESSTR_SIZE];

	void InitIcons();
};

CTrayIcon *GetTrayIcon();
void StartAnimateTrayIcon();

#endif // !defined __TRAYICON_H__
