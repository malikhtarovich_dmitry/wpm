#include <Windows.h>

void InitHelp();
void UpdateHelpFileName();
void FreeHelp();
void CallHelp(HWND hWnd, char* pcTopic);
void CallHelpPopup( HWND ACtrlWnd, UINT ATopicID);
void CallHelpTutor();
void CallHelpIntro();
void CloseHelp();